//
//  loyaltyConfirmPurchaseTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 11/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class loyaltyConfirmPurchaseTableViewCell: UITableViewCell {

    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateInfo() {
        productImageView.sd_setImage(with: URL(string: post!.productImg), placeholderImage: UIImage(named:"default"), options: SDWebImageOptions(rawValue: 0))
        
        let priceFloat = (post!.price as NSString).floatValue  * 20
        
        let price = String(format: "%.0f", priceFloat)

        
        
        
        
        productPriceLbl.text = price + " Points"
        productNameLbl.text =  post!.name
       
    }

}
