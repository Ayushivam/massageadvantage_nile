//
//  MAMyBookingListTableViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 07/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol MyBookingTableViewCellDelegate : class {
    
    func viewTapAction(_sender:MAMyBookingListTableViewCell)
    
}

class MAMyBookingListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var massageNameLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var completionId: UILabel!
    @IBOutlet weak var completionView: UIView!
    var delegate : MyBookingTableViewCellDelegate?
    
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.orange.cgColor
        yourViewBorder.lineDashPattern = [4, 4]
        yourViewBorder.frame = CGRect(x:0, y: 0, width: completionView.width, height: completionView.height)//savedOfferView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: CGRect(x:0, y: 0, width: completionView.width, height:completionView.height )).cgPath
        completionView.layer.addSublayer(yourViewBorder)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func viewButtonAction(_ sender: UIButton) {
        delegate?.viewTapAction(_sender: self)
    }
    
    func updateInfo(){
        userNameLabel.text = post?.therapistName
        massageNameLabel.text = post?.serviceName
        completionId.text = "Completion id: " + post!.completionID
        timeLbl.text = post?.time
        let month = APPDELEGATE.formattedDateFromString(dateString: post!.bookDate, withFormat: "MMM",inputFormat:"MM/dd/yyyy")!
        self.monthLabel.text = month
        let userID : String = (NSUSERDEFAULT.value(forKey: kUserId) as AnyObject) as! String
        
        let date = APPDELEGATE.formattedDateFromString(dateString: post!.bookDate, withFormat: "dd",inputFormat:"MM/dd/yyyy")!
        self.dateLabel.text = date
        let bookingStatus = post?.bookingStatus
        let cancelby = post?.cancelledBy
        if post?.isPaid == "1"{
            self.statusLabel.text = "Completed"
            self.statusLabel.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.6941176471, blue: 0.2235294118, alpha: 1)
        }
        else
        {
            if bookingStatus == "1" {
                self.statusLabel.text = "Approved"
                self.statusLabel.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.6941176471, blue: 0.2235294118, alpha: 1)
            }
                
            else if bookingStatus == "2" {
                if cancelby == userID
                {
                    self.statusLabel.text = "Cancelled"
                    self.statusLabel.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0.06274509804, blue: 0.1647058824, alpha: 1)
                }
                    
                else
                {
                    
                    self.statusLabel.text = "Cancelled"
                    self.statusLabel.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0.06274509804, blue: 0.1647058824, alpha: 1)
                }
            }
        }
    }
}
