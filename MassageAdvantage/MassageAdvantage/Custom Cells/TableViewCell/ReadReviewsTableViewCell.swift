//
//  ReadReviewsTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 20/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class ReadReviewsTableViewCell: UITableViewCell {

    @IBOutlet weak var commentView: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    ratingView.settings.fillMode = .half

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func updateInfo() {
        
        nameLabel.text = post?.name
        commentView.text = post?.comment
        let ratingStars = (post!.rating as NSString).doubleValue
        
        if ratingStars == 0 {
           ratingView.rating = 0
        }
        else {
           ratingView.rating = ratingStars
        }
    }
    
}
