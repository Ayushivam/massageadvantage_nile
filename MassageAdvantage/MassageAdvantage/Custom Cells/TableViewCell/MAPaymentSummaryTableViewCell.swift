//
//  MAPaymentSummaryTableViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 13/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit


class MAPaymentSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var completionView: UIView!
    @IBOutlet weak var completionIdLabel: UILabel!
    @IBOutlet weak var therapistNameLabel: UILabel!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var amountNameLabel: UILabel!
    @IBOutlet weak var rewardPointLabel: UILabel!
    @IBOutlet weak var paidOnLabel: UILabel!
    var post : UserInfo?
    {
        
        didSet{
            updateInfo()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.orange.cgColor
        // yourViewBorder.strokeColor = UIColor(rgb: 0xFFBD2F, alpha: 0.5).cgColor
        //   yourViewBorder.cornerRadius = 5.0
        //  yourViewBorder.masksToBounds = true
        yourViewBorder.lineDashPattern = [4, 4]
        yourViewBorder.frame = CGRect(x:0, y: 0, width: completionView.width, height: completionView.height)//savedOfferView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: CGRect(x:0, y: 0, width: completionView.width, height:completionView.height )).cgPath
        completionView.layer.addSublayer(yourViewBorder)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateInfo(){
         therapistNameLabel.text = post?.therapistName
        serviceNameLabel.text = post?.serviceName
        completionIdLabel.text = "Transaction  id: " + post!.txnId
        paidOnLabel.text = post?.bookDate
        amountNameLabel.text = post?.totalPrice
        rewardPointLabel.text = post?.loyaltyPoint

    }
}
