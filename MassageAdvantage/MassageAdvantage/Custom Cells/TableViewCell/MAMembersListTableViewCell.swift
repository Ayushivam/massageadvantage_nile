//
//  MAMembersListTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 28/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAMembersListTableViewCell: UITableViewCell {

    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        bgView.backgroundColor = UIColor.white
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOpacity = 8.0
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.shadowRadius = 8.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateInfo() {
        
        nameLbl.text = post?.name
        emailLbl.text = post?.email
    }

}
