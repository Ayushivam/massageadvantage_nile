//
//  MAUpgradeMassageTableViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 13/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol UpgradeeViewCellDelegate : class {
    
    func selectTapAction(_sender:MAUpgradeMassageTableViewCell)
    func selectActionClearBtn(_sender:MAUpgradeMassageTableViewCell)

    
}

class MAUpgradeMassageTableViewCell: UITableViewCell {
 
    @IBOutlet weak var addOnServicesView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cutLbl: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    
    @IBOutlet weak var addLbl: UILabel!
    @IBOutlet weak var priceCutLbl: UILabel!
    
    @IBOutlet weak var actionClearButton: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var discountPriceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var massageNameLabel: UILabel!
    
    var delegate : UpgradeeViewCellDelegate?

    var post = UserInfo(){
        didSet{
            updateInfo()
            
            if post.isSelect == "1" {
                mainView.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
                mainView.layer.shadowColor = UIColor.lightGray.cgColor
                mainView.layer.shadowOpacity = 0.10
                mainView.layer.shadowOffset = CGSize.zero
                mainView.layer.shadowRadius = 2.0
            }
            else {
                mainView.backgroundColor = UIColor.white
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func checkButtonAction(_ sender: UIButton) {
        
        delegate?.selectTapAction(_sender: self)
        
    }
    
    @IBAction func actionClearBtnAction(_ sender: UIButton) {
        delegate?.selectActionClearBtn(_sender: self)

    }
    
    func updateInfo()
    {
        let _ = (post.isSelect == "1") ? (checkButton.isSelected = true) : (checkButton.isSelected = false)
        
        massageNameLabel.text = post.serviceName
        let postPriceFloat = (post.price as NSString).floatValue
        let postPrice = String(format: "%.2f", postPriceFloat)
        priceLabel.text = "$" + postPrice

        if post.isGroupon == "1"{
            priceLabel.isHidden = true
        }else{
            priceLabel.isHidden = false

        }
        
        let discount = post.subDiscount
        if discount == "" {
            
       discountLabel.isHidden = true
       addLbl.isHidden = true
            
        }
        else {
            discountLabel.text = discount+"%Off"
            discountLabel.isHidden = false
            addLbl.isHidden = false
        }

        if post.subDiscountPrice == "" || post.serviceId == "1"{

            priceCutLbl.isHidden = true
            cutLbl.isHidden = true
            
        }
        else {
            priceCutLbl.isHidden = false
            cutLbl.isHidden = false
            let postPriceFloat = (post.price as NSString).floatValue
            let postPrice = String(format: "%.2f", postPriceFloat)
            priceCutLbl.text = "$" + postPrice
        }

        if post.subDiscountPrice == "" {
            
            discountPriceLabel.isHidden = true
            priceCutLbl.isHidden = true
            cutLbl.isHidden = true
        }
            
        else {
            
            let subDiscountPriceFloat = (post.subDiscountPrice as NSString).floatValue
            let subDiscountPrice = String(format: "%.2f", subDiscountPriceFloat)
            discountPriceLabel.text = "$" + subDiscountPrice
            discountPriceLabel.isHidden = false
            
        }
    }
}
