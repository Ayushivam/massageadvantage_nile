//
//  MAMoreMenuTableViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 07/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit


class MAMoreMenuTableViewCell: UITableViewCell {

    
    @IBOutlet weak var menuCellButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cornerImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
}
