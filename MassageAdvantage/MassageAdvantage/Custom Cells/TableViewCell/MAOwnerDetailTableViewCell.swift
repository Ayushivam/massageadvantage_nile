//
//  MAOwnerDetailTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 27/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAOwnerDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var readReviewsBtn: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var therapistProfileImageView: UIImageView!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var therapistNameLbl: UILabel!
    @IBOutlet weak var serviceName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        ratingView.settings.fillMode = .half
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
