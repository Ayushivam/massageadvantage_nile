//
//  retailPlanTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 23/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class retailPlanTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var borderColorView: UIView!
    @IBOutlet weak var commonBtn: UIButton!
    @IBOutlet weak var massagePriceLabel: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var packsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
