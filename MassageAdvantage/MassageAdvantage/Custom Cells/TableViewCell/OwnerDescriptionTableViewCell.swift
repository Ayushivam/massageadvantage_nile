//
//  OwnerDescriptionTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 27/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class OwnerDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var knowMoreLBL: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
