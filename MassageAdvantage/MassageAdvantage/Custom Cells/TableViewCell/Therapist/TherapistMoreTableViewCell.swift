//
//  TherapistMoreTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 26/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistMoreTableViewCell: UITableViewCell {

    @IBOutlet weak var clickButtonAction: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
