//
//  TherapistRefferalTransactionTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 19/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistRefferalTransactionTableViewCell: UITableViewCell {

    
    @IBOutlet weak var paymentDateLabel: UILabel!
    @IBOutlet weak var rewardAmountLabel: UILabel!
    @IBOutlet weak var payAmountLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    
    
    
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateInfo() {
        customerNameLabel.text = post?.name
        emailLabel.text = post?.email
        payAmountLabel.text = "$ " + post!.subsPrice
        rewardAmountLabel.text = "$ " + post!.rewardPoint
       let paymentDate : String = APPDELEGATE.formattedDateFromString(dateString: post!.paymentDate, withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd HH:mm:ss")!
        paymentDateLabel.text = paymentDate

    }

}
