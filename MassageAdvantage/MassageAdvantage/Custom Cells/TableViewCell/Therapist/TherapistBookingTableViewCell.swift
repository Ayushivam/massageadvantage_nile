//
//  Created by nile on 21/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
class TherapistBookingTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var markCompleteBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var timeToLbl: UILabel!
    @IBOutlet weak var timeFromLbl: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var cncelBtnWidth: NSLayoutConstraint!
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateInfo() {
        nameLabel.text = post?.customerName
        

        let isPaid = post?.isPaid
        
        if isPaid == "0" {
            markCompleteBtn.setTitle("Mark Complete", for: .normal)
            markCompleteBtn.isUserInteractionEnabled = true
            cncelBtnWidth.constant = 62
            
            
        }
        
        else if isPaid == "1" {
            markCompleteBtn.setTitle("Completed", for: .normal)
            markCompleteBtn.isUserInteractionEnabled = false
            cncelBtnWidth.constant = 0
        }
        
        
        let status = post?.bookingStatus
        if status == "Booked" {
            markCompleteBtn.isHidden = false

            if isPaid == "1"{
                markCompleteBtn.setTitle("Completed", for: .normal)
                markCompleteBtn.isUserInteractionEnabled = false
                cncelBtnWidth.constant = 0

            }
            else {
                cancelBtn.setTitle("Cancel", for: .normal)
                markCompleteBtn.isHidden = false
                cancelBtn.isUserInteractionEnabled = true
                cncelBtnWidth.constant = 62

            }

        }
        else if status == "Rejected" {
            
                markCompleteBtn.isHidden = true
                cncelBtnWidth.constant = 68
                cancelBtn.isUserInteractionEnabled = false
                cancelBtn.setTitle("Cancelled", for: .normal)
            
        }
        
        let bookingType = post?.bookingType
        
        if bookingType == "subscriber" {
            statusLbl.textColor = #colorLiteral(red: 0.862745098, green: 0.2078431373, blue: 0.2705882353, alpha: 1)
            statusLbl.borderColor = #colorLiteral(red: 0.862745098, green: 0.2078431373, blue: 0.2705882353, alpha: 1)
            sideView.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.2078431373, blue: 0.2705882353, alpha: 1)
            viewBtn.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.2078431373, blue: 0.2705882353, alpha: 1)
            cancelBtn.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.2078431373, blue: 0.2705882353, alpha: 1)
            markCompleteBtn.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.2078431373, blue: 0.2705882353, alpha: 1)
            profileBtn.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.2078431373, blue: 0.2705882353, alpha: 1)
            statusLbl.text = "MA Member"
           

        }
        else if bookingType == "retail" {
            statusLbl.textColor = #colorLiteral(red: 0.968627451, green: 0.7490196078, blue: 0.1529411765, alpha: 1)
            statusLbl.borderColor = #colorLiteral(red: 0.968627451, green: 0.7490196078, blue: 0.1529411765, alpha: 1)
            sideView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.7490196078, blue: 0.1529411765, alpha: 1)
            viewBtn.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.7490196078, blue: 0.1529411765, alpha: 1)
            cancelBtn.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.7490196078, blue: 0.1529411765, alpha: 1)
            markCompleteBtn.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.7490196078, blue: 0.1529411765, alpha: 1)
            statusLbl.text = "Non MA Member"
            profileBtn.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.7490196078, blue: 0.1529411765, alpha: 1)

        }
        else if bookingType == "coupon" {
            statusLbl.textColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)
            statusLbl.borderColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)
            sideView.backgroundColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)
            viewBtn.backgroundColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)
            cancelBtn.backgroundColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)
            markCompleteBtn.backgroundColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)
            statusLbl.text = "Daily Deal Customer"
            profileBtn.backgroundColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)

        }
        
        else if bookingType == "offline" {
            statusLbl.textColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
            statusLbl.borderColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
            sideView.backgroundColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
            viewBtn.backgroundColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
            cancelBtn.backgroundColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
            markCompleteBtn.backgroundColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
            statusLbl.text = "Offline"
            profileBtn.backgroundColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
            markCompleteBtn.isHidden = true

        }
        
       timeFromLbl.text = post?.bookingFrom
       timeToLbl.text = post?.bookingTo

        

    }
}
