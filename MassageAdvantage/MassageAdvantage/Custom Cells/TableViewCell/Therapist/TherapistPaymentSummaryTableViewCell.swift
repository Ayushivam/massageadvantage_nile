//
//  TherapistPaymentSummaryTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 09/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistPaymentSummaryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var receivedOn: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var customerType: UILabel!
    @IBOutlet weak var customerLbl: UILabel!
    
    @IBOutlet weak var viewBtn: UIButton!
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateInfo() {
        customerLbl.text = post?.customerName
        serviceName.text = post?.serviceName
        amountLbl.text = post?.payAmount
        
        if post?.bookingType == "subscriber"
        {
            customerType.text = "Member"
        }
        else if post?.bookingType == "coupon" {
             customerType.text = "Voucher"
        }
        
         else if post?.bookingType == "retail" {
             customerType.text = "Retail"
        }
        
        
//        if post?.serviceId == "1"
//        {
//            if post?.bookingType == "subscriber" {
//               amountLbl.text = post?.price
//            }
//
//             else if post?.bookingType == "coupon" {
//                 amountLbl.text = "20"
//            }
//            else if post?.bookingType == "retail" {
//
//                let bookingPrice = (post!.price as NSString).floatValue * 0.8
//
//                let bookingAmount = String(format: "%.2f", bookingPrice)
//
//                    amountLbl.text = bookingAmount
//            }
//
//        }
//
//        else {
//
//            if post?.bookingType == "subscriber" {
//            }
//
//            else if post?.bookingType == "coupon" {
//            }
//            else if post?.bookingType == "retail" {
//
          //  }
      //  }
        
        
        
        
    }

}
