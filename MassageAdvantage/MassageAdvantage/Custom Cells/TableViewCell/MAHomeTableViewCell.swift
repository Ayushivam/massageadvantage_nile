//
//  MAHomeTableViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 08/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol HomeTableViewCellDelegate : class {
    func cellTapAction(_sender:MAHomeTableViewCell)
}

class MAHomeTableViewCell: UITableViewCell {
    @IBOutlet weak var imageTapButton: UIButton!
    @IBOutlet weak var massageName: UILabel!
    @IBOutlet weak var massageImage: UIImageView!
    var delegate : HomeTableViewCellDelegate?
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    
    override func awakeFromNib() {

        super.awakeFromNib()
        massageImage.layer.masksToBounds = true
        massageImage.layer.cornerRadius = 20
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func imageTapAction(_ sender: UIButton) {
        delegate?.cellTapAction(_sender: self)
    }
    
    func updateInfo() {
        massageName.text = post?.serviceName
        massageImage.sd_setImage(with: URL(string: post!.serviceImg), placeholderImage: UIImage(named:"default"), options: SDWebImageOptions(rawValue: 0))
    }
}
