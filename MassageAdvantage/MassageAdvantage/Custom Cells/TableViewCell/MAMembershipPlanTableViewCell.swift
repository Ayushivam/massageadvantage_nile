//
//  MAMembershipPlanTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 03/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAMembershipPlanTableViewCell: UITableViewCell {
    
    @IBOutlet weak var transactionIDView: UIView!
    @IBOutlet weak var planLbl: UILabel!
    @IBOutlet weak var renewDate: UILabel!
    @IBOutlet weak var subsDate: UILabel!
    @IBOutlet weak var transactionIdLbl: UILabel!
    
    @IBOutlet weak var reinstateFeeLbl: UILabel!
    @IBOutlet weak var cancelLbl: UILabel!
    var post : UserInfo?
    {
        
        didSet{
            updateInfo()
        }
    }
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
  //      self.view.layoutIfNeeded()

        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.orange.cgColor
        
        yourViewBorder.lineDashPattern = [4, 4]
        yourViewBorder.frame = CGRect(x:0, y: 0, width: transactionIDView.width, height: transactionIDView.height)//savedOfferView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: CGRect(x:0, y: 0, width: transactionIDView.width, height:transactionIDView.height )).cgPath
        transactionIDView.layer.addSublayer(yourViewBorder)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateInfo(){
        
        transactionIdLbl.text = "Membership id: " + post!.txnId
        reinstateFeeLbl.text = "$ " + post!.fee
        subsDate.text = post?.subsDate
        renewDate.text = post?.renewDate
        planLbl.text = post?.subsPrice
        cancelLbl.text = post?.bookingStatus
        if post?.bookingStatus == "Cancelled"{
            cancelLbl.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        }else{
            cancelLbl.text = post?.bookingStatus
            cancelLbl.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        }
    }
}
