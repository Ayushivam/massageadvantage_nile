//
//  MAProfileTableViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 16/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLbl: UILabel!
    @IBOutlet weak var commonTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
