//
//  MAAvalabilityCollectionViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 10/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol AvalabilityTypeCellDelegate : class {
    
    func avalibilityCellTapAction(_sender:MAAvalabilityCollectionViewCell)
    
}

class MAAvalabilityCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var avalabilityBackView: UIView!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var availableLabel: UILabel!
    
    @IBOutlet weak var selectAvalabilityButton: UIButton!
  
    var avalabilityDelegate : AvalabilityTypeCellDelegate?
    
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    
    @IBAction func selectAvalabilityButtonAction(_ sender: UIButton) {
        
        avalabilityDelegate?.avalibilityCellTapAction(_sender: self)

    }
    
    
    func updateInfo()
    {
        
        self.timeLabel.text = post!.time
        self.availableLabel.text = post!.availability
        
        if post!.availability == "Available"{
            selectAvalabilityButton.isUserInteractionEnabled = true
            
            if post?.isSelect == "1" {
                avalabilityBackView.backgroundColor = #colorLiteral(red: 0.431372549, green: 0.7490196078, blue: 0.7647058824, alpha: 1)
                timeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                availableLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            }
            else{
                avalabilityBackView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                timeLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                availableLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                
            }
        }else{
            selectAvalabilityButton.isUserInteractionEnabled = false
            avalabilityBackView.backgroundColor = #colorLiteral(red: 0.8274509804, green: 0.8274509804, blue: 0.8274509804, alpha: 1)
            timeLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            availableLabel.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        
        
        
    }
}
