//
//  MAMassageTypesCollectionViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 10/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol MassageTypeCellDelegate : class {
    
    func cellTapAction(_sender:MAMassageTypesCollectionViewCell)

}

class MAMassageTypesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var massageTypeButton: UIButton!
    @IBOutlet weak var therapistName: UILabel!
    @IBOutlet weak var massageNameLbl: UILabel!
    @IBOutlet weak var massageImageView: UIImageView!
 
    var delegate : MassageTypeCellDelegate?
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }

    override func draw(_ rect: CGRect) { //Your code should go here.
        super.draw(rect)
        ratingView.settings.fillMode = .half
        
    }

    func updateInfo() {
        if   post!.requestURL == "" {
            massageTypeButton.setTitle("BOOK", for: .normal)
        }
        else {
            massageTypeButton.setTitle("REQUEST", for: .normal)
        }
        massageNameLbl.text = post?.businessName
        therapistName.text = post?.therapistName
        massageImageView.sd_setImage(with: URL(string: post!.serviceImg), placeholderImage: UIImage(named:"default"), options: SDWebImageOptions(rawValue: 0))
        let abc = (post!.rating as NSString).doubleValue
        if abc == 0 {
            ratingView.rating = 0
        }
        else {
            ratingView.rating = abc
        }
        if post!.city == "" {
             locationLbl.text =  post!.state
        }
        else if post!.state == "" {
            locationLbl.text =  post!.city
        }
        else {
        locationLbl.text = post!.city + "," + post!.state
        }
    }
    @IBAction func massageTypeAction(_ sender: UIButton) {
        delegate?.cellTapAction(_sender: self)

        }
}
