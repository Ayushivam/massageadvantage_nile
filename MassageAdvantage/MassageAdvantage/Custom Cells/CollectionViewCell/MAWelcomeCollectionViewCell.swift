
//
//  MAWelcomeCollectionViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 19/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAWelcomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var openUrlButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var descriptionLbl: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tutorialImg: UIImageView!
}
