//
//  MAOwnerDetailsImagesCollectionViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 27/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAOwnerDetailsImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var ownerMassageImageView: UIImageView!
}
