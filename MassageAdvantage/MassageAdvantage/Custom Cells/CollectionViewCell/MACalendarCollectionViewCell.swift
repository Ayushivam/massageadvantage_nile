//
//  MACalendarCollectionViewCell.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 10/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol CalendarTypeCellDelegate : class {
    
    func calendarCellTapAction(_sender:MACalendarCollectionViewCell)
    
}

class MACalendarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var calanderBlur: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var digitLabel: UILabel!
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var crossImage: UIImageView!
    
    var calendarDelegate : CalendarTypeCellDelegate?
    
    var post : UserInfo? {
        didSet{
            updateInfo()
        }
    }
    @IBAction func selectDateButtonAction(_ sender: UIButton) {
        calendarDelegate?.calendarCellTapAction(_sender: self)
    }
    
    func updateInfo(){
        monthLabel.text = post!.day
        digitLabel.text = post!.digit
        dayLabel.text = post!.weekName
        
        if post!.isActive == ""{
            self.crossImage.isHidden = false
            if post!.isBlocked != ""{
                self.selectButton.isUserInteractionEnabled = true
            }else{
                self.selectButton.isUserInteractionEnabled = false
            }
        }else{
            self.crossImage.isHidden = true
            self.selectButton.isUserInteractionEnabled = true
        }
        if post!.isSelect == "1"{
            backView.backgroundColor = #colorLiteral(red: 0.431372549, green: 0.7490196078, blue: 0.7647058824, alpha: 1)
            monthLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            dayLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            digitLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }else{
            backView.backgroundColor = .clear
            monthLabel.textColor = #colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)
            dayLabel.textColor = #colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)
            digitLabel.textColor = #colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)
        }
         if post!.isBlocked != ""
         {
            calanderBlur.isHidden = false
         }else{
            calanderBlur.isHidden = true
        }
    }
}
