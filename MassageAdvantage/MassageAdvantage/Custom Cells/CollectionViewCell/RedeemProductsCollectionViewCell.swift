//
//  RedeemProductsCollectionViewCell.swift
//  MassageAdvantage
//
//  Created by nile on 10/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol RedeemProductsCollectionDelegate : class {
    
    func viewImage(_sender:RedeemProductsCollectionViewCell)
    func seclectProduct(_sender:RedeemProductsCollectionViewCell)

}

class RedeemProductsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var loyaltyPointsLbl: UILabel!
    @IBOutlet weak var productImageButton: UIButton!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productImageView: UIImageView!

    @IBOutlet weak var productPriceLbl: UILabel!
    var delegate : RedeemProductsCollectionDelegate?

    @IBOutlet weak var commonBtn: UIButton!
    var post : UserInfo?
    {
        didSet{
            updateInfo()
        }
    }
    
    func updateInfo() {
         productImageView.sd_setImage(with: URL(string: post!.productImg), placeholderImage: UIImage(named:"default"), options: SDWebImageOptions(rawValue: 0))
        
        
        let priceFloat = (post!.price as NSString).floatValue  * 20
        
        let loyaltyPoints = String(format: "%.0f", priceFloat)

        
        
        loyaltyPointsLbl.text =  "\(loyaltyPoints)" + " Points"
         productPriceLbl.text = "$ " + post!.price
        productNameLbl.text =  post!.name
        if post?.selectProduct == "0" {
            checkImageView.image = #imageLiteral(resourceName: "uncheck")
        }
        
        else {
            checkImageView.image = #imageLiteral(resourceName: "check")

        }
    }

    @IBAction func showImageAction(_ sender: UIButton) {
        
         delegate?.viewImage(_sender: self)

    }

    @IBAction func selectProductAction(_ sender: UIButton) {
      
       
        
        delegate?.seclectProduct(_sender: self)

    }
}
