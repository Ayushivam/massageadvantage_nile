//
//  AppDelegate.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 06/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate{
    
    var window: UIWindow?
    var isReachable = false
    var navController :  UINavigationController?
    var navRatingController :  UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupReachability()

        if let _ = NSUSERDEFAULT.value(forKey: kFirstTime){
            setInitialController(identifier: "MALoginViewController")
        }else{
            NSUSERDEFAULT.set("1", forKey: kFirstTime)
            setInitialController(identifier: "MAWelcomeViewController")
        }

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        GMSPlacesClient.provideAPIKey("AIzaSyDNh8eYKd1BSHQXt9gq2YX8lhea6_bB6jo")
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
      //  callApiToCheckVersion()
        
        // Override point for customization after application launch.
        return true
    }
    //MARK:-  Date Format
    func formattedDateFromString(dateString: String, withFormat format: String,inputFormat:String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = inputFormat
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
    func formattedDateFromDate(dateString: Date, withFormat format: String,inputFormat:String) -> Date? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zz"
        let date = inputFormatter.string(from: dateString)
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = format
        return outputFormatter.date(from: date)
    }
    
    
    //MARK:- Set Initial Controller
    func setInitialController(identifier:String) {
        window = UIWindow(frame:UIScreen.main.bounds)
        let rootVC = storyBoardForName(name:"Main").instantiateViewController(withIdentifier:identifier)
        self.navController = UINavigationController.init(rootViewController: rootVC)
        self.navController?.isNavigationBarHidden = true
        self.window!.rootViewController = self.navController
        self.window?.makeKeyAndVisible()
    }
    
    //MARK:- Set Up Rechability
    func setupReachability() {
        // Allocate a reachability object
        let reach = Reachability.forInternetConnection()
        self.isReachable = reach!.isReachable()
        // Set the blocks
        reach?.reachableBlock = { (reachability) in
            DispatchQueue.main.async(execute: {
                self.isReachable = true
            })
        }
        reach?.unreachableBlock = { (reachability) in
            DispatchQueue.main.async(execute: {
                self.isReachable = false
            })}
        reach?.startNotifier()
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            
            if key != kFCMToken && key != kFirstTime {
                defaults.removeObject(forKey: key)
            }
        }
    }
    
    func goToLogin() {
        resetDefaults()
        APPDELEGATE.navController?.viewControllers.removeAll()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyBoard.instantiateViewController(withIdentifier: "MALoginViewController") as! MALoginViewController
        APPDELEGATE.navController?.pushViewController(loginVC, animated: true)
    }
    
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
       // print(remoteMessage.appData)
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        NSUSERDEFAULT.set(fcmToken, forKey: kFCMToken)
    }
    
    //MARK:- Push Notification Handler
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let notificationName : String = (userInfo["gcm.notification.notification_name"] as? String)!
        
         if notificationName == "massageComplete"
        {
            
            if application.applicationState == .active {
                let therapistId : String = (userInfo["gcm.notification.therapist_detail_id"] as? String)!
                let bookingId : String = (userInfo["gcm.notification.booking_id"] as? String)!
                
                let ratingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MARatingViewController") as! MARatingViewController
                
                ratingVC.bookingId = bookingId
                ratingVC.therapistId = therapistId
                ratingVC.providesPresentationContextTransitionStyle = true
                ratingVC.definesPresentationContext = true
                ratingVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                APPDELEGATE.navController?.present(ratingVC, animated: true, completion: nil)
                
            }else{
                
                AlertController.alert(title: "Service Completed", message: "Your service has been completed.", buttons: ["OK"]) { (alert, index) in
                    let therapistId : String = (userInfo["gcm.notification.therapist_detail_id"] as? String)!
                    let bookingId : String = (userInfo["gcm.notification.booking_id"] as? String)!
                    let ratingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MARatingViewController") as! MARatingViewController
                    ratingVC.bookingId = bookingId
                    ratingVC.therapistId = therapistId
                    ratingVC.providesPresentationContextTransitionStyle = true
                    ratingVC.definesPresentationContext = true
                    ratingVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                    APPDELEGATE.navController?.present(ratingVC, animated: true, completion: nil)
                }
            }
        }
        else{
            guard let alertDict = ((userInfo["aps"] as? NSDictionary)?.value(forKey: "alert")) as? NSDictionary,
                let title = alertDict["title"] as? String,
                let body = alertDict["body"] as? String
                else { return }
            
            AlertController.alert(title:title , message: body)
        }
        return
    }

//        func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//
//            let subtitle = notification.description
//            print(subtitle)
//
//                completionHandler([.alert, .badge, .sound])
//
//        }

    //MARK:- Background APi For status update
    func backgroundApiForUpdateStatus()
    {

        if let _ = NSUSERDEFAULT.value(forKey: kUserGroup) {
            
            if let _ = NSUSERDEFAULT.value(forKey: kUserId)
            {
            
            let userGroup : String = NSUSERDEFAULT.value(forKey: kUserGroup) as! String
                if userGroup == "2"{
                    
                APIManager.apiForGetUpdateUserStatus { (status) in

                    }
            }
            }
        }
    }

    //MARK:- API For Check Version
    func callApiToCheckVersion()
    {
        ServiceHelper.request([:], method: .get, apiName: kCheckVersion, hudType: .default) { (result, error, code) in
            
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                        
                        
                        let result : Array = response.validatedValue("results", expected: [] as AnyObject) as! Array<Any>
                      
                        if result.count != 0
                        {
                        let dict : Dictionary = result[0] as! Dictionary<String,AnyObject>
                        let version : String = dict.validatedValue("version", expected: "" as AnyObject) as! String

                        if version != appVersion{
                            AlertController.alert(title:"New Update Available",  message: "New update available on app store, click ok to update.", buttons: ["OK"], tapBlock: { (alert, index) in
                                    if let url = URL(string: "itms-apps://itunes.apple.com/app/id1473399997"),
                                        UIApplication.shared.canOpenURL(url){
                                        UIApplication.shared.openURL(url)
                                        self.resetDefaults()
                                        self.callApiToCheckVersion()
                                    }
                            })
                            
                        }
                    }
                    }
                }
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        backgroundApiForUpdateStatus()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
