//
//  UserInfo.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 07/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class UserInfo: NSObject {
    //Booking Status
    var imgURL = "https://www.massageadvantage.com/"
    //"https://www.niletechinnovations.com/projects/massage/"
    var bookingStatus = ""
    var dateCount = ""
    var monthName = ""
    var massageName = ""
    var userName = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var phoneNumber = ""
    var address = ""
    var profileImg = ""
    var profileThumb = ""
    var loyaltiPoints = ""
    var userLat = ""
    var userLong = ""
    var estimaterAmount = ""
    var totalAmount = ""
    var subDiscount = ""
    var subDiscountPrice = ""
    var isSelect = ""
    var isGroupon = ""
    var therapistName = ""
    var serviceName = ""
    var totalPrice = ""
    var rewardPoint = ""
    var paidOn = ""
    var serviceId = ""
    var serviceImg = ""
    var businessName = ""
    var desc =  ""
    var slug = ""
    var id = ""
    var thumbURL = ""
    var fileName = ""
    var rating = ""
    var name = ""
    var bookDate = ""
    var loyaltyPoint = ""
    var txnId = ""
    var therapistDetailID = ""
    var therapistID = ""
    var numberOfTherapist = ""
    var notAvlFromDate = ""
    var notAvlToDate = ""
    var gender = ""
    var acceptCoupon = ""
    var acceptNoOfCoupon = ""
    var openTime = ""
    var closeTime = ""
    var time = ""
    var completionID = ""
    var isPaid = ""
    var paymentID = ""
    var subsDate = ""
    var renewDate = ""
    var subsPrice = ""
    var timeSlotFrom = ""
    var timeSlotTo = ""
    var comment = ""
    var payOn = ""
    var createdDate = ""
    var availableDaysArray = [UserInfo]()
    var notAvailabelDatesArray = Array<String>()
    var serviceArray = [UserInfo]()
    var dateString = ""
    var isActive = ""
    var isBlocked = ""
    var day = ""
    var digit = ""
    var weekName = ""
    var availability = ""
    var soapNoteId = ""
    var price = ""
    var amount = ""
    var dummyTotal = ""
    var extraDiscount = ""
    var availableDay = ""
    
    var swedishPrice = ""
    var upgradePrice = ""
    var subTotal = ""
    var needToPay = ""
    var couponValue = ""
    var userId = ""
    var cancelledBy = ""
    var remark = ""
    var bookingType = ""
    var ownerName = ""
    var customerName = ""
    var cleaningTime = ""
    var bookingTo = ""
    var bookingOn = ""
    var bookingFrom = ""
    var requestURL = ""
    var city = ""
    var state = ""
    var couponID = ""
    var productImg = ""
    var selectProduct = "0"
    var payAmount = ""
    var cancellationCharge = ""
    var paymentDate = ""
    var payCancellationCharge = ""
    var content = ""
    var fee = ""
    var colorCode = ""
    var paySummData = Dictionary<String,AnyObject>()
    var payOutData = Dictionary<String,AnyObject>()
    var productsDetails =  [UserInfo]()

    class func getServiceListArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.serviceName = item.validatedValue("service_name", expected: "" as AnyObject) as! String
            modalObj.serviceId = item.validatedValue("service_id", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
            
            
        }
        return resultArray
    }
    
    class func getServiceNameListArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.serviceName = item.validatedValue("service_name", expected: "" as AnyObject) as! String
            modalObj.serviceId = item.validatedValue("service_id", expected: "" as AnyObject) as! String
            modalObj.serviceImg = item.validatedValue("service_img", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
            
        }
        return resultArray
    }
    
    class func therapistListArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            modalObj.businessName = item.validatedValue("business_name", expected: "" as AnyObject) as! String
            modalObj.desc = item.validatedValue("description", expected: "" as AnyObject) as! String
            modalObj.slug = item.validatedValue("slug", expected: "" as AnyObject) as! String
            modalObj.id = item.validatedValue("id", expected: "" as AnyObject) as! String
            modalObj.requestURL = item.validatedValue("request_url", expected: "" as AnyObject) as! String
            modalObj.thumbURL = item.validatedValue("thumb_url", expected: "" as AnyObject) as! String
            modalObj.fileName = item.validatedValue("file_name", expected: "" as AnyObject) as! String
            modalObj.therapistName = item.validatedValue("therapist_name", expected: "" as AnyObject) as! String
            modalObj.city = item.validatedValue("city", expected: "" as AnyObject) as! String
            modalObj.state = item.validatedValue("state", expected: "" as AnyObject) as! String
            modalObj.rating = item.validatedValue("rating", expected: "" as AnyObject) as! String
            modalObj.serviceImg = modalObj.imgURL + "uploads/gallery/" + (item.validatedValue("id", expected: "" as AnyObject) as! String) + "/thumbnail/" + (item.validatedValue("thumb_url", expected: "" as AnyObject) as! String)
            
            resultArray.append(modalObj)
            
        }
        return resultArray
    }
    
    
    class func getMembersListArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.name = item.validatedValue("first_name", expected: "" as AnyObject) as! String
            modalObj.email = item.validatedValue("email", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
            
        }
        return resultArray
    }
    
    class func getPaymentSummArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.therapistName = item.validatedValue("therapist_name", expected: "" as AnyObject) as! String
            modalObj.serviceName = item.validatedValue("massage_name", expected: "" as AnyObject) as! String
            modalObj.bookDate = item.validatedValue("booking_date", expected: "" as AnyObject) as! String
            modalObj.loyaltyPoint = item.validatedValue("loyalty_point", expected: "" as AnyObject) as! String
            modalObj.txnId = item.validatedValue("txn_id", expected: "" as AnyObject) as! String
            modalObj.totalPrice = item.validatedValue("total_price", expected: "" as AnyObject) as! String
            
            resultArray.append(modalObj)
            
        }
        return resultArray
    }
    
    class func therapistImageArray(responseArray: Array<Dictionary<String, Any>>,theripistId:String) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            modalObj.serviceImg = modalObj.imgURL + "uploads/gallery/" + theripistId + "/thumbnail/" + (item.validatedValue("thumb_url", expected: "" as AnyObject) as! String)
            resultArray.append(modalObj)
            
        }
        return resultArray
    }
    
    class func getBookingListArray(responseArray: Array<Dictionary<String, Any>>,payCancellationCharge:String) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            modalObj.therapistID = item.validatedValue("therapist_detail_id", expected: "" as AnyObject) as! String
            modalObj.therapistName = item.validatedValue("therapist_name", expected: "" as AnyObject) as! String
            modalObj.serviceName = item.validatedValue("massage_name", expected: "" as AnyObject) as! String
            
            modalObj.bookDate = item.validatedValue("booking_date", expected: "" as AnyObject) as! String
            modalObj.time = item.validatedValue("time", expected: "" as AnyObject) as! String
            modalObj.completionID = item.validatedValue("completion_id", expected: "" as AnyObject) as! String
            
            modalObj.bookingStatus = item.validatedValue("booking_status", expected: "" as AnyObject) as! String
            modalObj.isPaid = item.validatedValue("is_paid", expected: "" as AnyObject) as! String
            modalObj.paymentID = item.validatedValue("payment_id", expected: "" as AnyObject) as! String
            
            modalObj.phoneNumber = item.validatedValue("phone", expected: "" as AnyObject) as! String
            modalObj.email = item.validatedValue("email", expected: "" as AnyObject) as! String
            modalObj.address = item.validatedValue("address", expected: "" as AnyObject) as! String
            
            modalObj.totalPrice = item.validatedValue("total_price", expected: "" as AnyObject) as! String
            modalObj.slug = item.validatedValue("slug", expected: "" as AnyObject) as! String
            modalObj.bookingType = item.validatedValue("booking_type", expected: "" as AnyObject) as! String
            
            modalObj.cancelledBy = item.validatedValue("cancelled_by", expected: "" as AnyObject) as! String
            modalObj.bookingOn = item.validatedValue("booking_on", expected: "" as AnyObject) as! String
            modalObj.dateCount = item.validatedValue("due_days", expected: "" as AnyObject) as! String
            
            modalObj.cancellationCharge = item.validatedValue("cancellation_charge", expected: "" as AnyObject) as! String
            modalObj.paymentDate = item.validatedValue("payment_date", expected: "" as AnyObject) as! String
            modalObj.payCancellationCharge = payCancellationCharge
            
            resultArray.append(modalObj)
            
            
            
        }
        return resultArray
    }
    
    class func getAvailabelDaysArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.availableDay = item.validatedValue("ref_desc", expected: "" as AnyObject) as! String
            modalObj.openTime = item.validatedValue("open_time", expected: "" as AnyObject) as! String
            modalObj.closeTime = item.validatedValue("close_time", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
         return resultArray
    }
    
    class func getmemberShipPlanArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            
            modalObj.txnId = item.validatedValue("txn_id", expected: "" as AnyObject) as! String
            modalObj.subsDate = item.validatedValue("subscr_date", expected: "" as AnyObject) as! String
            modalObj.renewDate = item.validatedValue("renewal_date", expected: "" as AnyObject) as! String
            modalObj.subsPrice = item.validatedValue("subscription_price", expected: "" as AnyObject) as! String
            modalObj.bookingStatus = item.validatedValue("subscription_status", expected: "" as AnyObject) as! String
            modalObj.fee = item.validatedValue("reinstate_fee", expected: "" as AnyObject) as! String
            modalObj.id = item.validatedValue("subscribe_id", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
        return resultArray
    }

    class func getSlotArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            
            modalObj.time = item.validatedValue("time", expected: "" as AnyObject) as! String
            modalObj.availability = item.validatedValue("available", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
            
        }
        return resultArray
    }

    class func getServiceArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()

        for item in responseArray {


            let modalObj = UserInfo()

            modalObj.serviceId = item.validatedValue("service_id", expected: "" as AnyObject) as! String
            modalObj.serviceName = item.validatedValue("service_name", expected: "" as AnyObject) as! String
            modalObj.subDiscount = item.validatedValue("sub_discount", expected: "" as AnyObject) as! String
            modalObj.price = item.validatedValue("price", expected: "" as AnyObject) as! String
            modalObj.subDiscountPrice = item.validatedValue("sub_discount_price", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }

        
        return resultArray
    }
    
    class func checkoutArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.serviceId = item.validatedValue("service_id", expected: "" as AnyObject) as! String
            modalObj.swedishPrice = item.validatedValue("swedish_price", expected: "" as AnyObject) as! String
            modalObj.upgradePrice = item.validatedValue("upgrade_price", expected: "" as AnyObject) as! String
            modalObj.subDiscount = item.validatedValue("sub_discount", expected: "" as AnyObject) as! String
            modalObj.subTotal = item.validatedValue("sub_total", expected: "" as AnyObject) as! String
            modalObj.needToPay = item.validatedValue("need_to_pay", expected: "" as AnyObject) as! String
            modalObj.couponValue = item.validatedValue("cup_discount", expected: "" as AnyObject) as! String

            resultArray.append(modalObj)
        }
       
        return resultArray
    }
    
    class func reviewArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
           
            modalObj.name = item.validatedValue("user_name", expected: "" as AnyObject) as! String
            modalObj.comment = item.validatedValue("comment", expected: "" as AnyObject) as! String
            modalObj.rating = item.validatedValue("rating_point", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
        
        return resultArray
    }
  
    class func therapistBookingArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.id = item.validatedValue("booking_id", expected: "" as AnyObject) as! String
            modalObj.userId = item.validatedValue("user_id", expected: "" as AnyObject) as! String
            modalObj.soapNoteId = item.validatedValue("soap_note_id", expected: "" as AnyObject) as! String
            modalObj.cancelledBy = item.validatedValue("cancelled_by", expected: "" as AnyObject) as! String
            modalObj.bookingStatus = item.validatedValue("booking_status", expected: "" as AnyObject) as! String
            modalObj.bookingOn = item.validatedValue("booking_on", expected: "" as AnyObject) as! String
            modalObj.bookingFrom = item.validatedValue("booking_from", expected: "" as AnyObject) as! String
            modalObj.bookingTo = item.validatedValue("booking_to", expected: "" as AnyObject) as! String
            modalObj.cleaningTime = item.validatedValue("cleaning_time", expected: "" as AnyObject) as! String
            modalObj.serviceName = item.validatedValue("service_name", expected: "" as AnyObject) as! String
            
            modalObj.customerName = item.validatedValue("customer_name", expected: "" as AnyObject) as! String
             modalObj.email = item.validatedValue("customer_email", expected: "" as AnyObject) as! String
             modalObj.phoneNumber = item.validatedValue("customer_phone", expected: "" as AnyObject) as! String
            
            modalObj.ownerName = item.validatedValue("owner_name", expected: "" as AnyObject) as! String
            modalObj.bookingType = item.validatedValue("booking_type", expected: "" as AnyObject) as! String
            modalObj.isPaid = item.validatedValue("is_paid", expected: "" as AnyObject) as! String

            modalObj.remark = item.validatedValue("remark", expected: "" as AnyObject) as! String
            modalObj.totalAmount = item.validatedValue("total_amount", expected: "" as AnyObject) as! String
            
            resultArray.append(modalObj)
        }
        
        return resultArray
    }
    
    
    
    class func calanderDateArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.bookingOn = item.validatedValue("booking_on", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
        
        return resultArray
    }

    class func getTherapistPaymentSummArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.therapistName = item.validatedValue("username", expected: "" as AnyObject) as! String
            modalObj.email = item.validatedValue("customer_email", expected: "" as AnyObject) as! String
            modalObj.phoneNumber = item.validatedValue("customer_phone", expected: "" as AnyObject) as! String
            
            modalObj.serviceName = item.validatedValue("massage_name", expected: "" as AnyObject) as! String
            modalObj.amount = item.validatedValue("total_amount", expected: "" as AnyObject) as! String
            modalObj.payOn = item.validatedValue("payon", expected: "" as AnyObject) as! String
            
            modalObj.txnId = item.validatedValue("txn_id", expected: "" as AnyObject) as! String
            modalObj.loyaltyPoint = item.validatedValue("loyalty_point", expected: "" as AnyObject) as! String
            modalObj.bookingType = item.validatedValue("booking_type", expected: "" as AnyObject) as! String
            
            modalObj.paymentID = item.validatedValue("payment_id", expected: "" as AnyObject) as! String
            modalObj.couponID = item.validatedValue("coupon_id", expected: "" as AnyObject) as! String
            modalObj.serviceId = item.validatedValue("service_id", expected: "" as AnyObject) as! String
            
            modalObj.price = item.validatedValue("booking_price", expected: "" as AnyObject) as! String
            modalObj.customerName = item.validatedValue("customer_name", expected: "" as AnyObject) as! String
            modalObj.bookingOn = item.validatedValue("booking_on", expected: "" as AnyObject) as! String
            
            modalObj.bookingFrom = item.validatedValue("booking_from", expected: "" as AnyObject) as! String
            modalObj.payAmount = item.validatedValue("payable_amount", expected: "" as AnyObject) as! String
            
            modalObj.paySummData = item.validatedValue("payment_summary", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>

            modalObj.payOutData = item.validatedValue("therapist_payout", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>


            resultArray.append(modalObj)
            
        }
        return resultArray
    }
    
    
    class func paySummaryDataArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
             let modalObj = UserInfo()
            modalObj.swedishPrice = item.validatedValue("swedish_price", expected: "" as AnyObject) as! String
            modalObj.upgradePrice = item.validatedValue("upgrade_price", expected: "" as AnyObject) as! String
            modalObj.subTotal = item.validatedValue("subtotal", expected: "" as AnyObject) as! String
            modalObj.totalAmount = item.validatedValue("total_amount", expected: "" as AnyObject) as! String

            resultArray.append(modalObj)
        }
         return resultArray
    }

    class func therapistPayoutDataArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
            modalObj.swedishPrice = item.validatedValue("swedish_price", expected: "" as AnyObject) as! String
            modalObj.upgradePrice = item.validatedValue("upgrade_price", expected: "" as AnyObject) as! String
            modalObj.payAmount = item.validatedValue("total_payout", expected: "" as AnyObject) as! String

            resultArray.append(modalObj)
        }
        return resultArray
    }

    class func productsArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()

            modalObj.name = item.validatedValue("type_name", expected: "" as AnyObject) as! String
            modalObj.id = item.validatedValue("id", expected: "" as AnyObject) as! String
            modalObj.productsDetails += UserInfo.productsDetailsArray(responseArray: item.validatedValue("product_details", expected: [] as AnyObject) as! Array<AnyObject> as! Array<Dictionary<String, Any>>)
            resultArray.append(modalObj)
        }

        return resultArray
    }

    class func productsDetailsArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.name = item.validatedValue("product_name", expected: "" as AnyObject) as! String
            modalObj.price = item.validatedValue("product_price", expected: "" as AnyObject) as! String
            modalObj.productImg = modalObj.imgURL + "assets/modules/user/images/redeem_product/" + (item.validatedValue("product_image", expected: "" as AnyObject) as! String)
            modalObj.id = item.validatedValue("id", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
      //  "http://localhost/therapy/assets/modules/user/images/redeem_product/bracelet1.jpg"
        return resultArray
    }
    
    class func productsDetailsArrayLoyalityPoints(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.price = item.validatedValue("price", expected: "" as AnyObject) as! String
            modalObj.productImg = modalObj.imgURL + "assets/modules/user/images/redeem_product/" + (item.validatedValue("product_image", expected: "" as AnyObject) as! String)
            modalObj.id = item.validatedValue("id", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
        return resultArray
    }
    
    class func refferalTransactionArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        
        for item in responseArray {
            let modalObj = UserInfo()
            
            modalObj.paymentDate = item.validatedValue("payment_date", expected: "" as AnyObject) as! String
            modalObj.rewardPoint = item.validatedValue("reward_point", expected: "" as AnyObject) as! String
            modalObj.subsPrice =  item.validatedValue("subscription_price", expected: "" as AnyObject) as! String
            modalObj.email = item.validatedValue("reffaral_email", expected: "" as AnyObject) as! String
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String

            resultArray.append(modalObj)
        }
        return resultArray
    }
    
    
    
    class func retailPlanArray(responseArray: Array<Dictionary<String, Any>>) -> Array<UserInfo> {
        var resultArray = [UserInfo]()
        for item in responseArray {
            let modalObj = UserInfo()
            modalObj.id = item.validatedValue("id", expected: "" as AnyObject) as! String
            modalObj.name = item.validatedValue("pack_name", expected: "" as AnyObject) as! String
            modalObj.price =  item.validatedValue("price", expected: "" as AnyObject) as! String
            modalObj.content = item.validatedValue("content", expected: "" as AnyObject) as! String
            modalObj.colorCode = item.validatedValue("color_code", expected: "" as AnyObject) as! String
            modalObj.payAmount = item.validatedValue("purchase_price", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
        return resultArray
    }
    
}
