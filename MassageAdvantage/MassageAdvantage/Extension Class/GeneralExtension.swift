
import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIWindow {
    
    static var currentController: UIViewController? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        return appDelegate?.window?.currentController
    }
    
    var currentController: UIViewController? {
        if let vc = self.rootViewController {
            return getCurrentController(vc: vc)
        }
        return nil
    }
    
    func getCurrentController(vc: UIViewController) -> UIViewController {
        
        if let pc = vc.presentedViewController {
            return getCurrentController(vc: pc)
        }
        else if let nc = vc as? UINavigationController {
            if nc.viewControllers.count > 0 {
                return getCurrentController(vc: nc.viewControllers.last!)
            } else {
                return nc
            }
        }
            
        else {
            return vc
        }
    }
}

// MARK:- Array Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
extension Array {
    func contains<T>(obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
}

extension Data {
    
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
    
    
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

enum Type : String {
    case vertical
    case horizontal
}

func image(fromLayer layer: CALayer) -> UIImage {
    UIGraphicsBeginImageContext(layer.frame.size)
    
    layer.render(in: UIGraphicsGetCurrentContext()!)
    
    let outputImage = UIGraphicsGetImageFromCurrentImageContext()
    
    UIGraphicsEndImageContext()
    
    return outputImage!
}

extension UIView {
        func round(corners: UIRectCorner, radius: CGFloat) {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    
        func roundCorners(cornerRadius: Double) {
            self.layer.cornerRadius = CGFloat(cornerRadius)
            self.clipsToBounds = true
        }
        

    
    func setGradient(topGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            //            gradientLayer.removeFromSuperlayer()
        }
    }
    
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    func setBorder(color:UIColor, borderWidth:CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
        self.clipsToBounds = true
    }
    
    func generateImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
    }
    
    func shadow(_ color: UIColor = UIColor.lightGray) {
        self.layer.shadowColor = color.cgColor;
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 3
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
    }
    
    func aroundShadow(_ color: UIColor = UIColor.lightGray) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 3
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 6, height: 6)
    }
    
}

// MARK:- UILabel Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
extension UILabel {
    ///Find the index of character (in the attributedText) at point

    func blur(){
            let  blurRadius:CGFloat = 5.1
            
            UIGraphicsBeginImageContext(bounds.size)
            layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            let blurFilter = CIFilter(name: "CIGaussianBlur")
            blurFilter?.setDefaults()
            let imageToBlur = CIImage(cgImage: (image?.cgImage)!)
            blurFilter?.setValue(imageToBlur, forKey: kCIInputImageKey)
            blurFilter?.setValue(blurRadius, forKey: "inputRadius")
            let outputImage: CIImage? = blurFilter?.outputImage
            let context = CIContext(options: nil)
            let cgimg = context.createCGImage(outputImage!, from: (outputImage?.extent)!)
            layer.contents = cgimg!
        }
    
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)
        
        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
}
//MARK:- Date Extension
extension Date {
    
    func offsetFrom(date : Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);
        
        
        if let day = difference.day, day          > 0 { return "/" }
        return ""
    }
    

    func getWeekDayFromDay() -> String {
        let dateFormatter = DateFormatter()
        return dateFormatter.weekdaySymbols[Calendar.current.component(.weekday, from:self)-1]
    }
    
    func getTimeFromCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.string(from: self)
    }
    
    func checkTodayIsWeekDayORWeekend() -> String {
        let weekday = Calendar.current.component(.weekday, from: self)
        return weekday == 0 || weekday == 6 ? "Weekend" : "WeekDay"
    }
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date)) yrs ago"   }
        if months(from: date)  > 0 { return "\(months(from: date)) months ago"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date)) w ago"   }
        if days(from: date)    > 0 { return "\(days(from: date)) d ago"    }
        if hours(from: date)   > 0 { return "\(hours(from: date)) hrs ago"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date)) mins ago" }
        if seconds(from: date) > 0 { return "\(seconds(from: date)) sec ago" }
        return "just now"
    }
}

extension UITextField {
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.layer.shadowOpacity = 0.75
        self.layer.shadowRadius = 0.0
        self.setLeftPaddingPoints(12)

    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
extension UIButton {
    func setShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 0.0
    }
}

//// MARK:- UIImageView Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//extension UIImageView {
//    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>> Changing icon color according to theme <<<<<<<<<<<<<<<<<<<<<<<<*/
//    func setColor(_ color:UIColor) {
//        
//        if let image = self.image {
//            var s = image.size // CGSize
//            s.height *= image.scale
//            s.width *= image.scale
//            
//            UIGraphicsBeginImageContext(s)
//            
//            var contextRect = CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: s)
//            
//            // Retrieve source image and begin image context
//            let itemImageSize = s //CGSize
//            
//            let xVal = (contextRect.size.width - itemImageSize.width)/2
//            let yVal = (contextRect.size.height - itemImageSize.height)
//            
//            //let itemImagePosition = CGPoint(x: ceilf(xFloatVal), y: ceilf(yVal)) //CGPoint
//            
//            let itemImagePosition = CGPoint(x: xVal, y: yVal) //CGPoint
//            
//            UIGraphicsBeginImageContext(contextRect.size)
//            
//            let c = UIGraphicsGetCurrentContext() //CGContextRef
//            
//            // Setup shadow
//            // Setup transparency layer and clip to mask
//            c!.beginTransparencyLayer(auxiliaryInfo: nil)
//            c!.scaleBy(x: 1.0, y: -1.0)
//            
//            //CGContextRotateCTM(c, M_1_PI)
//            
//            c!.clip(to: CGRect(x: itemImagePosition.x, y: -itemImagePosition.y, width: itemImageSize.width, height: -itemImageSize.height), mask: image.cgImage!)
//            
//            // Fill and end the transparency layer
//            let colorSpace = color.cgColor.colorSpace //CGColorSpaceRef
//            let model = colorSpace!.model //CGColorSpaceModel
//            
//            let colors = color.cgColor.components
//            
//            if (model == .monochrome) {
//                c!.setFillColor(red: (colors?[0])!, green: (colors?[0])!, blue: (colors?[0])!, alpha: (colors?[1])!)
//            } else {
//                c!.setFillColor(red: (colors?[0])!, green: (colors?[1])!, blue: (colors?[2])!, alpha: (colors?[3])!)
//            }
//            
//            contextRect.size.height = -contextRect.size.height
//            contextRect.size.height -= 15
//            c!.fill(contextRect)
//            c!.endTransparencyLayer()
//            
//            let img = UIGraphicsGetImageFromCurrentImageContext() //UIImage
//            
//            let img2 = UIImage(cgImage: img!.cgImage!, scale: image.scale, orientation: image.imageOrientation)
//            
//            UIGraphicsEndImageContext()
//            
//            self.image = img2
//            
//        } else {
//            Debug.log("Unable to chage color of image. Image not found")
//        }
//    }
//}
extension UIImage {

    func fixOrientationAndResize(width:CGFloat,height:CGFloat) -> UIImage? {
        
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: height)))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        
        if self.imageOrientation == UIImage.Orientation.up {
            return result
        }
        //        UIGraphicsBeginImageContextWithOptions(result.size, false, result.scale)
        //        self.draw(in: CGRect(x: 0, y: 0, width: result.size.width, height: result.size.height))
        //        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
        //            UIGraphicsEndImageContext()
        //            return normalizedImage
        //        } else {
        return result
        //}
    }
}
// MARK:- Dictionary Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
extension Dictionary {
    
    func validatedValue(_ key: Key, expected: AnyObject) -> AnyObject {
        
        // checking if in case object is nil
        
        if let object = self[key] {
            
            // added helper to check if in case we are getting number from server but we want a string from it
            if object is NSNumber && expected is String {
                
                //logInfo("case we are getting number from server but we want a string from it")
                
                return "\(object)" as AnyObject
            }
                
                // checking if object is of desired class
            else if ((object as AnyObject) .isKind(of: expected.classForCoder) == false) {
                //logInfo("case // checking if object is of desired class....not")
                
                return expected
            }
                
                // checking if in case object if of string type and we are getting nil inside quotes
            else if object is String {
                if ((object as! String == "null") || (object as! String == "<null>") || (object as! String == "(null)")) {
                    //logInfo("null string")
                    return "" as AnyObject
                }
            }
            
            return object as AnyObject
        }
        else {
            
            if expected is String || expected as! String == "" {
                return "" as AnyObject
            }
            
            return expected
        }
    }
}

// MARK:- UIColor Extension
extension UIColor {
    //    class func appYellowColor() -> UIColor {
    //        return RGBA(r: 238, g: 92, b: 48, a: 1)
    //    }
    class func appBlueColor() -> UIColor {
        return UIColor(red: 45.0/255.0, green: 195.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        
    }
        //MARK:-Change color to RGB
    class  func hexStringToUIColor(hex:String,alpha:Float) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
}

// MARK:- NSDictionary Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
extension NSDictionary {
    
    func toNSData() -> NSData {
        return try! JSONSerialization.data(withJSONObject: self, options: []) as NSData
    }
    
//    func toJsonString() -> String {
//        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
//        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
//        return jsonString
//    }
}

extension UITableView {
    
    func reloadData(completion: @escaping ()->()) {
            UIView.animate(withDuration: 0, animations: { self.reloadData() })
            { _ in completion() }
        }
    
    func setEmptyText(status: Bool , text : String) {
        DispatchQueue.main.async {
            let view = UIView()
            let titleLabel = UILabel.init(frame: CGRect.init(x: 0, y: self.layer.bounds.size.height / 2 - 50 , width: self.layer.bounds.size.width, height: 100))
            titleLabel.numberOfLines = 2
            titleLabel.textAlignment = .center
            titleLabel.textColor = UIColor.gray
            titleLabel.text = text
            
            view.addSubview(titleLabel)
            
            if status {
                self.backgroundView = view
            }else {
                self.backgroundView = UIView()
            }
            super.layoutIfNeeded()
        }
    }
    
    func setEmptyImage(status: Bool) {
        DispatchQueue.main.async {
            let view = UIView()
            let dataImageView = UIImageView.init(frame: CGRect.init(x: self.layer.bounds.size.width / 2 - 75, y: self.layer.bounds.size.height / 2 - 50 , width: 150, height: 150))
            
            dataImageView.image = UIImage(named: "NoResultFound")
            view.addSubview(dataImageView)
            
            if status {
                self.backgroundView = view
            }else {
                self.backgroundView = UIView()
            }
            super.layoutIfNeeded()
        }
        
    }
    
    func reloadWithAnimation() {
        self.reloadData()
        let tableViewHeight = self.bounds.size.height
        let cells = self.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.0, delay: 0.1 * Double(delayCounter),usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}

extension UICollectionView {
    
    func setEmptyText(status: Bool) {
        DispatchQueue.main.async {
            let view = UIView()
            let titleLabel = UILabel.init(frame: CGRect.init(x: 0, y: self.layer.bounds.size.height / 2 - 50 , width: self.layer.bounds.size.width, height: 100))
            titleLabel.numberOfLines = 2
            titleLabel.textAlignment = .center
            titleLabel.textColor = UIColor.gray
            titleLabel.text = "OOPS!\nNO RECORD FOUND"
            
            view.addSubview(titleLabel)
            
            if status {
                self.backgroundView = view
            }else {
                self.backgroundView = UIView()
            }
            super.layoutIfNeeded()
        }
        
    }
    
    func setEmptyImage(status: Bool) {
        DispatchQueue.main.async {
            let view = UIView()
            let dataImageView = UIImageView.init(frame: CGRect.init(x: self.layer.bounds.size.width / 2 - 50, y: self.layer.bounds.size.height / 2 - 50 , width: 100, height: 100))
            
            dataImageView.image = UIImage(named: "NoResultFound")
            view.addSubview(dataImageView)
            
            if status {
                self.backgroundView = view
            }else {
                self.backgroundView = UIView()
            }
            super.layoutIfNeeded()
        }
        
    }
    
}
/////======
@IBDesignable
class Gradient: UIView {
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
