//import Foundation
//import CoreLocation
//
////protocol LocationManagerDelegate: class {
////    func locationManagerDidUpdateLocation(_ locationManager: LocationManager, location: CLLocation)
////    func locationManagerDidUpdateHeading(_ locationManager: LocationManager, heading: CLLocationDirection, accuracy: CLLocationDirection)
////}
//
/////Handles retrieving the location and heading from CoreLocation
/////Does not contain anything related to ARKit or advanced location
//
//class LocationManager: NSObject,CLLocationManagerDelegate {
//
//    var locationManager: CLLocationManager!
//    var seenError : Bool = false
//
//    func startTracking() {
//
//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        self.locationManager.distanceFilter = 10.0
//
//        locationManager.requestAlwaysAuthorization()
//        locationManager.startUpdatingLocation()
//    }
//
//    private func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
//        locationManager.stopUpdatingLocation()
//        if ((error) != nil) {
//            if (seenError == false) {
//                seenError = true
//                print(error)
//            }
//        }
//    }
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        let lat : String = locValue.latitude.description
//        let lng : String = locValue.longitude.description
//        NSUSERDEFAULT.set(lat, forKey: kLat)
//        print(lat,lng)
//        AlertController.alert(title: lat,message: lng)
//      
//        NSUSERDEFAULT.set(lng, forKey: kLong)
//    }
//}
