//
//  ValidationClass.swift
//  YastaaProvider
//
//  Created by Shivam_Mobiloitte on 01/05/18.
//  Copyright © 2018 Manoj_Mobiloitte. All rights reserved.
//

import UIKit
class ValidationClass: NSObject {
    
    // MARK: ----- constant messages -----
    
    // --- email validation message ---
    static let vcErrorSelectService                          =  "Pleae select service"
    static let vcErrorTheripistName                          =  "Pleae select theripist name"
    static let vcErrorLocation                               =  "Pleae select location"
    static let vcErrorSelecDistance                          =  "Pleae select distance"

    static let vcErrorEmail                           =  "Please enter email address."
    static let vcErrorValidEmail                      =  "Please enter valid email address."
    static let vcErrorPassword                        =  "Please enter password."
    static let vcErrorValidPassword                   =  "Password must contain at least 5 characters."
    static let vcErrorConfirmPassword                 =  "Please enter confirm password."
    static let vcErrorValidConfirmPassword            =  "Confirm Password must contain at least 5 characters."
    
    
    // --- name validation message ---
    static let vcErrorName                            =  "Please enter name."
    static let vcErrorValidName                       =  "Please enter valid name."
    
    
    static let vcErrorLName                            =  "Please enter last name."
    static let vcErrorValidLName                       =  "Please enter valid last name."
    static let vcCouponCode                           =  "Please enter voucher code."
    
    // --- phone number validation message ---
    static let vcErrorPhoneNumber                     =  "Please enter phone number."
    static let vcErrorValidPhoneNumber                =  "Please enter valid phone number."
    // --- password validation message ---
    static let vcErrorOldPassword                     =  "Please enter old password."
    static let vcErrorValidOldPassword                =  "Old Password must contain at least 5 characters."
    static let vcErroNewPassword                      =  "Please enter new password."
    static let vcErrorValidNewPassword                =  "New password must contain at least 5 characters."
    static let vcErrorMismatchPasswrdAndConPassword   =  "New password and old password must be same."//"Password and confirm password does not match."
    
    // --- Submit a query---
    static let vcErrorSubmitQuery = "Please submit query"
    
    // --- Reset Password
    
    static let vcErrorMismatchNewPasswrdAndConPassword   =  "Confirm password and new password must be same."//"Password and confirm password does not match."
    static let vcErrorMismatchNewPasswrdAndConPasswordForReset   =  "New password and old password can not be same."
    
    static let vcErrorMismatchPasswrdAndConPasswordForReset   =  "Confirm password and new password must be same."
    static let vcErrorOTP = "Please enter OTP"
    static let vcErrorValidOTP = "Please enter valid OTP"


    

    // MARK: --------------------- UIVIEWCONTROLLER SPECIFIC VALIDATION ---------------
    
    // MARK: Login Validation
    class func verifyFieldForLogin(email : String, password : String) -> (String, Bool, Int) {
        
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        // -- verify email/username
        let emailVerification = ValidationClass.verifyEmail(text: email)
        let passwordVerification = ValidationClass.verifyPassword(password: password)
        
        if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        } else if passwordVerification.1 == false {
            verifyObj.message = passwordVerification.0
            verifyObj.isVerified = passwordVerification.1
            verifyObj.index = 1
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    // MARK: Register Validation
    class func verifyFieldForRegisterUser(name : String,email : String, phone : String, password : String) -> (String, Bool, Int) {
        
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        // -- verify email/username
        let emailVerification = ValidationClass.verifyEmail(text: email)
        let phoneVerification = ValidationClass.verifyPhoneNumber(text: phone)
        let passwordVerification = ValidationClass.verifyPassword(password: password)
      
        if name.count == 0 {
            verifyObj.message = vcErrorName
        }
      else  if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        }
        
        else if phoneVerification.1 == false {
            verifyObj.message = phoneVerification.0
            verifyObj.isVerified = phoneVerification.1
            verifyObj.index = 0
        }
        
        else if passwordVerification.1 == false {
            verifyObj.message = passwordVerification.0
            verifyObj.isVerified = passwordVerification.1
            verifyObj.index = 1
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    
    
    
    class func verifyFieldForRetailInfo(name : String,email : String, phone : String) -> (String, Bool, Int) {
        
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        // -- verify email/username
        let emailVerification = ValidationClass.verifyEmail(text: email)
        let phoneVerification = ValidationClass.verifyPhoneNumber(text: phone)
        
        if name.count == 0 {
            verifyObj.message = vcErrorName
        }
        else  if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        }
            
        else if phoneVerification.1 == false {
            verifyObj.message = phoneVerification.0
            verifyObj.isVerified = phoneVerification.1
            verifyObj.index = 0
        }
        else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    // MARK: Refferal Validation
    class func verifyFieldForRefferalProg(name : String, email : String) -> (String, Bool, Int) {
        
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        // -- verify email/username
        let emailVerification = ValidationClass.verifyEmail(text: email)
        
        if name.count == 0 {
            verifyObj.message = vcErrorName
        }
        
       else if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        }
        else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    
    // MARK:  Plan
    class func verifyFieldForPlanMassage(service : String, name : String,location : String,distance : String) -> (String, Bool, Int) {
        
        var verifyObj = (message : "", isVerified : false, index: 0)
        if service.count == 0 {
             verifyObj.message = vcErrorSelectService
        }else if name.count == 0 {
            verifyObj.message = vcErrorTheripistName
        }else if location.count == 0 {
            verifyObj.message = vcErrorLocation
        }else if distance.count == 0 {
            verifyObj.message = vcErrorSelecDistance
        }else {
             verifyObj.isVerified = true
        }
        return verifyObj
    }
    // MARK: Login Validation
    class func verifyFieldForCouponCode(couponCode : String) -> (String, Bool, Int) {
        
        var verifyObj = (message : "", isVerified : false, index: 0)
        if couponCode.count == 0 {
            verifyObj.message = vcCouponCode
        }else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    // MARK: -----  password and Confirm password -----
    class func verifyNewPasswordAndConfirmPassword(newPassword : String, confirmPassword : String) -> (String, Bool, Int) {
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        if newPassword.length == 0 {
            verifyObj.message = vcErroNewPassword
            verifyObj.index = 0
        } else if newPassword.length < 5 {
            verifyObj.message = vcErrorValidNewPassword
            verifyObj.index = 0
        }
        else if confirmPassword.length == 0 {
            verifyObj.message = vcErrorConfirmPassword
            verifyObj.index = 1
        }
            
        else if confirmPassword.length < 5 {
            verifyObj.message = vcErrorValidConfirmPassword
            verifyObj.index = 1
        }
            
        else if confirmPassword != newPassword {
            verifyObj.message = vcErrorMismatchPasswrdAndConPasswordForReset
            verifyObj.index = 1
        }
            
        else {
            verifyObj.isVerified = true
        }
        
        return verifyObj
    }
    
}

// MARK: ---- extension to define individual verfication methods
extension ValidationClass {
    
    // MARK: ----- email validation -----
    class func verifyEmail(text : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if text.count == 0 {
            verifyObj.message = vcErrorEmail
        } else if !text.isEmail {
            verifyObj.message = vcErrorValidEmail
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    
    // MARK: ----- name validation -----


    
    // MARK: ----- mobile number -----
    class func verifyPhoneNumber(text : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if text.length == 0 {
            verifyObj.message = vcErrorPhoneNumber
        } else if text.length < 8 { //< 8 { if country code option is also on screen
            verifyObj.message = vcErrorValidPhoneNumber
        } else if text.isContainsAllZeros() {
            verifyObj.message = vcErrorValidPhoneNumber
            
        } else if !text.containsNumberOnly() {
            verifyObj.message = vcErrorValidPhoneNumber
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    
    
    
    // MARK: ----- email / username validation -----
    class func verifyEmailOrUsername(text : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if text.length == 0 {
            verifyObj.message = vcErrorEmail
            
        } else if text.contains("@") && !text.trimWhiteSpace.isEmail {
            verifyObj.message = vcErrorValidEmail
            
        } else if !text.contains("@") && text.length < 2  {
            verifyObj.message = vcErrorValidEmail
            
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
//    // MARK: ----- mobile number -----
//    class func verifyPhoneNumber(text : String) -> (String, Bool) {
//        var verifyObj = (message : "", isVerified : false)
//            if text.length == 0 {
//                verifyObj.message = vcErrorPhoneNumber
//                
//            } else if text.first != "0" { //< 8 { if country code option is also on screen
//                verifyObj.message = vcFirstDigitShouldBeZero
//                
//            } else if text.isContainsAllZeros() {
//                verifyObj.message = vcErrorValidPhoneNumber
//                
//            } else if !text.containsNumberOnly() {
//                verifyObj.message = vcErrorValidPhoneNumber
//            } else {
//                verifyObj.isVerified = true
//            }
//            return verifyObj
//        //}
//        
//    }
    
    
    // MARK: Forgot Password Validation
    class func verifyFieldForForgotPassword(email : String) -> (String, Bool, Int) {
        var verifyObj = (message : "", isVerified : false, index: 0)
        // -- verify email
        let emailVerification = ValidationClass.verifyEmail(text: email)
        if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        }else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    // MARK: ----- password -----
    class func verifyPassword(password : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if password.length == 0 {
            verifyObj.message = vcErrorPassword
            
        } else if password.length < 4 {
            verifyObj.message = vcErrorValidPassword
            
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    // MARK: ----- Verify For Empty -----
    class func verifyPassword(field : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        
        if field.length == 0 {
            verifyObj.message = vcErrorPassword
            
        } else {
            verifyObj.isVerified = true
        }
        
        return verifyObj
    }
}
