//
//  AppUtility.swift
//  Molla Kuqe
//
//  Created by ETHANEMAC on 22/10/18.
//  Copyright © 2018 ETHANE TECHNOLOGIES PVT. LTD. All rights reserved.
//

import UIKit

let imageExtensions = ["png", "jpg", "jpeg", "gif"]

let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let Window_Width = UIScreen.main.bounds.size.width
let Window_Height = UIScreen.main.bounds.size.height
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
// MARK: - Helper functions
func RGBA(_ r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) -> UIColor {
    return UIColor(red: (r/255.0), green: (g/255.0), blue: (b/255.0), alpha: a)
}

func resizeImage(imageUrl : String ,/*image: UIImage,*/ newWidth: CGFloat) -> UIImage {
    if imageUrl.length > 0 {
        let url = URL(string: imageUrl)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        //        UIImage(data: data!)
        let image = UIImage(data: data!)!
        
        let scale = (newWidth) / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    } else {
        return UIImage(named: "BannerPostPlaceholder")!
    }
}

func getUserDefaultValue(_ forKey :String) -> String {
    if let string = UserDefaults.standard.value(forKey: forKey) as? String {
        return string
    }
    return ""
}

//MARK:-  Date Format
func formattedDateFromString(dateString: String, withFormat format: String,inputFormat:String) -> String? {
    
    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = inputFormat
    if let date = inputFormatter.date(from: dateString) {
        
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = format
        return outputFormatter.string(from: date)
    }
    return nil
}

func delay(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}

func storyBoardForName(name:String) -> UIStoryboard{
    return UIStoryboard.init(name: name, bundle: nil)
}

func attributedTextWithSpacing(text:String) -> NSMutableAttributedString {
    let attributedString = NSMutableAttributedString(string: text)
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 2
    attributedString.addAttribute(kCTParagraphStyleAttributeName as NSAttributedString.Key, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
    return attributedString;
}

func setPlaceholder(placeholder:String, color : UIColor = .white) -> NSAttributedString? {
    let attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor:color])
    return attributedPlaceholder
}


class AppUtilities: NSObject {

    class func JSONFromFile(path :String)-> Array<Any> {
        
        let path : String = Bundle.main.path(forResource: path, ofType: "json")!
        
        let data =  NSData( contentsOfFile: path )
        
        return [try! JSONSerialization.jsonObject(with: data! as Data, options:[]) ]
        
    }
}
