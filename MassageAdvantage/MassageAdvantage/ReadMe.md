TECHNICAL SPECIFICATION
1. Min version:                               iOS 11.0
2. IDE: XCode:                               10.1*
3. Design pattern:                          MVC
4. Programming language usage: Swift 5
5. View spefication:                       StoryBoards
6. Layout support:                         Autolayout + Autoresizing + size class
7. Compatibility:                            All iPhone

