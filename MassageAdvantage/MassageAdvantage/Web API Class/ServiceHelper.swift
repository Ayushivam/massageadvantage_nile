//
//  ServiceHelper.swift
//  Template
//  Created by Chandan Mishra on 05/05/18.
//  Copyright © 2016 Nile. All rights reserved.
//
//

import UIKit
import CoreLocation

//import Alamofire
// Staging URL
let stagingURL = "https://www.massageadvantage.com/"
//"https://www.niletechinnovations.com/projects/massage/"

let timeoutInterval:Double = 45
var apiNames = ""
enum loadingIndicatorType: CGFloat {
    case `default`  = 0 // showing indicator & text by disable UI
    case simple  = 1 // // showing indicator only by disable UI
    case noProgress  = 2 // without indicator by hdisable UI
    case smoothProgress  = 3 // without indicator by enable UI i.e No hud
}

enum MethodType: CGFloat {
    case get  = 0
    case post = 1
    case put  = 2
    case delete = 3
    case patch  = 4  
}

class ServiceHelper: NSObject {
    
    //MARK:- Public Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    class func request(_ parameterDict: [String: Any], method: MethodType, apiName: String, hudType: loadingIndicatorType, completionBlock: @escaping (AnyObject?, Error?, Int) -> Void) ->Void {
        //>>>>>>>>>>> create request
        let url = requestURL(method, apiName: apiName, parameterDict: parameterDict)
        var request = URLRequest(url: url)
        request.httpMethod = methodName(method)
        request.timeoutInterval = timeoutInterval
        let jsonData = body(method, parameterDict: parameterDict)
        request.httpBody = getURLForBody(parameterDict: parameterDict,methodName:method)
         request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        apiNames = apiName
        Debug.log("\n\n Request URL  >>>>>>\(url)")
        Debug.log("\n\n Request Header >>>>>> \n\(request.allHTTPHeaderFields.debugDescription)")
        Debug.log("Content-Length >>> \(String (jsonData.count))")
        Debug.log("\n\n Request Parameters >>>>>>\n\(parameterDict.toJsonString())")
        request.perform(hudType: hudType) { (responseObject: AnyObject?, error: Error?, httpResponse: HTTPURLResponse) in
            DispatchQueue.main.async(execute: {
                completionBlock(responseObject, error, httpResponse.statusCode)
            })
        }
    }
    
    class private func showErrorAlert(errorDict: Dictionary<String, AnyObject>) {
        
        // go to login screen
        var errorTitle = "Authentication Error!"
        let message = "Please login and try again."
        
        if let title = errorDict["error"] as? String {
            errorTitle = title
        }
        
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: errorTitle, message: message, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) -> Void in}
            let loginAction = UIAlertAction(title: "Login", style: .default) { (action) -> Void in
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(loginAction)
            UIWindow.currentController!.present(alertController, animated: true, completion: nil)
        })
    }
    
    //MARK:- Private Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    class fileprivate func methodName(_ method: MethodType)-> String {
        
        switch method {
        case .get: return "GET"
        case .post: return "POST"
        case .delete: return "DELETE"
        case .put: return "PUT"
        case .patch: return "PATCH"
        }
    }
    
    class fileprivate func body(_ method: MethodType, parameterDict: [String: Any]) -> Data {
        
        // Create json with your parameters
        switch method {
        case .post: fallthrough
        case .patch: fallthrough
        case .put: return parameterDict.toData()
        case .get: fallthrough
        default: return Data()
        }
    }
    
    class fileprivate func requestURL(_ method: MethodType, apiName: String, parameterDict: [String: Any]) -> URL {
        var urlString = String()
      
        if apiName == kPayment
        {
            urlString = apiName
        }
        else if apiName == kCheckVersion{
            urlString = apiName
        }
        else{
            urlString = stagingURL + apiName
        }

        switch method {
        case .get,.put,.delete:
            return getURL(apiName, parameterDict: parameterDict)
        case .post: fallthrough
           // fallthrough
        case .patch: fallthrough
        default:
            let original = urlString
            let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            if  encoded == original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let  url = URL(string: encoded!)
            {

            }
            return URL(string: encoded!)!
        }
    }

    class fileprivate func getURL(_ apiName: String, parameterDict: [String: Any]) -> URL {

        var urlString = String()
        if apiName == kPayment{

            urlString =  apiName
        }

        else if apiName == kCheckVersion{
            urlString = apiName
        }
        else{
            urlString = stagingURL + apiName
        }
        
        
        var isFirst = true
        
        for key in parameterDict.keys {
            let object = parameterDict[key]
            if object is NSArray {
                let array = object as! NSArray
                for eachObject in array {
                    var appendedStr = "&"
                    if (isFirst == true) {
                        appendedStr = "?"
                    }
                    urlString += appendedStr + (key) + "=" + (eachObject as! String)
                    isFirst = false
                }
            } else {
                var appendedStr = "&"
                if (isFirst == true) {
                    appendedStr = "?"
                }
                let parameterStr = parameterDict[key] as! String
                urlString += appendedStr + (key) + "=" + parameterStr
            }
            isFirst = false
        }

        let strUrl = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        return URL(string:strUrl!)!
    }

    class fileprivate func getURLForBody(parameterDict: [String: Any],methodName : MethodType) -> Data {

        var bodyString = String()
        var isFirst = true

        if methodName != .get {
            
            for key in parameterDict.keys {
                
                let object = parameterDict[key]
                
                if object is NSArray {
                    let array = object as! NSArray
               
                    for eachObject in array {
                        var appendedStr = "&"
                        if (isFirst == true) {
                            appendedStr = ""
                        }
                        bodyString += appendedStr + (key) + "=" + (eachObject as! String)
                        isFirst = false
                    }
                }
                
                else
                {
                    var appendedStr = "&"
                    if (isFirst == true) {
                        appendedStr = ""
                    }
                    let parameterStr = parameterDict[key] as! String
                    bodyString += appendedStr + (key) + "=" + parameterStr
                }
                isFirst = false
            }
        }
        let strUrl = bodyString.data(using:String.Encoding.utf8, allowLossyConversion: false)
        return strUrl!
    }

    class func showHud() {
        let attribute = RappleActivityIndicatorView.attribute(style: RappleStyleCircle, tintColor: .white, screenBG: nil, progressBG: .black, progressBarBG: .lightGray, progreeBarFill: .yellow)
        RappleActivityIndicatorView.startAnimating(attributes: attribute)
    }

    class func hideHud() {
        RappleActivityIndicatorView.stopAnimation()
        RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: "", completionTimeout: 1.0)
    }

    class func hideAllHuds(_ status: Bool, type: loadingIndicatorType) {

        if (type == .smoothProgress) {
            return
        }
        DispatchQueue.main.async(execute: {
            if status{
                ServiceHelper.hideHud()
            }else {
                ServiceHelper.showHud()
            }
            //                hud?.hide(animated: true, afterDelay: 0.3)
            //            }
        }
        )
    }

    static  func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
    static func updateProfileInfo (parameterDict: [String: Any],imageDta:Data,haveImage:Bool, callback:@escaping ( _ data: NSDictionary?,  _ error: NSError? ) -> Void)
    {
        let url =  stagingURL + "api/update_profile"
        let params = parameterDict
        
        upload(
            multipartFormData: { multipartFormData in
                for (key, value) in params
                {
                 //   multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)! , withName: key)
                    
                   // multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)! , withName: key)
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue)! , withName: key)
                }
                if haveImage{
                    
                    multipartFormData.append(imageDta , withName:"file",fileName:ServiceHelper.randomString(length: 10) + ".png" , mimeType: "image/jpg")
                }
        },
            to: url, headers: nil,
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _ ,_  ):
                    upload.responseJSON { response in
                        guard response.result.error == nil
                            else {
                                print("Error for updateUserInfo :\(url):\(response.result.error!)")
                                print(response.result.error!)
                                callback(nil , response.result.error! as NSError? )
                                return
                        }
                        if let JSON = response.result.value {
                            if let result = JSON as? Dictionary<String, AnyObject> {
                                print("Response for updateUserInfo :\(url):\(JSON)")
                                callback(result as NSDictionary , nil )
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
    
    
    static  func getAddressString(lat:Double,long:Double, completionBlock: @escaping (String) -> Void) -> Void {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = long
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        var addressString : String = ""
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                    completionBlock(addressString)
                }
        })
    }

}

extension URLRequest  {
    func perform(hudType: loadingIndicatorType, completionBlock: @escaping (AnyObject?, Error?, HTTPURLResponse) -> Void) -> Void {
        //hud_type = hudType
        if (APPDELEGATE.isReachable == false) {
            AlertController.alert(title: "Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
            return
        }
        ServiceHelper.hideAllHuds(false, type: hudType)
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        //var session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        
        let task = session.dataTask(with: self, completionHandler: {
            (data, response, error) in
            
            ServiceHelper.hideAllHuds(true, type: hudType)
            Debug.log("Error ==== \(error.debugDescription)")
            if let response = response {
                let httpResponse = response as! HTTPURLResponse
                let responseCode = httpResponse.statusCode
              //  let contentType = httpResponse.allHeaderFields["Content-Type"] as? String
            //    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                _ = httpResponse.allHeaderFields
                Debug.log("Response Code : \(responseCode))")
                if let error = error {
                    Debug.log("\n\n error  >>>>>>\n\(error)")
                    completionBlock(nil, error, httpResponse)
                } else {
                    var responseStr = ""
                    if let responseString = NSString.init(data: data!, encoding: String.Encoding.utf8.rawValue) {
                        Debug.log("Response String : \n \(responseString)")
                        responseStr = "\(responseString)"
                    }
                    do {
                        if(apiNames == kPayment)
                        {
                           // let result = try JSONSerialization.jsonObject(with: data!, options:[])
                            let result = ["responseCode":"200","ResponseString":responseStr]
                            completionBlock(result as AnyObject?, nil, httpResponse)
                            
                            
                        }
                        else{
                            let result = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                            completionBlock(result as AnyObject?, nil, httpResponse)
                        }
                        
                    } catch {                    //   Debug.log("\n\n error in JSONSerialization")
                        Debug.log("\n\n error  >>>>>>\n\(error)")
                        if responseCode == 200 && apiNames != kPayment{
                            
                            let result = ["responseCode":"200","ResponseString":responseStr]
                            completionBlock(result as AnyObject?, nil, httpResponse)
                        }
                    }
                }
            } else {
                AlertController.alert(title: "Request Timeout!", message: "Please check your internet connection and try again.")
            }
        })
        task.resume()
    }
}

extension NSDictionary {
    func toData() -> Data {
        return try! JSONSerialization.data(withJSONObject: self, options: [])
    }
    
    func toJsonString() -> String {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        return jsonString
    }
}

extension Dictionary {
    
    func toData() -> Data {
        return try! JSONSerialization.data(withJSONObject: self, options: [])
    }
    
    func toJsonString() -> String {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        return jsonString
    }
}

func resolutionScale() -> CGFloat {
    
    return UIScreen.main.scale
}
