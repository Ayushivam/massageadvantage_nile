//
//  StaticScreenViewController.swift
//  Zoloh
//
//  Created by Vibhuti Sharma on 14/02/19.
//  Copyright © 2019 nile. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    
    // MARK: ---  Login Api Handler  ---
    class func apiForLogin(email : String, password : String, completion: @escaping ((Bool) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "email" : email as AnyObject,
            "password" : password as AnyObject,
            "device_type" : "3" as AnyObject,
            "device_id" : UIDevice.current.identifierForVendor?.uuidString as AnyObject,
            "fcm_id" : NSUSERDEFAULT.value(forKey: kFCMToken) as AnyObject,
            ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kLogin, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        let data : Dictionary = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        let loggedIn : String = data.validatedValue("logged_in", expected: "" as AnyObject) as! String
                        let userId : String = data.validatedValue("user_id", expected: "" as AnyObject) as! String
                        let userGroup : String = data.validatedValue("user_group", expected: "" as AnyObject) as! String
                        let parentId : String = data.validatedValue("parent_id", expected: "" as AnyObject) as! String
                        let isSubsCription : String = data.validatedValue("is_subscription", expected: "" as AnyObject) as! String
                        let addMember : String = data.validatedValue("add_member", expected: "" as AnyObject) as! String
                        let soapId : String = data.validatedValue("soap_note_id", expected: "" as AnyObject) as! String
                        let planId : String = data.validatedValue("plan_id", expected: "" as AnyObject) as! String
                        let giftPrice : String = data.validatedValue("gift_price", expected: "" as AnyObject) as! String

                        
                        NSUSERDEFAULT.set(loggedIn, forKey: kLoggedIn)
                        NSUSERDEFAULT.set(userId, forKey: kUserId)
                        NSUSERDEFAULT.set(userGroup, forKey: kUserGroup)
                        NSUSERDEFAULT.set(parentId, forKey: kParentId)
                        NSUSERDEFAULT.set(isSubsCription, forKey: kIsSubs)
                        NSUSERDEFAULT.set(addMember, forKey: kAddmember)
                        NSUSERDEFAULT.set("0", forKey: kcheckLoginType)
                        NSUSERDEFAULT.set(soapId, forKey: kSoapNoteID)
                        NSUSERDEFAULT.set(planId, forKey: kPlanId)
                        NSUSERDEFAULT.set(giftPrice, forKey: kGiftPrice)
                        completion(true)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                         completion(false)
                    }
                   
                } else {

                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    let msg = results.validatedValue("message", expected: false as AnyObject) as! String
                    if msg == "Your account is not verified yet, OTP has been send to your registered email id."
                    {
                        completion(false)
                    }else {
                        completion(false)
                    }
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }

    // MARK: --- Seperate Login Api Handler  ---
    class func apiForSeperateLogin(email : String, password : String, completion: @escaping ((Bool) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "email" : email as AnyObject,
            "password" : password as AnyObject,
            "device_token" : "asdasdasdasd" as AnyObject,
            "device_type" : "iOS" as AnyObject,
            "device_id" : UIDevice.current.identifierForVendor?.uuidString as AnyObject,
            "fcm_id" : "fghfghdfgdfg565dfgdfg4dffg46df" as AnyObject,
            ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kLogin, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        let data : Dictionary = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        let loggedIn : String = data.validatedValue("logged_in", expected: "" as AnyObject) as! String
                        let userId : String = data.validatedValue("user_id", expected: "" as AnyObject) as! String
                        let userGroup : String = data.validatedValue("user_group", expected: "" as AnyObject) as! String
                        let parentId : String = data.validatedValue("parent_id", expected: "" as AnyObject) as! String
                        let isSubsCription : String = data.validatedValue("is_subscription", expected: "" as AnyObject) as! String
                        let addMember : String = data.validatedValue("add_member", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(loggedIn, forKey: kLoggedIn)
                        NSUSERDEFAULT.set(userId, forKey: kUserId)
                        NSUSERDEFAULT.set(userGroup, forKey: kUserGroup)
                        NSUSERDEFAULT.set(parentId, forKey: kParentId)
                        NSUSERDEFAULT.set(isSubsCription, forKey: kIsSubs)
                        NSUSERDEFAULT.set(addMember, forKey: kAddmember)
                        completion(true)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false)
                    }
                    
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    let msg = results.validatedValue("message", expected: false as AnyObject) as! String
                    if msg == "Your account is not verified yet, OTP has been send to your registered email id."
                    {
                        completion(false)
                    }else {
                        completion(false)
                    }
                }
                
            } else {
                
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
                
            }
        }
    }
    
    // MARK: ---  Forgot Password Api Handler  ---
    
    class func apiForForgotPassword(email : String, completion: @escaping ((Bool) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "email" : email as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kForgotPassword, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        AlertController.alert(title: "", message:results.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                            completion(true)
                        })
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }
    
    // MARK: ---  Change Password Api Handler  ---
    class func apiForChangePassword(newPassword : String, confirmNewPassword : String, dict:Dictionary<String,AnyObject>, completion: @escaping ((Bool) -> Void)) {
        let userDict : Dictionary<String, AnyObject> = [
            "new_password" : newPassword as AnyObject,
            "confirm_password" : confirmNewPassword as AnyObject,
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kChangePassword, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        AlertController.alert(title: "", message:results.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                           
                            
                            completion(true)
                        })
                    }
                        
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }
    
    // MARK: ---   Get Select Service Api Handler  ---
    class func apiForGetServiceList(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()

        
        ServiceHelper.request(userDict, method: .get, apiName: kServiceList, hudType: .default) { (result, error, responseCode) in
            var seviceListArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        seviceListArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        

                        completion(true,seviceListArr)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,seviceListArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,seviceListArr)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,seviceListArr)
            }
        }
    }
    
    
    // MARK: ---   Get  Service Name Api Handler  ---
    class func apiForGetServiceName(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetServicesName, hudType: .default) { (result, error, responseCode) in
            var seviceNameListArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        seviceNameListArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        
                        completion(true,seviceNameListArr)
                    }
                    else {
                        completion(false,seviceNameListArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,seviceNameListArr)
                }
            }
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,seviceNameListArr)
            }
        }
    }
    
    // MARK: ---  Search Therapist Api Handler  ---
    class func apiForSearchTherapist(selectService : String, therapistName : String,lat : String,long : String,distance : String,couponType : String ,page: Int,loader:loadingIndicatorType,completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        let userDict : Dictionary<String, AnyObject> = [
            "s" : selectService as AnyObject,
            "n" : therapistName as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : long as AnyObject,
            "d" : distance as AnyObject,
            "coupon_type" : couponType as AnyObject,
            "page" : "\(page)" as AnyObject,
            ]
        
        ServiceHelper.request(userDict, method: .get, apiName: kSearchTherapist, hudType: loader) { (result, error, responseCode) in
            var therapistListArry : Array = Array<AnyObject>()

            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                if status == true {
                        therapistListArry = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        completion(true,therapistListArry)
                    }
                    else {
                        completion(false,therapistListArry)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,therapistListArry)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,therapistListArry)
            }
        }
    }
    
    // MARK: ---  Booking Handler  ---
    class func apiForBookingDetails(slugName : String,coupon:String,completion: @escaping ((Bool,Dictionary<String,AnyObject>,Array<AnyObject>,Array<AnyObject>) -> Void)) {
        
        let currentDate  =  Date()
        print(currentDate)
        let formatted = APPDELEGATE.formattedDateFromString(dateString: "\(currentDate)", withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd HH:mm:ss z")
        
        let userDict : Dictionary<String, AnyObject> = [
           "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
           "coupon"  : coupon as AnyObject,
            "date" : formatted as AnyObject,
            ]
        
        ServiceHelper.request(userDict, method: .get, apiName: kBookingDetails+"/" + slugName, hudType: .default) { (result, error, responseCode) in
            var data : Dictionary = Dictionary<String,AnyObject>()
            var checkOutArray : Array = Array<AnyObject>()
            var reviewArray : Array = Array<AnyObject>()


            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                       data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        
                        checkOutArray = results.validatedValue("checkout", expected: [] as AnyObject) as! Array<AnyObject>
                    
                         reviewArray = results.validatedValue("review", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        completion(true,data,checkOutArray,reviewArray)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,data,checkOutArray,reviewArray)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,data,checkOutArray,reviewArray)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,data,checkOutArray,reviewArray)
            }
        }
    }
    
    
    
    // MARK: ---   Get user Profile Api Handler  ---
    class func apiForGetProfile(userId : String,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : userId as AnyObject,
        ]
        ServiceHelper.request(userDict, method: .post, apiName: kGetProfile, hudType: .default) { (result, error, responseCode) in
            var data : Dictionary = Dictionary<String,AnyObject>()
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                         data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>

                        completion(true,data)
                        
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,data)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,data)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,data)
            }
        }
    }
    
    
    
    // MARK: ---  Loyalty Points  Api Handler   ---
    class func apiForLoyaltyPoints(completion: @escaping ((Bool,Dictionary<String,AnyObject>,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kloyaltiPoints, hudType: .default) { (result, error, responseCode) in
            var data : Dictionary = Dictionary<String,AnyObject>()
            var productsArray : Array = Array<AnyObject>()
            
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        productsArray = data.validatedValue("products", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        
                        completion(true,data,productsArray)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,data,productsArray)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,data,productsArray)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,data,productsArray)
            }
        }
    }
    
    
    // MARK: ---  get Product Price Api Handler   ---
    class func apiForProductPrice(item : String,completion: @escaping ((Bool,Dictionary<String,AnyObject>,Array<AnyObject>,String) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "item" : item as AnyObject,
        ]
        var msg = ""

        
        ServiceHelper.request(userDict, method: .post, apiName: kProductPrice, hudType: .default) { (result, error, responseCode) in
            var data : Dictionary = Dictionary<String,AnyObject>()
            var productsDetailsArray : Array = Array<AnyObject>()

            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        productsDetailsArray = data.validatedValue("product_detail", expected: [] as AnyObject) as! Array<AnyObject>

                        
                        completion(true,data,productsDetailsArray,msg)
                    }
                    else {
                        msg  = results.validatedValue("message", expected: "" as AnyObject) as! String
                        completion(false,data,productsDetailsArray,msg)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,data,productsDetailsArray,msg)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,data,productsDetailsArray,msg)
            }
        }
    }
    
    // MARK: ---   Confirm Purchase API Handler ---
    class func apiForConfirmPurchase(item : String, price : String,completion: @escaping ((Bool,String) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "item" : item as AnyObject,
            "price" : price as AnyObject,
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,

           
            ]
        
        var msg = ""
        ServiceHelper.request(userDict, method: .post, apiName: kConfirmPurchase, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        msg  = results.validatedValue("message", expected: "" as AnyObject) as! String
                        
                        completion(true,msg)
                    }
                    else {
                        msg  = results.validatedValue("message", expected: "" as AnyObject) as! String
                        completion(false,msg)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                }
                
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,msg)
            }
        }
        
    }
    

    // MARK: ---   Refferal Program Api Handler  ---
    class func apiForRefferalProg(name : String, email : String,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "name" : name as AnyObject,
            "email" :  email as AnyObject,
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "user_group" : NSUSERDEFAULT.value(forKey: kUserGroup) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kRefferalProgram, hudType: .default) { (result, error, responseCode) in
            //  var data : Dictionary = Dictionary<String,AnyObject>()
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        completion(true,results)
                        
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,results)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,results)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
            }
        }
    }

    // MARK: ---   Update Profile
    class func apiForUpdateProfile(dict:Dictionary<String,AnyObject>,data:Data,haveImage:Bool,completion: @escaping ((Bool) -> Void)) {
        
        ServiceHelper.hideAllHuds(false, type: loadingIndicatorType.default)
        ServiceHelper.updateProfileInfo(parameterDict: dict, imageDta: data, haveImage: haveImage) { (dict, error) in
            ServiceHelper.hideAllHuds(true, type: loadingIndicatorType.default)
            if error == nil{
                let status = dict!["status"] as! Bool//results.validatedValue("status", expected: false as AnyObject) as! Bool
                if status{
                    completion(true)
                }else {
                    completion(false)
                }
            }
            else {
                completion(false)
            }
        }
    }


    // MARK: ---   Get Members List Api Handler  ---
    class func apiForGetMembersList(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kMemberList, hudType: .default) { (result, error, responseCode) in
            var memberListArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        memberListArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        completion(true,memberListArr)
                    }
                    
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,memberListArr)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,memberListArr)
            }
        }
    }
    
    
    // MARK: ---   ADD Members Api Handler  ---
    class func apiForAddMember(name : String, email : String,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_name" : name as AnyObject,
            "user_email" :  email as AnyObject,
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kAddMember, hudType: .default) { (result, error, responseCode) in
            //  var data : Dictionary = Dictionary<String,AnyObject>()
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        completion(true,results)
                        
                    }
                    else {
                        completion(false,results)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,results)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
            }
        }
    }
    
    
    
    // MARK: ---    Payment summary Api Handler  ---
    class func apiForGetPaymentSummaryList(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kPaymentSummary, hudType: .default) { (result, error, responseCode) in
            var paymentSummArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        paymentSummArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        
                        completion(true,paymentSummArr)
                    }
                    else {
                        completion(false,paymentSummArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,paymentSummArr)
                }
            }
            
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,paymentSummArr)
            }
        }
    }
    
    // MARK: ---   Get subscription List Api Handler  ---
    class func apiForGetSubscriptionList(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kGetSubscription, hudType: .default) { (result, error, responseCode) in
            var subscriptionListArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        subscriptionListArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        
                        completion(true,subscriptionListArr)
                    }
                    else {
                        completion(false,subscriptionListArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,subscriptionListArr)
                }
            }
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,subscriptionListArr)
            }
        }
    }
    
    // MARK: ---   Get booking Appointment List Api Handler  ---
    class func apiForGetBookingAppointmentList(completion: @escaping ((Bool,Array<AnyObject>,String) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        ServiceHelper.request(userDict, method: .post, apiName: kBookingAppointment, hudType: .default) { (result, error, responseCode) in
            var bookingAppointmentListArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        let cancellationCharge : String = results.validatedValue("pay_cancellation_charge", expected: "" as AnyObject) as! String
                        
                        bookingAppointmentListArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        completion(true,bookingAppointmentListArr,cancellationCharge)
                    }
                    else {
                    
                        completion(false,bookingAppointmentListArr,"")
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,bookingAppointmentListArr,"")
                }
            }
                
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,bookingAppointmentListArr,"")
            }
        }
    }
    
    
    
    // MARK: ---  Verify Coupon Code  ---
    class func apiForVerifyCouponCode(couponCode : String, completion: @escaping ((Bool,String,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "coupon" : couponCode as AnyObject,
            
            ]
        
        var msg = ""
        ServiceHelper.request(userDict, method: .post, apiName: kVerifyCoupon, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                         msg  = results.validatedValue("message", expected: "" as AnyObject) as! String
                        let data : Dictionary = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>

                        completion(true,msg,data)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,msg,[:])
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                }
                
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,msg,[:])
            }
        }
    
    }
    
    // MARK: --- Register API  ---
    class func apiForRegisterUser(Name : String,Email : String,Phone : String,Password : String,completion: @escaping ((Bool,String) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "name" : Name as AnyObject,
            "email" : Email as AnyObject,
            "phone" : Phone as AnyObject,
            "password" : Password as AnyObject,

            ]
        var msg = ""
        ServiceHelper.request(userDict, method: .post, apiName: kRegister, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        msg  = results.validatedValue("message", expected: "" as AnyObject) as! String

                        let data : Dictionary = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        let userId : String = data.validatedValue("user_id", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(userId, forKey: kUserId)
                        let loggedIn : String = data.validatedValue("logged_in", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(loggedIn, forKey: kLoggedIn)
                        let userGroup : String = data.validatedValue("user_group", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(userGroup, forKey: kUserGroup)
                        
                        NSUSERDEFAULT.set("0", forKey: kIsSubs)
                        
                        let parentId : String = data.validatedValue("parent_id", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(parentId, forKey: parentId)
                        let addMember : String = data.validatedValue("add_member", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(addMember, forKey: kAddMember)
                        let soapId : String = data.validatedValue("soap_note_id", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(soapId, forKey: kSoapNoteID)
                        let planId : String = data.validatedValue("plan_id", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(planId, forKey: kPlanId)
                        let giftPrice : String = data.validatedValue("gift_price", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(giftPrice, forKey: kGiftPrice)

                        completion(true,msg)
                    }
                    else {
                        let data : Dictionary = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        
                       msg  = data.validatedValue("email", expected: "" as AnyObject) as! String
                        
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,msg)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    
                }
                
            } else {
                
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,msg)
                
            }
        }
        
        
    }
    
    // MARK: --- Slot Detail  ---
    class func apiForGetSlotDetail(date : String,theripistDetailsId:String,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "therapist_detail_id" : theripistDetailsId as AnyObject,
            "date" : date as AnyObject,
            ]

        
        ServiceHelper.request(userDict, method: .get, apiName: kTimeSlot, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        completion(true,results)
                    }
                    else {
//
//
//                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,results)
                    }
                    
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    
                }
                
            } else {
                
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
                
            }
        }
    }

    // MARK: --- Payment Api
    class func apiForPayment(cardNumber : String,cvc:String,expiryDate:String,amount:String,completion: @escaping ((Dictionary<String,AnyObject>,String) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "type" : "sale" as AnyObject,
            "ccnumber" : cardNumber as AnyObject,
            "ccexp" : expiryDate as AnyObject,
            "cvv" : cvc as AnyObject,
            "username" : "MassageAdvantage" as AnyObject,
            "password" : "MARocks2019?!" as AnyObject,
            "amount" : amount  as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .get, apiName: kPayment, hudType: .default) { (result, error, responseCode) in
            
            
            completion(result as! Dictionary<String, AnyObject>,"\(responseCode)")
        }
    }
    
    // MARK: --- Confirm Booking API ---
    class func apiForConfirmBooking(dict:Dictionary<String,AnyObject>,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {

        ServiceHelper.request(dict, method: .post, apiName: kConfirmBooking, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results : Dictionary<String, AnyObject> = result as! Dictionary<String, AnyObject>

                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {

                        completion(true,results)
                    }
                    else {
                        completion(false,results)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    
                }
                
            } else {
                
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
                
            }
        }
    }

    // MARK: ---  Booking Confirm Appointment Handler  ---
    class func apiForBookingConfirmAppointment(bookingID : String,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        
        ServiceHelper.request(userDict, method: .get, apiName: kConfirmBookingAppointment+"/" + bookingID, hudType: .default) { (result, error, responseCode) in
            var data : Dictionary = Dictionary<String,AnyObject>()
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        completion(true,data)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,data)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,data)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,data)
            }
        }
    }
    
    // MARK: ---  Booking Checkout Handler  ---
    class func apiForBookingCheckout(price : String,coupon : String,couponPrice : String,therapistId : String,serviceId : String,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "price" : price as AnyObject,
            "coupon" : coupon as AnyObject,
            "coupon_price" : couponPrice as AnyObject,
            "therapist_id" : therapistId as AnyObject,
            "service_id" : serviceId as AnyObject,
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kbookingCheckOut, hudType: .default) { (result, error, responseCode) in
            var data : Dictionary = Dictionary<String,AnyObject>()
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        data = results.validatedValue("checkout", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        completion(true,data)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,data)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,data)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,data)
            }
        }
    }
    
    // MARK: ---   cancel Booking Api Handler  ---
    class func apiForCancelBooking(bookingId : String , completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "booking_id" : bookingId as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kcancelBooking, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        completion(true,results)
                        
                    }
                    else {
                        completion(false,results)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,results)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
            }
        }
    }
    
    // MARK: --- Reschedule API  ---
    class func apiForRescheduleBooking(bookingId : String,selectDate : String,selectTime : String , txnId : String,completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        
        let slotCut = selectTime.components(separatedBy: " -")
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "booking_id" : bookingId as AnyObject,
            "select_date" : selectDate as AnyObject,
            "select_time" : slotCut[0] as AnyObject,
            "txn_id" : txnId as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kresheduledBooking, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {

                        completion(true,results)
                    }
                    else {
                        completion(false,results)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,results)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
            }
        }
    }

    // MARK: --- Request API  ---
    class func apiForRequestUrl(slug : String, completion: @escaping ((Bool) -> Void)) {

        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "slug" : slug as AnyObject,

            ]

        ServiceHelper.request(userDict, method: .get, apiName: kRequestURL, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        completion(true)
                    }
                    else {
                        completion(false)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }
    
    
    
   // MARK: *********************************************** THERAPIST SIDE ***************************************************************/
    
    // MARK: ---    Therapist Booking Api Handler  ---
 
    class func apiForTherapistBookingList(selectedDate : String,completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "date" : selectedDate as AnyObject,

        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: ktherapistBooking, hudType: .default) { (result, error, responseCode) in
            var TherapistBookingArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        TherapistBookingArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        
                        completion(true,TherapistBookingArr)
                    }
                    else {
                        completion(false,TherapistBookingArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,TherapistBookingArr)
                }
            }
                
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,TherapistBookingArr)
            }
        }
    }
    
    // MARK: ---   MARK Complete Api Handler  ---
    class func apiForMarkComplete(bookingID : String, payCode : String, completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "booking_id" : bookingID as AnyObject,
            "pay_code" : payCode as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kMarkComplete, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        completion(true,results)
                        
                    }
                    else {
                        completion(false,results)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,results)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
            }
        }
    }
    
    // MARK: ---   Rejected Booking Therapist Api Handler  ---
    class func apiForRejectedBookingTherapist(bookingId : String , completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "booking_id" : bookingId as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kRejectedBookingTherapist, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        completion(true,results)
                        
                    }
                    else {
                        completion(false,results)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,results)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
            }
        }
    }
    
    
    // MARK: ---  Therapist Profile Api Handler   ---
    class func apiForTherapistProfile(completion: @escaping ((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            
            ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kTherapistProfile, hudType: .default) { (result, error, responseCode) in
            var data : Dictionary = Dictionary<String,AnyObject>()
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        completion(true,data)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,data)
                    }
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,data)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,data)
            }
        }
    }

    // MARK: ---   Logout Api Handler  ---
    class func apiForLogout(completion: @escaping ((Bool) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
        "device_id" : UIDevice.current.identifierForVendor?.uuidString as AnyObject,
        "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kLogOut, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {                        
                        completion(true)
                    }
                    else {
                        completion(false)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }
    
    // MARK: ---   Feedback Api Handler  ---
    class func apiForGiveFeedback(therapistId:String,bookingId:String,comment:String,rating:String,completion: @escaping ((Bool) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [

            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "therapist_detail_id" : therapistId as AnyObject,
            "payment_id" : bookingId as AnyObject,
            "comment" : comment as AnyObject,
            "rating" : rating as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kGiveFeedBack, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        completion(true)
                    }
                    else {
                        completion(false)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }

    // MARK: ---   Calander Event Api Handler  ---
    class func apiForGetCalanderEvent(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        var datesArray : Array = Array<AnyObject>()
        ServiceHelper.request(userDict, method: .post, apiName: kCalanderEvent, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                

                
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        datesArray = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>

                        
                        completion(true,datesArray)
                    }
                    else {
                        completion(false,datesArray)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,datesArray)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,datesArray)
            }
        }
    }
    

    // MARK: ---  Gift A Massage Api Handler  ---
    class func apiForGiftMassage(name : String, email :String,txnId:String,completion:@escaping((Bool,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
            "user_name" : name as AnyObject,
            "user_email" : email as AnyObject,
            "txn_id" : txnId as AnyObject,
            
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kGiftAMassage, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        completion(true,results)
                    }
                    else {
                        AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,results)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,results)
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,[:])
            }
        }
    }
    
    // MARK: ---    Therapist Payment summary Api Handler  ---
    class func apiForGetTherapistPaymentSummaryList(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        
        ServiceHelper.request(userDict, method: .post, apiName: kTherapistPaymentSummary, hudType: .default) { (result, error, responseCode) in
            var TherapistpaymentSummArr : Array = Array<AnyObject>()
            var data : Dictionary = Dictionary<String,AnyObject>()

            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        TherapistpaymentSummArr = data.validatedValue("bookings", expected: [] as AnyObject) as! Array<AnyObject>

                        completion(true,TherapistpaymentSummArr)
                    }
                    else {
                        completion(false,TherapistpaymentSummArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,TherapistpaymentSummArr)
                }
            }
                
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,TherapistpaymentSummArr)
            }
        }
    }

    // MARK: ---    Api For Update User Status  ---
    class func apiForGetUpdateUserStatus(completion: @escaping ((Bool) -> Void)) {

        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]

        ServiceHelper.request(userDict, method: .post, apiName: kBackground, hudType: .smoothProgress) { (result, error, responseCode) in

            var data : Dictionary = Dictionary<String,AnyObject>()

            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        data = results.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                        let isSubsCription : String = data.validatedValue("is_subscription", expected: "" as AnyObject) as! String
                        let addMember : String = data.validatedValue("add_member", expected: "" as AnyObject) as! String
                        NSUSERDEFAULT.set(isSubsCription, forKey: kIsSubs)
                        NSUSERDEFAULT.set(addMember, forKey: kAddmember)

                        completion(true)
                    }
                    else {
                    }
                } else {
                }
            }
                
            else {
                completion(false)
            }
        }
    }

    // MARK: ---  RefferalTransaction Api Handler  ---
    class func apiForRefferalTransaction(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary<String, AnyObject> = [
            "user_id" : NSUSERDEFAULT.value(forKey: kUserId) as AnyObject,
        ]
        ServiceHelper.request(userDict, method: .post, apiName: kRefferalTransaction, hudType: .default) { (result, error, responseCode) in
            var RefferalTransactionArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        
                        RefferalTransactionArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        completion(true,RefferalTransactionArr)
                    }
                    else {
                        
                        completion(false,RefferalTransactionArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,RefferalTransactionArr)
                }
            }
                
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,RefferalTransactionArr)
            }
        }
    }
    
    
    // MARK: ---    RetailPlan Api Handler  ---
    
    class func apiForRetailPlan(completion: @escaping ((Bool,Array<AnyObject>) -> Void)) {
        
        let userDict = Dictionary<String, AnyObject>()
        
        ServiceHelper.request(userDict, method: .get, apiName: kRetailPlan, hudType: .default) { (result, error, responseCode) in
            var retailPackArr : Array = Array<AnyObject>()
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        retailPackArr = results.validatedValue("data", expected: [] as AnyObject) as! Array<AnyObject>
                        
                        
                        completion(true,retailPackArr)
                    }
                    else {
                        completion(false,retailPackArr)
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,retailPackArr)
                }
            }
                
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,retailPackArr)
            }
        }
    }
    
    
    
    // MARK: ---    RetailPlan Api Handler  ---
    
    class func apiForSuccessPaymentRetailPlan(dict : Dictionary<String,AnyObject>,completion: @escaping ((Bool,String) -> Void)) {
      
//        let userDict : Dictionary<String, AnyObject>()
        
        
        ServiceHelper.request(dict, method: .post, apiName: kPurchaseRetailPack, hudType: .default) { (result, error, responseCode) in
            
            if error == nil {
                let results : Dictionary = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {
                        let msg : String = results.validatedValue("message", expected: "" as AnyObject) as! String

                        
                        completion(true,msg)
                    }
                    else {
                        completion(false,"")
                    }
                } else {
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,"")
                }
            }
                
            else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    
}
