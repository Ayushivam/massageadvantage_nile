
//
//  TherapistCustomerProfileViewController.swift
//  MassageAdvantage
//
//  Created by nile on 08/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistCustomerProfileViewController: UIViewController {
    
    
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    
    @IBOutlet weak var emailLBl: UILabel!
    
    @IBOutlet weak var phoneNumLbl: UILabel!
    
    
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet weak var specialRequirementLbl: UILabel!
    
    var userID = ""
    var soapNotes = ""
    let accountInfoObj = UserInfo()

    override func viewDidLoad() {
        super.viewDidLoad()

        APIManager.apiForGetProfile(userId: userID){ (status,dict) in
            if status {
                self.accountInfoObj.firstName = dict["first_name"] as! String
                self.accountInfoObj.lastName = dict["last_name"] as! String
                self.accountInfoObj.email = dict["email"] as! String
                self.accountInfoObj.phoneNumber = dict["phone"] as! String
                self.accountInfoObj.address = dict["address"] as! String
                self.accountInfoObj.desc = dict["description"] as! String
                
                self.firstName.text = self.accountInfoObj.firstName
                self.lastName.text = self.accountInfoObj.lastName
                self.emailLBl.text = self.accountInfoObj.email
                self.phoneNumLbl.text = self.accountInfoObj.phoneNumber
                self.locationLbl.text = self.accountInfoObj.address
                self.specialRequirementLbl.text = self.accountInfoObj.desc

            }
        }

        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- UIButton Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func viewSoapNotesBtnActn(_ sender: UIButton) {
        
        if let url = URL(string: "https://www.massageadvantage.com/user/therapist_mobile_profile" + "/" + soapNotes) {
            UIApplication.shared.open(url)
        }
        
    }
}

