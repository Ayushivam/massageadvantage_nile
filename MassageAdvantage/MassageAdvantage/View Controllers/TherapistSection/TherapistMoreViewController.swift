//
//  TherapistMoreViewController.swift
//  MassageAdvantage
//
//  Created by nile on 21/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistMoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
   
    var menuListArray = ["Payment Summary","Refer a Customer"," Referral Transactions","Change Password","Terms & Conditions","Privacy Policy","Contact US","Refund Policy","Cancellation Policy","Log Out"]

    @IBOutlet weak var TherapistMoreTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TherapistMoreTableViewCell" , for: indexPath) as! TherapistMoreTableViewCell
        cell.titleLbl.text = menuListArray[indexPath.row]
        cell.clickButtonAction.tag = indexPath.row+1100
        cell.clickButtonAction.addTarget(self, action: #selector(commonBtnAction), for: .touchUpInside)
        return cell
    }
   
    @objc func commonBtnAction(sender : UIButton) {
        
          let index = sender.tag-1100
        
        
        if index == 0 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Therapist", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "TherapistPaymentSummaryViewController") as! TherapistPaymentSummaryViewController
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
            
        }
            
        else if index == 1 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "MARefferalProgramViewController") as! MARefferalProgramViewController
            staticVC.refferalORGiftType = refferalORGiftEnumType.referAcustomer
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
        
        else if index == 2 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Therapist", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "TherapistRefferalTransactionViewController") as! TherapistRefferalTransactionViewController
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
            
       else if index == 3 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "MAChangePasswordViewController") as! MAChangePasswordViewController
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
            
        else if index == 4 {
            UIApplication.shared.openURL(NSURL(string: "https://www.massageadvantage.com/terms-and-service")! as URL)
        }
            
        else if index == 5 {
            UIApplication.shared.openURL(NSURL(string: "https://www.massageadvantage.com/privacy-policy")! as URL)
        }
            
        
        else  if index == 6 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "MAStaticViewController") as! MAStaticViewController
            staticVC.contentType = methodType.contactUs
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
            
        }
        
        else  if index == 7 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "MAStaticViewController") as! MAStaticViewController
            staticVC.contentType = methodType.refundPolicy
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
            
        }
        else  if index == 8 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "MAStaticViewController") as! MAStaticViewController
            staticVC.contentType = methodType.cancellationPolicy
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
        
        else if index == 9 {
            
            if let _ = NSUSERDEFAULT.value(forKey: kUserId) {
                
                
                AlertController.alert(title: "Logout", message: "Are you sure, you want to logout?", buttons: ["Cancel","Confirm"]) { (alert, index) in
                    
                    if index == 1{
                        APIManager.apiForLogout(completion: { (status) in
                            if status{
                                let viewControllers = APPDELEGATE.navController!.viewControllers
                                
                                for i in 0..<viewControllers.count {
                                    let obj = viewControllers[i]
                                    if obj.isKind(of: MALoginViewController.self){
                                        APPDELEGATE.resetDefaults()
                                        APPDELEGATE.navController!.popToViewController(obj, animated: true)
                                    }
                                }
                                
                            }
                        })
                        
                    }
                }
            }
        }
    }
}


