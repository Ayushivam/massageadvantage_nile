//
//  TherapistManageCalendarViewController.swift
//  MassageAdvantage
//
//  Created by nile on 21/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
//import VACalendar

class TherapistManageCalendarViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    
    var calanderDatesArray = [UserInfo]()

    
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .short, calendar: Calendar.current)
            weekDaysView.appearance = appereance
        }
    }
    
    var calendarView: VACalendarView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let startDate = Calendar.current.date(byAdding: .month, value: -6, to: Date())
        let endDate = Calendar.current.date(byAdding: .month, value: 6, to: Date())
        
        let calendar = VACalendar(
            startDate: startDate,
            endDate: endDate,
            calendar: Calendar.current
        )
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
        resetCalendar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshCalander(notification :)), name: NSNotification.Name("reloadCalanderDates"), object: nil)
        apiForRefreshCalander()
        self.backgroundView.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
    }
   
    func apiForRefreshCalander()
    {
        APIManager.apiForGetCalanderEvent { (status,arr) in
            
            if status{
                
                self.calanderDatesArray.removeAll()
                self.calanderDatesArray += UserInfo.therapistBookingArray(responseArray: arr as! Array<Dictionary<String, Any>>)
                var arr = [(Date, [VADaySupplementary])]()
                for i in self.calanderDatesArray{
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let date = dateFormatter.date(from: i.bookingOn)
                    arr.append((Calendar.current.date(byAdding: .day, value: 0, to: date!)!, [VADaySupplementary.bottomDots([.red])]))
                }
                self.calendarView.setSupplementaries(arr)
                self.calendarView.reloadInputViews()
            }
        }
    }
    
    func resetCalendar()
    {
        calendarView.showDaysOut = false
        calendarView.selectionStyle = .single
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .vertical
        backgroundView.addSubview(calendarView)
        self.calendarView.reloadInputViews()
    }

    @objc func refreshCalander(notification: Notification) {
        self.calendarView.removeFromSuperview()
        let startDate = Calendar.current.date(byAdding: .month, value: -6, to: Date())
        let endDate = Calendar.current.date(byAdding: .month, value: 6, to: Date())
        let calendar = VACalendar(
            startDate: startDate,
            endDate: endDate,
            calendar: Calendar.current
        )
        
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
          resetCalendar()
        if calendarView.frame == .zero {
            calendarView.frame = CGRect(
                x: 0,
                y: weekDaysView.frame.maxY,
                width: backgroundView.frame.width,
                height: backgroundView.frame.height - weekDaysView.frame.maxY
            )
            calendarView.setup()
        }
        apiForRefreshCalander()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if calendarView.frame == .zero {
            calendarView.frame = CGRect(
                x: 0,
                y: weekDaysView.frame.maxY,
                width: backgroundView.frame.width,
                height: backgroundView.frame.height - weekDaysView.frame.maxY
            )
            calendarView.setup()
        }
    }
}

extension TherapistManageCalendarViewController: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 5.0
    }
    
    func rightInset() -> CGFloat {
        return 5.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .black
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return .red
    }
    
    func verticalMonthDateFormater() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter
    }
    
}

extension TherapistManageCalendarViewController: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
        default:
            return .black
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return .red
        default:
            return .clear
        }
    }

    func shape() -> VADayShape {
        
        return .circle
    }

    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
}

extension TherapistManageCalendarViewController: VACalendarViewDelegate {
    
    func selectedDate(_ date: Date) {
        let formatted = APPDELEGATE.formattedDateFromString(dateString: "\(date)", withFormat: "yyyy-MM-dd", inputFormat: "yyyy-MM-dd HH:mm:ss z")

        let storyboard : UIStoryboard = UIStoryboard.init(name: "Therapist", bundle: nil)
                let newViewController = storyboard.instantiateViewController(withIdentifier: "TherapistBookingsViewController")as! TherapistBookingsViewController
        newViewController.selectedDate = formatted!
                APPDELEGATE.navController?.pushViewController(newViewController, animated: true)
    
       
    }
    
}
