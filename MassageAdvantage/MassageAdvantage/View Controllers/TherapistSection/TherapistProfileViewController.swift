//
//  TherapistProfileViewController.swift
//  MassageAdvantage
//
//  Created by nile on 21/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistProfileViewController: UIViewController {
    
    @IBOutlet weak var textImageView: UIImageView!
    @IBOutlet weak var whtsAppImageView: UIImageView!
    @IBOutlet weak var firstNameLbl: UILabel!
    @IBOutlet weak var emailAddressLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var officeBookingURLLbl: UILabel!
    @IBOutlet weak var bioServiceLbl: UILabel!
     @IBOutlet weak var acceptLbl: UILabel!
    
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessLocated: UILabel!
    @IBOutlet weak var businessLocationLbl: UILabel!
    @IBOutlet weak var busniessAddressLbl: UILabel!
    
    @IBOutlet weak var bankNAmeLbl: UILabel!
    
    @IBOutlet weak var accHolderNameLbl: UILabel!
    @IBOutlet weak var accNumberLbl: UILabel!
    @IBOutlet weak var routingNumberLbl: UILabel!
    
    
    @IBOutlet weak var zipCodeLbl: UILabel!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
          apiForTherapistProfile()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTherapistProfileApi(notification :)), name: NSNotification.Name("reloadTherapistProfileApi"), object: nil)


        // Do any additional setup after loading the view.
    }

    @objc func reloadTherapistProfileApi(notification: Notification) {
        apiForTherapistProfile()
    }

    func apiForTherapistProfile() {
    
    APIManager.apiForTherapistProfile { (status,data) in
      
        self.firstNameLbl.text =  (data.validatedValue("therapist_name", expected: "" as AnyObject) as! String)
        self.emailAddressLbl.text = (data.validatedValue("therapist_email", expected: "" as AnyObject) as! String)
        self.phoneLbl.text =  (data.validatedValue("therapist_phone", expected: "" as AnyObject) as! String)
        self.officeBookingURLLbl.text =  (data.validatedValue("offline_booking_url", expected: "" as AnyObject) as! String)
        self.bioServiceLbl.text =  (data.validatedValue("business_description", expected: "" as AnyObject) as! String)
        
        let accept = (data.validatedValue("accept_message", expected: "" as AnyObject) as! String)
        
        if accept == ""   {
            self.acceptLbl.isHidden = true
            self.textImageView.isHidden = true
             self.whtsAppImageView.isHidden = true
        }
        
        
      else  if accept == "2"  {
            self.acceptLbl.isHidden = true
            self.textImageView.isHidden = true
            self.whtsAppImageView.isHidden = false

        }
        else  if accept == "1" {
            self.acceptLbl.isHidden = true
            self.whtsAppImageView.isHidden = true
            self.textImageView.isHidden = false


        }
        else  if accept == "3" {
            self.acceptLbl.isHidden = true
            self.whtsAppImageView.isHidden = false
            self.textImageView.isHidden = false

        }
         else  if accept == "4" {
            self.acceptLbl.isHidden = false
            self.textImageView.isHidden = true
            self.whtsAppImageView.isHidden = true
            self.acceptLbl.text = "None"
        }
        
        
        self.businessName.text =  (data.validatedValue("business_name", expected: "" as AnyObject) as! String)
        self.businessLocated.text = (data.validatedValue("business_located", expected: "" as AnyObject) as! String)
        self.busniessAddressLbl.text =  (data.validatedValue("address_one", expected: "" as AnyObject) as! String) + "," + (data.validatedValue("address_two", expected: "" as AnyObject) as! String) + "," + (data.validatedValue("city", expected: "" as AnyObject) as! String) + "," + (data.validatedValue("state", expected: "" as AnyObject) as! String) + "," + (data.validatedValue("therapist_zip_code", expected: "" as AnyObject) as! String)
        
        self.businessLocationLbl.text =  (data.validatedValue("business_location", expected: "" as AnyObject) as! String)
        
        
        self.bankNAmeLbl.text =  (data.validatedValue("bank_name", expected: "" as AnyObject) as! String)
        
        self.accHolderNameLbl.text =  (data.validatedValue("account_holder_name", expected: "" as AnyObject) as! String)
        
        self.accNumberLbl.text =  (data.validatedValue("account_number", expected: "" as AnyObject) as! String)
        self.routingNumberLbl.text =  (data.validatedValue("swift_code", expected: "" as AnyObject) as! String)
        
        self.zipCodeLbl.text =  (data.validatedValue("zip_code", expected: "" as AnyObject) as! String)
}
}
    
    //MARK:- UiButton Action
    @IBAction func copyBtnAction(_ sender: UIButton) {
        MessageView.showMessage(message: "Text Copied", time: 3.0, verticalAlignment: .bottom)
        UIPasteboard.general.string = officeBookingURLLbl.text
        
    }
    
}
