//
//  BookingDetailsPopUpViewController.swift
//  MassageAdvantage
//
//  Created by nile on 25/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class BookingDetailsPopUpViewController: UIViewController {

    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneNumLbl: UILabel!
    @IBOutlet weak var noteTextview: UITextView!
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var appointmentDateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var masssageTypeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var bookTypeLbl: UILabel!
    var objDetails = UserInfo()
    var bookingType = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        customerNameLbl.text = objDetails.customerName
        emailLbl.text = objDetails.email
        phoneNumLbl.text = objDetails.phoneNumber
        
        noteTextview.text = objDetails.remark
        
         appointmentDateLbl.text  = APPDELEGATE.formattedDateFromString(dateString: objDetails.bookingOn, withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd")!
        
    
        
        
        priceLbl.text = objDetails.totalAmount
        timeLbl.text = objDetails.bookingFrom + " - " + objDetails.bookingTo
        masssageTypeLbl.text = objDetails.serviceName
        
        
        let isPaid = objDetails.isPaid
            
        if isPaid == "1" {
            statusLbl.text = "Completed"
            
        }
        else {
             statusLbl.text = objDetails.bookingStatus
        }
        
        self.bookingType = objDetails.bookingType
        
        if bookingType == "subscriber" {
            
            bookTypeLbl.text = "MA Member"
        }
        else if bookingType == "retail" {
            
        bookTypeLbl.text = "Non MA Member"
        }
        else if bookingType == "coupon" {
            
            bookTypeLbl.text = "Daily Deal Customer"
        }
        else if bookingType == "offline" {
            
            bookTypeLbl.text = "Offline"
        }
        // Do any additional setup after loading the view.
    }

    //MARK :- Button Action
    @IBAction func crossBtnAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
