//
//  TherapistTabViewController.swift
//  MassageAdvantage
//
//  Created by nile on 21/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistTabViewController: UIViewController {

    @IBOutlet weak var heightConstarints: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var calendarButton: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var topHeadingLbl: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var calendarView: UIImageView!
    @IBOutlet weak var moreImageView: UIImageView!
        var currentVC: UIViewController?

  
    
    var profileVC : TherapistProfileViewController?
    var manageCalendarVC : TherapistManageCalendarViewController?
    var moreVC : TherapistMoreViewController?
    var  baseNavController: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        self.topHeadingLbl.text = "Manage Calendar"
        self.calendarView.image = #imageLiteral(resourceName: "THCalendarB")
        self.calendarButton.setTitleColor(#colorLiteral(red: 0.3009740114, green: 0.758854866, blue: 0.7705888152, alpha: 1), for: .normal)
        display(manageCalendarVC!)
        // Do any additional setup after loading the view.
    }
    func initialSetUp() {
        self.navigationController?.navigationBar.isHidden = true
        topView.center = self.view.center
        topView.backgroundColor = UIColor.white
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 8.0
        topView.layer.shadowOffset = CGSize.zero
        topView.layer.shadowRadius = 8.0
        baseNavController = UINavigationController()
        baseNavController?.isNavigationBarHidden = true
        let homeStoryboard = UIStoryboard.init(name: "Therapist", bundle: nil)
        profileVC = homeStoryboard.instantiateViewController(withIdentifier: "TherapistProfileViewController") as? TherapistProfileViewController
        manageCalendarVC = homeStoryboard.instantiateViewController(withIdentifier: "TherapistManageCalendarViewController") as? TherapistManageCalendarViewController
        moreVC = homeStoryboard.instantiateViewController(withIdentifier: "TherapistMoreViewController") as? TherapistMoreViewController
    
    }
    
    func replaceCurrentVC(withVC newVC: UIViewController) {
        currentVC?.willMove(toParent: nil)
        currentVC?.view.removeFromSuperview()
        currentVC?.removeFromParent()
        currentVC = newVC
        addChild(currentVC!)
        containerView?.addSubview((currentVC?.view)!)
        currentVC?.view.frame = (containerView?.bounds)!
        currentVC?.didMove(toParent: self)
    }
    
    func display(_ viewController: UIViewController) {
        baseNavController?.viewControllers = [viewController]
        baseNavController?.isNavigationBarHidden = true
        replaceCurrentVC(withVC: baseNavController!)
    }
    
    
    
   
    @IBAction func profileBtnAction(_ sender: UIButton) {
        initialImage()
        self.topHeadingLbl.text = "Profile"
        self.profileImageView.image = #imageLiteral(resourceName: "THProfileB")
        self.profileBtn.setTitleColor(#colorLiteral(red: 0.3009740114, green: 0.758854866, blue: 0.7705888152, alpha: 1), for: .normal)
        NotificationCenter.default.post(name: Notification.Name("reloadTherapistProfileApi"), object: nil)
        display(profileVC!)
        
    }
    
    @IBAction func calendarBtnAction(_ sender: UIButton) {
        initialImage()
        self.topHeadingLbl.text = "Manage Calendar"
        self.calendarView.image = #imageLiteral(resourceName: "THCalendarB")
        self.calendarButton.setTitleColor(#colorLiteral(red: 0.3009740114, green: 0.758854866, blue: 0.7705888152, alpha: 1), for: .normal)
        display(manageCalendarVC!)
        NotificationCenter.default.post(name: Notification.Name("reloadCalanderDates"), object: nil)

    }
    
    @IBAction func moreBtnAction(_ sender: UIButton) {
        initialImage()
        self.topHeadingLbl.text = "More"
        self.moreImageView.image = #imageLiteral(resourceName: "THMoreB")
        self.moreBtn.setTitleColor(#colorLiteral(red: 0.3009740114, green: 0.758854866, blue: 0.7705888152, alpha: 1), for: .normal)
        display(moreVC!)
    }
    
    @IBAction func logOutBtnAction(_ sender: UIButton) {
        
        AlertController.alert(title: "Logout", message: "Are you sure, you want to logout?", buttons: ["Cancel","Confirm"]) { (alert, index) in
            if index == 1{
                APIManager.apiForLogout(completion: { (status) in
                    if status{
                        let viewControllers = APPDELEGATE.navController!.viewControllers
                        for i in 0..<viewControllers.count {
                            let obj = viewControllers[i]
                            if obj.isKind(of: MALoginViewController.self){
                                APPDELEGATE.resetDefaults()
                                APPDELEGATE.navController!.popToViewController(obj, animated: true)
                            }
                        }
                    }
                })
            }
        }
    }
    
    func initialImage() {
        self.profileImageView.image = #imageLiteral(resourceName: "THProfile")
        self.calendarView.image = #imageLiteral(resourceName: "THCalendar")
        self.moreImageView.image = #imageLiteral(resourceName: "THMore")
        self.profileBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        self.calendarButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        self.moreBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }
}
