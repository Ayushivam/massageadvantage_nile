//
//  TherapistBookingsViewController.swift
//  MassageAdvantage
//
//  Created by nile on 21/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistBookingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var TherapistBookingTableView: UITableView!
    
    var therapistBookingArray = [UserInfo]()
    var selectedDate = ""
    var bookingID = ""
    @IBOutlet weak var noDataFoundLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookingApi()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Booking API
    func bookingApi()
    {
        APIManager.apiForTherapistBookingList(selectedDate: selectedDate) { (status, TherapistBookingArr) in
            if status{
                self.therapistBookingArray.removeAll()
                self.therapistBookingArray += UserInfo.therapistBookingArray(responseArray: TherapistBookingArr as! Array<Dictionary<String, Any>>)
                
                self.TherapistBookingTableView.reloadData()
            }
            else {
                let _ = (self.therapistBookingArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
            }
        }

    }
   
    //MARK :- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK :- TableView Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return therapistBookingArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TherapistBookingTableViewCell", for: indexPath)as! TherapistBookingTableViewCell
        cell.post = therapistBookingArray[indexPath.row]
        cell.viewBtn.tag = indexPath.row+100
        cell.cancelBtn.tag = indexPath.row+1100
        cell.viewBtn.addTarget(self, action: #selector(viewBtnAction), for: .touchUpInside)
        cell.cancelBtn.addTarget(self, action: #selector(cancelBtnAction), for: .touchUpInside)
        cell.markCompleteBtn.tag = indexPath.row+2100
        cell.markCompleteBtn.addTarget(self, action: #selector(markCompleteBtnAction), for: .touchUpInside)
        cell.profileBtn.tag = indexPath.row+3100
        cell.profileBtn.addTarget(self, action: #selector(profileBtnAction), for: .touchUpInside)

        cell.selectionStyle = .none

        return cell
    }
    
   //MARK :- Table View CellButton Action
    @objc func viewBtnAction(sender : UIButton) {
        let obj = therapistBookingArray[sender.tag-100]
        
        let popOverVC = UIStoryboard(name: "Therapist", bundle: nil).instantiateViewController(withIdentifier: "BookingDetailsPopUpViewController") as! BookingDetailsPopUpViewController
        popOverVC.objDetails = obj
        popOverVC.providesPresentationContextTransitionStyle = true
        popOverVC.definesPresentationContext = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        popOverVC.view.backgroundColor = UIColor.init(white : 0.50, alpha: 0.75)
        self.present(popOverVC, animated: true, completion: nil)
    }
    
    @objc func cancelBtnAction(sender : UIButton) {
         let obj = therapistBookingArray[sender.tag-1100]
        
         self.bookingID = obj.id
        
        
        AlertController.alert(title: "", message: "Are you sure, you want to cancel this appointment?", buttons: ["No","Yes"]) { (alert, index) in
            
            if index == 1{
                APIManager.apiForRejectedBookingTherapist(bookingId: self.bookingID) { (status,dict) in
                    if status{
                        
                        let msg : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                        AlertController.alert(title: msg, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                            self.bookingApi()
                        })
                    }
                }
            }
        }
        
    }
    
    @objc func markCompleteBtnAction (sender : UIButton) {
        let obj = therapistBookingArray[sender.tag-2100]
        self.bookingID = obj.id
        presentAlert()
    }
    
    
    @objc func profileBtnAction(sender : UIButton) {
          let obj = therapistBookingArray[sender.tag-3100]
        let storyBoard : UIStoryboard = UIStoryboard.init(name: "Therapist", bundle: nil)
        let newVc = storyBoard.instantiateViewController(withIdentifier: "TherapistCustomerProfileViewController")as! TherapistCustomerProfileViewController
        newVc.userID = obj.userId
        newVc.soapNotes = obj.soapNoteId
        navigationController?.pushViewController(newVc, animated: true)
        
        
    }
    
    
    func presentAlert()
    {
        let alertController = UIAlertController(title: "Mark Complete", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Apply", style: .default, handler: { alert -> Void in
            let codeText = alertController.textFields![0] as UITextField
            if codeText.text! == ""
            {
                MessageView.showMessage(message: "Please enter code.", time: 2.0,verticalAlignment: .center)
                self.presentAlert()
            }else{
                
                APIManager.apiForMarkComplete(bookingID: self.bookingID, payCode: codeText.text!, completion: { (status,dict) in
                      if status {
                        let msg : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                        
                        AlertController.alert(title: msg, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                            self.bookingApi()
                        })
                    }
                      else {
                        let msg : String = dict.validatedValue("message", expected: "" as AnyObject) as! String

                        MessageView.showMessage(message: msg, time: 2.0,verticalAlignment: .center)
                        self.presentAlert()
                    }
                })
            }
        }
        ))
        
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter Code"
        })
        self.present(alertController, animated: true, completion: nil)

    }
    
}
