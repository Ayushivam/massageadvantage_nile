//
//  TherapistRefferalTransactionViewController.swift
//  MassageAdvantage
//
//  Created by nile on 19/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistRefferalTransactionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    @IBOutlet weak var refferalTransactionTableView: UITableView!
    var  refferalTransactionArray = [UserInfo]()
    @IBOutlet weak var noDataFoundlabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIManager.apiForRefferalTransaction { (status, RefferalTransactionArr) in
            if status {
               self.refferalTransactionArray.removeAll()
                self.refferalTransactionArray += UserInfo.refferalTransactionArray(responseArray: RefferalTransactionArr as! Array<Dictionary<String, Any>>)
                self.refferalTransactionTableView.reloadData()
                
            }
            
           
            else {
                let _ = (self.refferalTransactionArray.count == 0) ? (self.noDataFoundlabel.isHidden = false) : (self.noDataFoundlabel.isHidden = true)
                
            }

            
            
            
        }
        
        // Do any additional setup after loading the view.
    }
    

    //MARK:- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return refferalTransactionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TherapistRefferalTransactionTableViewCell", for: indexPath) as! TherapistRefferalTransactionTableViewCell
        
        cell.post = refferalTransactionArray[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
}
