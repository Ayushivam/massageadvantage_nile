//
//  TherapistPaymentSummaryViewController.swift
//  MassageAdvantage
//
//  Created by nile on 09/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistPaymentSummaryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var therapistPaySummTableView: UITableView!
    var therapistpaymentSummaryArray = [UserInfo]()
    var paySummary = UserInfo()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIManager.apiForGetTherapistPaymentSummaryList { (status,TherapistpaymentSummArr) in
            
            if status{
                self.therapistpaymentSummaryArray.removeAll()
                self.therapistpaymentSummaryArray += UserInfo.getTherapistPaymentSummArray(responseArray: TherapistpaymentSummArr as! Array<Dictionary<String, Any>>)
                
                
                
                

                self.therapistPaySummTableView.reloadData()
            }


            else {
                let _ = (self.therapistpaymentSummaryArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)

            }
            
            
        }

        // Do any additional setup after loading the view.
    }
    
    //MARK :- Button Action

    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 195
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return therapistpaymentSummaryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TherapistPaymentSummaryTableViewCell", for: indexPath)as! TherapistPaymentSummaryTableViewCell
        cell.post = therapistpaymentSummaryArray[indexPath.row]
        cell.viewBtn.tag = indexPath.row+1000
        cell.viewBtn.addTarget(self, action: #selector(viewBtnAction), for: .touchUpInside)
        cell.selectionStyle = .none

        return cell
    }
    
    @objc func viewBtnAction(sender : UIButton) {

    let obj = therapistpaymentSummaryArray[sender.tag-1000]
        
        
        let storyBoard : UIStoryboard = UIStoryboard.init(name: "Therapist", bundle: nil)
        let newVc = storyBoard.instantiateViewController(withIdentifier: "TherapistPaymentDetailsViewController")as! TherapistPaymentDetailsViewController
        newVc.objViewDetails = obj
        navigationController?.pushViewController(newVc, animated: true)
        
     
    }

}
