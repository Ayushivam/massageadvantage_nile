//
//  TherapistPaymentDetailsViewController.swift
//  MassageAdvantage
//
//  Created by nile on 10/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class TherapistPaymentDetailsViewController: UIViewController {

    
    @IBOutlet weak var memberShioDiscountConstarints: NSLayoutConstraint!
    @IBOutlet weak var memberShipDiscHeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var couponDiscHeadingConstrints: NSLayoutConstraint!
    
    @IBOutlet weak var couponDiscountConstarints: NSLayoutConstraint!
    @IBOutlet weak var memberShipDiscHeadingLbl: UILabel!
    @IBOutlet weak var couponDisHeadingLBl: UILabel!
    @IBOutlet weak var memberShipDiscountLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var customerTypeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var massageTypeLbl: UILabel!
    @IBOutlet weak var transacIDLbl: UILabel!
    @IBOutlet weak var swedishMassagePriceLbl: UILabel!
    @IBOutlet weak var upgradeMassagePriceLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var payoutSwedishPriceLbl: UILabel!
    @IBOutlet weak var payoutUpgradePriceLbl: UILabel!
    @IBOutlet weak var totalPayoutLbl: UILabel!
    var objViewDetails = UserInfo()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.swedishMassagePriceLbl.text = "$" + (self.objViewDetails.paySummData.validatedValue("swedish_price", expected: "" as AnyObject) as!
            String)
        
        self.upgradeMassagePriceLbl.text = "$" + (self.objViewDetails.paySummData.validatedValue("upgrade_price", expected: "" as AnyObject) as!
            String)
        
        self.subTotalLbl.text = "$" + (self.objViewDetails.paySummData.validatedValue("subtotal", expected: "" as AnyObject) as!
            String)
        
        self.totalAmountLbl.text = "$" + (self.objViewDetails.paySummData.validatedValue("total_amount", expected: "" as AnyObject) as!
            String)
        
        
        self.payoutSwedishPriceLbl.text = "$" + (self.objViewDetails.payOutData.validatedValue("swedish_price", expected: "" as AnyObject) as!
            String)
        
        self.payoutUpgradePriceLbl.text = "$" + (self.objViewDetails.payOutData.validatedValue("upgrade_price", expected: "" as AnyObject) as!
            String)
        self.totalPayoutLbl.text = "$" + (self.objViewDetails.payOutData.validatedValue("total_payout", expected: "" as AnyObject) as!
            String)
        
      
        
    self.nameLbl.text = objViewDetails.customerName
        
        self.emailLbl.text = objViewDetails.email
        self.phoneLbl.text = objViewDetails.phoneNumber
        
        self.timeLbl.text = APPDELEGATE.formattedDateFromString(dateString: self.objViewDetails.bookingFrom, withFormat: "h:mm a", inputFormat: "HH:mm:ss")!
        self.dateLbl.text  = APPDELEGATE.formattedDateFromString(dateString: objViewDetails.bookingOn, withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd")!
        self.massageTypeLbl.text = objViewDetails.serviceName
        if objViewDetails.txnId == "" {
            transacIDLbl.text = "N/A"
        }
        else {
            transacIDLbl.text = objViewDetails.txnId
        }
        
        if objViewDetails.bookingType == "coupon" {
            self.customerTypeLbl.text = "Voucher"

            if objViewDetails.serviceId == "1" {
                
                couponDiscountLbl.isHidden = true
                memberShipDiscountLbl.isHidden = true
                couponDisHeadingLBl.isHidden = true
                memberShipDiscHeadingLbl.isHidden = true
                couponDiscountConstarints.constant = 0
                couponDiscHeadingConstrints.constant = 0
                memberShioDiscountConstarints.constant = 0
                memberShipDiscHeadingConstraint.constant = 0
            }
            else {
                memberShipDiscountLbl.isHidden = true
                memberShipDiscHeadingLbl.isHidden = true
                memberShioDiscountConstarints.constant = 0
                memberShipDiscHeadingConstraint.constant = 0
                couponDiscountLbl.isHidden = false
                couponDisHeadingLBl.isHidden = false
                couponDiscountConstarints.constant = 18
                couponDiscHeadingConstrints.constant = 18
                couponDiscountLbl.text = "$" + (self.objViewDetails.paySummData.validatedValue("coupon_discount", expected: "" as AnyObject) as!
                    String)

            }
        }
        
        else if objViewDetails.bookingType == "subscriber" {
            self.customerTypeLbl.text = "Member"
            
            if objViewDetails.serviceId == "1" {
                couponDiscountLbl.isHidden = true
                memberShipDiscountLbl.isHidden = true
                couponDisHeadingLBl.isHidden = true
                memberShipDiscHeadingLbl.isHidden = true
                couponDiscountConstarints.constant = 0
                couponDiscHeadingConstrints.constant = 0
                memberShioDiscountConstarints.constant = 0
                memberShipDiscHeadingConstraint.constant = 0
                
            }
            else {
                couponDiscountLbl.isHidden = true
                couponDisHeadingLBl.isHidden = true
                memberShipDiscountLbl.isHidden = false
                memberShipDiscHeadingLbl.isHidden = false
                couponDiscountConstarints.constant = 0
                couponDiscHeadingConstrints.constant = 0
                memberShioDiscountConstarints.constant = 18
                memberShipDiscHeadingConstraint.constant = 18
                memberShipDiscountLbl.text = "$" + (self.objViewDetails.paySummData.validatedValue("membership_discount", expected: "" as AnyObject) as!
                    String)

            }
        }
        
        else if objViewDetails.bookingType == "retail" {
            self.customerTypeLbl.text = "Retail"

            if objViewDetails.serviceId == "1" {
                couponDiscountLbl.isHidden = true
                memberShipDiscountLbl.isHidden = true
                couponDisHeadingLBl.isHidden = true
                memberShipDiscHeadingLbl.isHidden = true
                couponDiscountConstarints.constant = 0
                couponDiscHeadingConstrints.constant = 0
                memberShioDiscountConstarints.constant = 0
                memberShipDiscHeadingConstraint.constant = 0
            }
            else {
                couponDiscountLbl.isHidden = true
                memberShipDiscountLbl.isHidden = true
                couponDisHeadingLBl.isHidden = true
                memberShipDiscHeadingLbl.isHidden = true
                couponDiscountConstarints.constant = 0
                couponDiscHeadingConstrints.constant = 0
                memberShioDiscountConstarints.constant = 0
                memberShipDiscHeadingConstraint.constant = 0
               
            }
            
        }
        
        
      
        // Do any additional setup after loading the view.
    }
    
    //MARK :- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

}
