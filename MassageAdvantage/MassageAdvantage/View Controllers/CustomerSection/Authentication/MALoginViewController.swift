//
//  MALoginViewController.swift
//  MassageAdvantage
//
//  Created by nile on 07/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MALoginViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var passwordBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let _ = NSUSERDEFAULT.value(forKey: kUserId)
        {
            let userGroup = NSUSERDEFAULT.value(forKey: kUserGroup)
            if (userGroup as AnyObject) as! String == "2" {
                let  storyboard:UIStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
                let newViewController = storyboard.instantiateViewController(withIdentifier: "MATabViewController") as! MATabViewController
                self.navigationController?.pushViewController(newViewController, animated: false)
            }
            else  if (userGroup as AnyObject) as! String == "3" {
                let  storyboard:UIStoryboard =  UIStoryboard.init(name: "Therapist", bundle: nil)
                let newViewController = storyboard.instantiateViewController(withIdentifier: "TherapistTabViewController") as! TherapistTabViewController
                self.navigationController?.pushViewController(newViewController, animated: false)
            }
        }
        self.hideKeyboardWhenTappedAround()
        emailAddressTextField.setBottomBorder()
        passwordTextField.setBottomBorder()
        loginBtn.setShadow()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.emailAddressTextField.text = ""
            self.passwordTextField.text = ""
    }
    //MARK:- Button Actions
    @IBAction func loginBtnAction(_ sender: UIButton) {
        let validationObj = ValidationClass.verifyFieldForLogin(email: self.emailAddressTextField.text!, password: passwordTextField.text!)
        if validationObj.1{
            APIManager.apiForLogin(email: self.emailAddressTextField.text!, password: passwordTextField.text!) { (status) in
                if status{
                    let userGroup = NSUSERDEFAULT.value(forKey: kUserGroup)
                    if (userGroup as AnyObject) as! String == "2" {
                        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let newViewController = storyboard.instantiateViewController(withIdentifier: "MATabViewController")as! MATabViewController
                        self.navigationController?.pushViewController(newViewController, animated: true)
                    }
                 else if (userGroup as AnyObject) as! String == "3" {
                        
                        let storyboard : UIStoryboard = UIStoryboard.init(name: "Therapist", bundle: nil)
                        let newViewController = storyboard.instantiateViewController(withIdentifier: "TherapistTabViewController")as! TherapistTabViewController
                        self.navigationController?.pushViewController(newViewController, animated: true)
                    }
                }
            }
        }
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }
    
    @IBAction func skipLoginBtnAction(_ sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "MATabViewController")as! MATabViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }

    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            return true
    }
    
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "MAForgotPasswordViewController")as! MAForgotPasswordViewController
        navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @IBAction func facebookBtnAction(_ sender: UIButton) {
       //  facebookLogin()
    }
    
    
    @IBAction func passwrdHideBtnActn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            passwordTextField.isSecureTextEntry = false
            passwordBtn.setImage(UIImage (named: "eye"), for: .normal)
        }
        else {
            passwordTextField.isSecureTextEntry = true
            passwordBtn.setImage(UIImage (named: "hide"), for: .normal)
        }
    }
    
    @IBAction func googleBtnAction(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 60
            {
                return false
            }
            break
        case 101:
            if str.length > 15
            {
                return false
            }
        default:
            break
        }
        
        //MARK:- Does'nt Support Emojies
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
    
}
