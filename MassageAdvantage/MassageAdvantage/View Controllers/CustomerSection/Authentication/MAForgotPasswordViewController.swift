//
//  MAForgotPasswordViewController.swift
//  MassageAdvantage
//
//  Created by nile on 07/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var emailAddressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        emailAddressTextField.setBottomBorder()
        sendBtn.setShadow()
    }
    
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func sendResetLinkAction(_ sender: UIButton) {
        let validationObj = ValidationClass.verifyFieldForForgotPassword(email: emailAddressTextField.text!)
        if validationObj.1{
            APIManager.apiForForgotPassword(email: self.emailAddressTextField.text!) { (status) in
                if status{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
        
    }
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 60
            {
                return false
            }
            break
            
        default:
            break
        }
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
