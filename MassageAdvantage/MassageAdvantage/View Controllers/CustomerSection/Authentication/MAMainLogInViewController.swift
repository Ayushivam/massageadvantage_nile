

import UIKit


class MAMainLogInViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   view.layoutIfNeeded()
        
      
        // Do any additional setup after loading the view.
    }
    
    @IBAction func logInBtnAction(_ sender: UIButton) {
        let storyboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "MALoginViewController") as! MALoginViewController
        navigationController?.pushViewController(newViewController, animated: true)
    }
}
