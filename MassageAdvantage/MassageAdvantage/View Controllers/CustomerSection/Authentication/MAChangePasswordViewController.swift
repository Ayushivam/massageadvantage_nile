//
//  MAChangePasswordViewController.swift
//  MassageAdvantage
//
//  Created by nile on 09/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAChangePasswordViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var confirmPasswordBtn: UIButton!
    @IBOutlet weak var newPasswordBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    var passData = Dictionary<String,AnyObject>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        newPasswordTextField.setBottomBorder()
        confirmPasswordTextField.setBottomBorder()
        submitBtn.setShadow()
        // Do any additional setup after loading the view.
    }

    //MARK:- UIButton action

    @IBAction func bckBTnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        let validationObj = ValidationClass.verifyNewPasswordAndConfirmPassword(newPassword: newPasswordTextField.text!, confirmPassword: confirmPasswordTextField.text!)
        if validationObj.1 {
            APIManager.apiForChangePassword(newPassword: self.newPasswordTextField.text!, confirmNewPassword: confirmPasswordTextField.text!, dict: self.passData) { (status) in
                if status{
                    self.navigationController?.popViewController(animated: true)
                    }
            }
        }
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }
    
    @IBAction func newPasswordBtnActn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            newPasswordTextField.isSecureTextEntry = false
            newPasswordBtn.setImage(UIImage (named: "eye"), for: .normal)
        }
        else {
            newPasswordTextField.isSecureTextEntry = true
            newPasswordBtn.setImage(UIImage (named: "hide"), for: .normal)
        }
    }
    
    @IBAction func confirmPasswordBtnActn(_ sender: UIButton) {

        
    sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            confirmPasswordTextField.isSecureTextEntry = false
            confirmPasswordBtn.setImage(UIImage (named: "eye"), for: .normal)
            
        }
        else {
            confirmPasswordTextField.isSecureTextEntry = true
            confirmPasswordBtn.setImage(UIImage (named: "hide"), for: .normal)
            
        }
    }
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 15
            {
                return false
            }
            break
        case 101:
            if str.length > 15
            {
                return false
            }
        default:
            break
        }
        //MARK:- Does'nt Support Emojies
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
    
}
