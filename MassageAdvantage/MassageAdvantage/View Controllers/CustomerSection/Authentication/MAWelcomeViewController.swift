//
//  MAWelcomeViewController.swift
//  MassageAdvantage
//
//  Created by nile on 19/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
class MAWelcomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    var imgArr = ["loginTutorial","bookingTutorial","paymentTutorial","welcome3"]
    var titleArr = ["Register your account","Book a Massage","Pay via NMI"]

    var subtitleArr = ["To register your account, go to https://www.massageadvantage.com/register, Once registered, you will be able to login.","Choose a massage service along with your location and preferred therapist,enjoy the services and don’t forget to share your experience via reviews & ratings","Book your services and pay via NMI payment gateway."]

    @IBOutlet weak var skipBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func skipAction(_ sender: UIButton) {
        let  storyboard:UIStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "MALoginViewController") as! MALoginViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    // UI Collection View Delegate And Datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MAWelcomeCollectionViewCell", for: indexPath) as! MAWelcomeCollectionViewCell
        cell.pageControl.numberOfPages = 3
        cell.openUrlButton.addTarget(self, action: #selector(openUrlAction), for: .touchUpInside)
        cell.openUrlButton.tag = indexPath.row + 100
        cell.tutorialImg.image = UIImage(named: imgArr[indexPath.row])
        cell.titleLbl.text =  titleArr[indexPath.row]
        cell.descriptionLbl.text =  subtitleArr[indexPath.row]
        cell.pageControl.currentPage = indexPath.row

        let _ = (indexPath.row == 1) ? (cell.backView.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.768627451, blue: 0.7803921569, alpha: 1)) : (indexPath.row == 0) ? (cell.backView.backgroundColor = #colorLiteral(red: 0.4156862745, green: 0.7450980392, blue: 0.3450980392, alpha: 1)) : (cell.backView.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.2862745098, blue: 0.337254902, alpha: 1))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let cellsAcross: CGFloat = 2
//        var widthRemainingForCellContent = collectionView.bounds.width
//        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
//            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
//            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
//        }
//        
//        let cellWidth = widthRemainingForCellContent / cellsAcross
        return CGSize(width: self.homeCollectionView.frame.width, height: self.homeCollectionView.frame.height)
    }

    @objc func openUrlAction(_sender:UIButton)
    {
        let index = _sender.tag - 100
        if index == 0{
            if let url = URL(string: "https://www.massageadvantage.com/register") {
                UIApplication.shared.open(url, options: [:])
            }
        }
    }
}
