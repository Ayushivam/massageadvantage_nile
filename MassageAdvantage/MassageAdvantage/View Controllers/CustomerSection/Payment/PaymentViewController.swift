//
//  PaymentViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 04/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
import Stripe
import CreditCardForm

protocol  SuccessPayment : class{
    
    func notifySuccessPayment(transactionId:String)
    
}

protocol SuccessGiftAMassagePayment : class {
    func notifySuccessGiftAMassagePayment(transactionId:String)
    
}

protocol RescheduleMassagePayment : class {
    func notifySuccessReschedulePayment(transactionId:String)
    
}

protocol RetailPlanPayment : class {
    func notifySuccessRetailPlanPayment(transactionId:String,responseString:String)
}


class PaymentViewController: UIViewController,STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var creditCardView: CreditCardFormView!
    
    var amount = ""
    
    var delegate : SuccessPayment?
    var giftDelegate : SuccessGiftAMassagePayment?
    var rescheduleDelegate : RescheduleMassagePayment?
    var retailPlanDelegate : RetailPlanPayment?
    let paymentTextField = STPPaymentCardTextField()
    var isFromGift = false
    var isFromReschedule = false
    var isFromRetailPlan = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        creditCardView.cardHolderString = ""
        createTextField()
    }
    
    func createTextField() {
        paymentTextField.frame = CGRect(x: 15, y: 199, width: self.view.frame.size.width - 30, height: 44)
        paymentTextField.delegate = self
        paymentTextField.translatesAutoresizingMaskIntoConstraints = false
        paymentTextField.layer.borderWidth = 0
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: paymentTextField.frame.size.height - width, width:  paymentTextField.frame.size.width, height: paymentTextField.frame.size.height)
        border.borderWidth = width
        paymentTextField.layer.addSublayer(border)
        paymentTextField.layer.masksToBounds = true
        
        view.addSubview(paymentTextField)
        
        NSLayoutConstraint.activate([
            paymentTextField.topAnchor.constraint(equalTo: creditCardView.bottomAnchor, constant: 20),
            paymentTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            paymentTextField.widthAnchor.constraint(equalToConstant: self.view.frame.size.width-20),
            paymentTextField.heightAnchor.constraint(equalToConstant: 44)
            ])
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        creditCardView.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: textField.expirationYear, expirationMonth: textField.expirationMonth, cvc: textField.cvc)
    }
    
    func paymentCardTextFieldDidEndEditingExpiration(_ textField: STPPaymentCardTextField) {
        creditCardView.paymentCardTextFieldDidEndEditingExpiration(expirationYear: textField.expirationYear)
    }
    
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardView.paymentCardTextFieldDidBeginEditingCVC()
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardView.paymentCardTextFieldDidEndEditingCVC()
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        if isFromRetailPlan {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
//                let transactionId : String = randomString(length: 10)
//
//            let dummyString : String = "response=1&responsetext=SUCCESS&authcode=123456&transactionid=" + transactionId + "&avsresponse=&cvvresponse=N&orderid=&type=sale&response_code=100"
//
//        if self.isFromGift {
//                    self.giftDelegate?.notifySuccessGiftAMassagePayment(transactionId: transactionId)
//                    self.navigationController?.popViewController(animated: true)
//                }
//                else if self.isFromReschedule{
//                    self.rescheduleDelegate?.notifySuccessReschedulePayment(transactionId: transactionId)
//                    self.navigationController?.popViewController(animated: true)
//                }
//                else if self.isFromRetailPlan{
//                    self.retailPlanDelegate?.notifySuccessRetailPlanPayment(transactionId: transactionId,responseString:dummyString )
//                    self.dismiss(animated: true, completion: nil)
//                }
//                else
//                {
//                    self.delegate?.notifySuccessPayment(transactionId: transactionId)
//                    self.navigationController?.popViewController(animated: true)
//                }
//
        
        if paymentTextField.isValid
        {

            let exp : String = paymentTextField.formattedExpirationMonth! + "\(paymentTextField.expirationYear)"

            APIManager.apiForPayment(cardNumber: paymentTextField.cardNumber!, cvc: paymentTextField.cvc!, expiryDate: exp, amount: self.amount) { (dict,code) in

                if code == "200"{

                    let responseStr : String = dict["ResponseString"] as! String
                    print(responseStr)
                    let resArr = responseStr.components(separatedBy: "&")
                    let resParts = resArr[0].components(separatedBy: "=")
                    if resParts[0] == "response"
                    {
                        if resParts[1] == "1"
                        {
                            for i in resArr{

                                let parts = i.components(separatedBy: "=")
                                if parts[0] == "transactionid"
                                {
                                    if parts[1] != ""
                                    {
                                        if self.isFromGift {
                                            self.giftDelegate?.notifySuccessGiftAMassagePayment(transactionId: parts[1])
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                        else if self.isFromReschedule{
                                            self.rescheduleDelegate?.notifySuccessReschedulePayment(transactionId: parts[1])
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                        else if self.isFromRetailPlan{
                                            self.retailPlanDelegate?.notifySuccessRetailPlanPayment(transactionId: parts[1],responseString: responseStr)
                                            //                                            self.navigationController?.popViewController(animated: true)
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            self.delegate?.notifySuccessPayment(transactionId: parts[1])
                                            self.navigationController?.popViewController(animated: true)
                                        }

                                        break
                                    }else{
                                        AlertController.alert(title: "Something Went Wrong!", message: "Transaction failed, Please try after sometime.", buttons: ["OK"], tapBlock: { (alert, index) in

                                            if self.isFromRetailPlan {
                                                self.dismiss(animated: true, completion: nil)
                                            }
                                            else {
                                                self.navigationController?.popViewController(animated: true)

                                            }
                                        })

                                        break
                                    }

                                }
                            }
                        }
                        else{
                            AlertController.alert(title: "Something Went Wrong!", message: "Transaction failed.", buttons: ["OK"], tapBlock: { (alert, index) in
                                if self.isFromRetailPlan {
                                    self.dismiss(animated: true, completion: nil)
                                }
                                else {
                                    self.navigationController?.popViewController(animated: true)

                                }
                            })
                        }
                    }
                }else{
                    AlertController.alert(title: "Something Went Wrong!", message: "Transaction failed.", buttons: ["OK"], tapBlock: { (alert, index) in
                        if self.isFromRetailPlan {
                            self.dismiss(animated: true, completion: nil)
                        }
                        else {
                            self.navigationController?.popViewController(animated: true)

                        }
                    })
                }

            }
        }else{
            AlertController.alert(title: "Wrong Detail.")
        }
    }
    
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
}
