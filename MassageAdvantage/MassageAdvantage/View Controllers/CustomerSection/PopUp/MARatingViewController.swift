//
//  MARatingViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 28/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MARatingViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    
    var therapistId = ""
    var bookingId = ""
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        commentTextView.textColor = UIColor.lightGray
        commentTextView.text = "Comment...."

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        var ratingComment = ""
        
        if self.commentTextView.text! == "" || self.commentTextView.text! == "Comment...."
        {
            ratingComment = ""
        }else{
        
            ratingComment = self.commentTextView.text!
        
        }
        
        let rating : Int = Int(ratingView.rating)
        
        
        APIManager.apiForGiveFeedback(therapistId:therapistId , bookingId: bookingId, comment: ratingComment, rating: "\(rating)") { (status) in
            if status{
                self.dismiss(animated: false, completion: nil)
                NotificationCenter.default.post(name: Notification.Name("reloadAfterSucessComlpete"), object: nil)

            }
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if commentTextView.text == "Comment...."{
            commentTextView.textColor = UIColor.darkGray
            commentTextView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            commentTextView.textColor = UIColor.lightGray
            commentTextView.text = "Comment...."
        }
    }
    
}
