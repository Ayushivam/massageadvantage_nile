//
//  MACouponViewController.swift
//  MassageAdvantage
//
//  Created by nile on 29/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol BookDelegate : class{
    func showDetailPage(slug:String,dict:Dictionary<String,AnyObject>)
    
    func showTherapistLoginPage()
    func showRequestUrlPage(url:String,slug:String)
    
}

class MACouponViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var noLoginView: UIView!
    
    @IBOutlet weak var noLoginCouponButton: UIButton!
    
    @IBOutlet weak var noLoginMassageButton: UIButton!

    @IBOutlet weak var couponBackView: UIView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var enterCouponTextField: UITextField!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var authLoginButton: UIButton!
    @IBOutlet weak var authRegisterButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginEmailField: UITextField!
    @IBOutlet weak var loginBackView: UIView!
    @IBOutlet weak var couponViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var loginPaswordField: UITextField!
    @IBOutlet weak var authBckView: UIView!
    @IBOutlet weak var registerBackView: UIView!
    @IBOutlet weak var registerEmailField: UITextField!
    @IBOutlet weak var registerNameField: UITextField!
    @IBOutlet weak var registerPhoneField: UITextField!
    @IBOutlet weak var registerPasswordField: UITextField!
    var delegate : BookDelegate?
    var slug = ""
    var isCoupon = ""
    var requestUrl = ""
    
    var couponDict = Dictionary<String,AnyObject>()
    
    
    @IBOutlet weak var lookingForDealBtn: UIButton!
    @IBOutlet weak var onlyLoginBackView: UIView!
    @IBOutlet weak var loginPasswordBtn: UIButton!
    @IBOutlet weak var registerPasswordBtn: UIButton!

    @IBOutlet weak var onlyLoginEmailField: UITextField!
    @IBOutlet weak var onlyLoginPasswordField: UITextField!

    @IBOutlet weak var onlyEyePasswordButton: UIButton!
    
    @IBOutlet weak var onlySubmitButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.hideKeyboardWhenTappedAround()
        enterCouponTextField.setBottomBorder()
        applyBtn.setShadow()
        loginButton.setShadow()
        registerButton.setShadow()
        authLoginButton.setShadow()
        onlySubmitButton.setShadow()
        authRegisterButton.setShadow()
        self.loginEmailField.setBottomBorder()
        self.loginPaswordField.setBottomBorder()
        registerNameField.setBottomBorder()
        registerEmailField.setBottomBorder()
        registerPhoneField.setBottomBorder()
        registerPasswordField.setBottomBorder()
        self.onlyLoginEmailField.setBottomBorder()
        self.onlyLoginPasswordField.setBottomBorder()
     
        if isCoupon != "1"{
            
          //  initialStateForNotLogin()
            couponBackView.isHidden = true
            loginBackView.isHidden = true
            onlyLoginBackView.isHidden = true
            authBckView.isHidden = true
            noLoginView.isHidden = false
            
        }

        // Do any additional setup after loading the view.
    }
    
    func initialStateForNotLogin()
    {
        self.enterCouponTextField.isUserInteractionEnabled = false
        self.applyBtn.isHidden = true
        self.couponViewHeightConstraints.constant = 180
        authBckView.isHidden = false
        loginBackView.isHidden = false
        registerBackView.isHidden = true
        loginEmailField.text = ""
        loginPaswordField.text = ""
        registerNameField.text = ""
        registerEmailField.text = ""
        registerPhoneField.text = ""
        registerPasswordField.text = ""
    }
    
    //MARK:- UI Button Actions
    @IBAction func crossBtnAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
    }

    @IBAction func applyBtnAction(_ sender: UIButton) {
        
         let validationObj = ValidationClass.verifyFieldForCouponCode(couponCode: self.enterCouponTextField.text!)
        
        if validationObj.1{
            APIManager.apiForVerifyCouponCode(couponCode: self.enterCouponTextField.text!) { (status,msg,dict) in
                if status {
                    self.couponDict = dict
                    self.isCoupon = "1"
                    if let _ = NSUSERDEFAULT.value(forKey: kUserId)
                        {
                            NSUSERDEFAULT.set(self.enterCouponTextField.text!, forKey: kCouponCode)
                            if self.requestUrl == ""{
                                self.dismiss(animated: false, completion: {
                                    MessageView.showMessage(message: msg, time: 2.0,verticalAlignment: .bottom)
                                    self.delegate?.showDetailPage(slug: self.slug,dict:self.couponDict)
                                })
                            }
                            else{
                                self.dismiss(animated: false, completion: {
                                    self.delegate?.showRequestUrlPage(url: self.requestUrl,slug: self.slug)
                                })
                            }
                        }
                        else {
                        
                        self.messageLbl.text = msg
                        self.initialStateForNotLogin()
                            UIView.animate(withDuration: 1) {
                        
                            self.scrollView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
                        }
                        }
                }
            }
        }
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }
    
    @IBAction func authLoginAction(_ sender: UIButton) {
      
        initialStateForNotLogin()
        loginBackView.isHidden = false
        
        UIView.animate(withDuration: 1) {
             self.scrollView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
        }
    }
    
    @IBAction func authRegisterAction(_ sender: UIButton) {
    
        initialStateForNotLogin()
        registerBackView.isHidden = false
        UIView.animate(withDuration: 1) {
            
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
            
        }
    }

    @IBAction func loginAction(_ sender: UIButton) {

        let validationObj = ValidationClass.verifyFieldForLogin(email: self.loginEmailField.text!, password: self.loginPaswordField.text!)

        if validationObj.1{

            APIManager.apiForLogin(email: self.loginEmailField.text!, password: loginPaswordField.text!) { (status) in
                if status{

                    let userGroup = NSUSERDEFAULT.value(forKey: kUserGroup)
                    if self.isCoupon == "1"{
                        
                        NSUSERDEFAULT.set(self.enterCouponTextField.text!, forKey: kCouponCode)
                    }
                    if self.requestUrl == ""
                    {
                   
                    if (userGroup as AnyObject) as! String == "2" {
                        
                        if let _ = NSUSERDEFAULT.value(forKey: kIsSubs) {
                            
                            let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
                            print(isSubscriptionValue)
                            
                            if self.isCoupon == "1"{
                                
                                NSUSERDEFAULT.set(self.enterCouponTextField.text!, forKey: kCouponCode)
                            }
                            
                            if isSubscriptionValue == "1" {
                                
                                self.dismiss(animated: false, completion: {
                                    self.delegate?.showDetailPage(slug: self.slug,dict: self.couponDict)
                                })
                            }
                            else if isSubscriptionValue == "0" {
                                self.onlyLoginBackView.isHidden = true
                                
                                if self.isCoupon == "1"{
                                    
                                    NSUSERDEFAULT.set(self.enterCouponTextField.text!, forKey: kCouponCode)
                                    
                                    
                                    self.dismiss(animated: false, completion: {
                                        self.delegate?.showDetailPage(slug: self.slug,dict: self.couponDict)
                                    })
                                }
                                else{
                                    AlertController.alert(title: "It seems, you are not a Massage Advantage Member click the below link to buy membership Plan", message: "", buttons: ["Cancel","Click Here"]) { (alert, index) in
                                        
                                        if index == 1{
                            UIApplication.shared.openURL(NSURL(string: kSubscriptionPlanUrl)! as URL)
                                            
                                            self.dismiss(animated: false, completion: nil)
                                        }else{
                                            self.dismiss(animated: false, completion: nil)
                                        }
                                    }

                                }
                            }
                        }
                    }
                    else if (userGroup as AnyObject) as! String == "3" {
                        self.dismiss(animated: false, completion: {
                            self.delegate?.showTherapistLoginPage()
                        })
                    }
                    }
                    else{
                        if let _ = NSUSERDEFAULT.value(forKey: kIsSubs) {
                            let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
                            print(isSubscriptionValue)
                            if isSubscriptionValue == "1" {
                                self.dismiss(animated: false, completion: {
                                    self.delegate?.showRequestUrlPage(url: self.requestUrl,slug: self.slug)
                                })
                            }
                            else if isSubscriptionValue == "0" {
                                self.onlyLoginBackView.isHidden = true
                                if self.isCoupon == "1"{
                                    
                                    self.dismiss(animated: false, completion: {
                                        
                                        self.delegate?.showRequestUrlPage(url: self.requestUrl,slug: self.slug)
                                    })
                                    
                                }else{
                                    AlertController.alert(title: "It seems, you are not a Massage Advantage Member click the below link to buy membership Plan", message: "", buttons: ["Cancel","Click Here"]) { (alert, index) in
                                        
                                        if index == 1{
                UIApplication.shared.openURL(NSURL(string: kSubscriptionPlanUrl)! as URL)
                                            
                                            self.dismiss(animated: false, completion: nil)
                                        }else{
                                            self.dismiss(animated: false, completion: nil)
                                        }
                                    }
                                    
                                }
                            }
                        }
                        else if (userGroup as AnyObject) as! String == "3" {
                            self.dismiss(animated: false, completion: {
                                self.delegate?.showTherapistLoginPage()
                            })
                        }
                    }
                }
            }
        }

        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }

    @IBAction func registerAction(_ sender: UIButton) {

        let validationObj = ValidationClass.verifyFieldForRegisterUser(name: registerNameField.text!, email: registerEmailField.text!, phone: registerPhoneField.text!, password: registerPasswordField.text!)

        if validationObj.1 {

            APIManager.apiForRegisterUser(Name:  registerNameField.text!, Email: registerEmailField.text!, Phone: registerPhoneField.text!, Password: registerPasswordField.text!) { (status, msg) in

                if status{
                    if self.isCoupon == "1"{
                        
                        NSUSERDEFAULT.set(self.enterCouponTextField.text!, forKey: kCouponCode)
                    }
                self.dismiss(animated: false, completion: {
                    MessageView.showMessage(message: msg, time: 2.0,verticalAlignment: .bottom)
                    if self.requestUrl == ""
                    {
                        self.delegate?.showDetailPage(slug: self.slug,dict: self.couponDict)

                    }else{
                        self.delegate?.showRequestUrlPage(url: self.requestUrl,slug: self.slug)
                    }
                })

                }
            }
        }
        
        else {
            _ = AlertController.alert(title: "", message:  validationObj.0)
        }
     
    }
    
    
    @IBAction func logInPasswordBtnAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            loginPaswordField.isSecureTextEntry = false
            loginPasswordBtn.setImage(UIImage (named: "eye"), for: .normal)
            
        }
        else {
            loginPaswordField.isSecureTextEntry = true
            loginPasswordBtn.setImage(UIImage (named: "hide"), for: .normal)
            
        }
        
    }
    
    @IBAction func registerPasswordBtnAction(_ sender: UIButton) {
        
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            registerPasswordField.isSecureTextEntry = false
            registerPasswordBtn.setImage(UIImage (named: "eye"), for: .normal)
            
        }
        else {
            registerPasswordField.isSecureTextEntry = true
            registerPasswordBtn.setImage(UIImage (named: "hide"), for: .normal)
            
        }
    }
    
    //Only Login
    
    @IBAction func onlyLoginSubmitButtonAction(_ sender: UIButton) {
        
        let validationObj = ValidationClass.verifyFieldForLogin(email: self.onlyLoginEmailField.text!, password: self.onlyLoginPasswordField.text!)
        
        if validationObj.1{
            
            APIManager.apiForLogin(email: self.onlyLoginEmailField.text!, password: self.onlyLoginPasswordField.text!) { (status) in
                if status{
                    
                    let userGroup = NSUSERDEFAULT.value(forKey: kUserGroup)
                    if self.requestUrl == ""
                    {
                    if (userGroup as AnyObject) as! String == "2" {
                        
                        if let _ = NSUSERDEFAULT.value(forKey: kIsSubs) {
                            
                            let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
                            print(isSubscriptionValue)
                            if isSubscriptionValue == "1" {
                                
                                self.dismiss(animated: false, completion: {
                                    self.delegate?.showDetailPage(slug: self.slug,dict: self.couponDict)
                                })
                            }
                                
                            else if isSubscriptionValue == "0" {
                                
                                self.onlyLoginBackView.isHidden = true
                                
                                AlertController.alert(title: "It seems, you are not a Massage Advantage Member click the below link to buy membership Plan", message: "", buttons: ["Cancel","Click Here"]) { (alert, index) in
                                    
                                    if index == 1{
                                 UIApplication.shared.openURL(NSURL(string: kSubscriptionPlanUrl)! as URL)
                                        
                                        self.dismiss(animated: false, completion: nil)
                                    }else{
                                        self.dismiss(animated: false, completion: nil)
                                    }
                                }
                                
                            }
                            
                        }
                    }
                    else if (userGroup as AnyObject) as! String == "3" {
                        self.dismiss(animated: false, completion: {
                            self.delegate?.showTherapistLoginPage()
                        })
                    }
                }
                    else{
                        
                        if let _ = NSUSERDEFAULT.value(forKey: kIsSubs) {
                            
                            let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
                            print(isSubscriptionValue)
                            if isSubscriptionValue == "1" {
                                self.dismiss(animated: false, completion: {
                                    self.delegate?.showRequestUrlPage(url: self.requestUrl,slug: self.slug)
                                })
                            }
                                
                            else if isSubscriptionValue == "0" {
                                self.onlyLoginBackView.isHidden = true
                                AlertController.alert(title: "It seems, you are not a Massage Advantage Member click the below link to buy membership Plan", message: "", buttons: ["Cancel","Click Here"]) { (alert, index) in
                                    if index == 1{
                            UIApplication.shared.openURL(NSURL(string: kSubscriptionPlanUrl)! as URL)
                                        
                                        self.dismiss(animated: false, completion: nil)
                                    }else{
                                        self.dismiss(animated: false, completion: nil)
                                    }
                                }
                            }
                        }
                        else if (userGroup as AnyObject) as! String == "3" {
                            self.dismiss(animated: false, completion: {
                                self.delegate?.showTherapistLoginPage()
                            })
                        }
                    }
                }
            }
        }
            
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }
    
    @IBAction func onlyLoginEyePasswordButtonAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            onlyLoginPasswordField.isSecureTextEntry = false
            onlyEyePasswordButton.setImage(UIImage (named: "eye"), for: .normal)
        }
        else {
            onlyLoginPasswordField.isSecureTextEntry = true
            onlyEyePasswordButton.setImage(UIImage (named: "hide"), for: .normal)
            
        }
    }
    
    @IBAction func noLoginCouponAction(_ sender: UIButton) {
        
        noLoginMassageButton.isSelected = false
        
        if noLoginCouponButton.isSelected{
            
            noLoginCouponButton.isSelected = false
            
        }else{
            noLoginCouponButton.isSelected = true
        }
        
    }
    
    @IBAction func noLoginMassageAction(_ sender: UIButton) {
        
        noLoginCouponButton.isSelected = false
       
        if noLoginMassageButton.isSelected{

            noLoginMassageButton.isSelected = false

        }else{

            noLoginMassageButton.isSelected = true

        }
    }
    
    @IBAction func noLoginOkAction(_ sender: UIButton) {
        
        if noLoginCouponButton.isSelected == false && noLoginMassageButton.isSelected == false{
            
            AlertController.alert(title: "Please select your choice.")
        }
        else{
            
            if self.noLoginCouponButton.isSelected {
                
                couponBackView.isHidden = false
                loginBackView.isHidden = true
                onlyLoginBackView.isHidden = true
                authBckView.isHidden = true
                noLoginView.isHidden = true
                
                
            }else{
                
                couponBackView.isHidden = true
                loginBackView.isHidden = true
                onlyLoginBackView.isHidden = false
                authBckView.isHidden = true
                noLoginView.isHidden = true
            }
            
        }
    }

    @IBAction func noLoginCrossAction(_ sender: UIButton) {
        
         dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func lookingForDealBtnAction(_ sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "RetailPlansViewController")as! RetailPlansViewController
        newViewController.isFromCoupon = true
        self.present(newViewController, animated: true, completion: nil)
    }
    
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 60
            {
                return false
            }
            break
      
        default:
            break
        }
        
        //MARK:- Does'nt Support Emojies
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
    
    
}
