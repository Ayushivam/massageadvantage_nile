//
//  MAReedemImagePopUpViewController.swift
//  MassageAdvantage
//
//  Created by nile on 11/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAReedemImagePopUpViewController: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    var productImage = ""
    override func viewDidLoad() {
        super.viewDidLoad()
 productImageView.sd_setImage(with: URL(string: productImage), placeholderImage: UIImage(named:"default"), options: SDWebImageOptions(rawValue: 0))
        // Do any additional setup after loading the view.
    }
    
    //MARK:- UI Button Actions
    @IBAction func crossBtnAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
    }
   
}
