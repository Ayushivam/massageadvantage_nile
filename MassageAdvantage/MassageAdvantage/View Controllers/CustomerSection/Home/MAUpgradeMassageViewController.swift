//
//  MAUpgradeMassageViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 13/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAUpgradeMassageViewController: UIViewController {
   
    @IBOutlet weak var discHeadingLbl: UILabel!
    @IBOutlet weak var couponDiscHeadingLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var subTotalAmountLbl: UILabel!
    @IBOutlet weak var discountAmountLbl: UILabel!
    @IBOutlet weak var upgradePriceAmountLbl: UILabel!
    @IBOutlet weak var swedishMassageAmountLbl: UILabel!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var upgradeMassageTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var remarkTextView: UITextView!
    @IBOutlet weak var payButton: UIButton!
    
    var couponType = ""
    var upgradePlanArray = [UserInfo]()
    var checkout = [UserInfo]()
    var obj = UserInfo()
    var couponDict = Dictionary<String,AnyObject>()
    var date = ""
    var slot = ""
    var subDiscountPrice = ""
    var serviceId = ""
    var headingName = ""
    var price = ""
    var needToPay = ""
    
    var discount = ""
    var couponDisc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headingLbl.text = headingName
        remarkTextView.textColor = UIColor.lightGray
        remarkTextView.text = "Remark or Instructions"
        for i in 0..<obj.serviceArray.count{

            let index = obj.serviceArray[i]
            index.isSelect = ""
            self.obj.serviceArray[i] = index
        }
        resetDataInitial()
       
                // Do any additional setup after loading the view.
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
        
    }
    
    //First poisition for service
    func resetDataInitial()
    {
        for i in 0..<obj.serviceArray.count{
            
            let index = obj.serviceArray[i]
            if index.serviceId == "1"{
                index.isSelect = "1"
                let couponType : String = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String
                if couponType == "1"{
                    index.isGroupon = "1"
                }
                
                obj.serviceArray[i] = index
                self.serviceId = index.serviceId
                self.price = index.price
                self.subDiscountPrice = index.subDiscountPrice
               
                //CheckOut Array
                
                let obj = checkout[0]
                self.swedishMassageAmountLbl.text = "$" + obj.swedishPrice
                let upgradePriceFloat = (obj.upgradePrice as NSString).floatValue
                let discountPriceFloat : Float = (obj.subDiscount as NSString).floatValue
                let subValue : Float = (100 - discountPriceFloat)
                let upgradeValueFloat : Float = upgradePriceFloat * 100/subValue
                let upgradeValue = String(format: "%.2f", upgradeValueFloat)
                let discountValueFloat : Float = upgradeValueFloat - upgradePriceFloat
                let discountValue = String(format: "%.2f", discountValueFloat)
                upgradePriceAmountLbl.text = "$" + upgradeValue
                
                if obj.couponValue == "" {
                    
                    couponDiscountLbl.isHidden = true
                    couponDiscHeadingLbl.isHidden = true
                    
                }else {
                    couponDiscountLbl.isHidden = false
                    couponDiscountLbl.text = "$" + obj.couponValue
                    couponDiscHeadingLbl.isHidden = false
                }
                
                if obj.subDiscount == "" {
                    discountAmountLbl.isHidden = true
                    discHeadingLbl.isHidden = true
                }
                    
                else {
                    discountAmountLbl.text = "- $" + discountValue
                }
                
                if obj.subTotal == "" {
                     subTotalAmountLbl.text = "$0"
                     totalAmountLbl.text = "$0"
                }
                else {
                subTotalAmountLbl.text = "$" + obj.subTotal
                totalAmountLbl.text = "$" + obj.subTotal
                }
                
                if obj.subTotal == ""
                {
                     subTotalAmountLbl.text = "$0"
                }
                
                self.needToPay = obj.needToPay
                
                if obj.subTotal == "" || obj.subTotal == "0" || obj.subTotal == "0.0"{
                    self.payButton.setTitle("Confirm", for: .normal)
                }
                else{
                    self.payButton.setTitle("Pay", for: .normal)
                }
                
                if couponType == "1"{
                    self.swedishMassageAmountLbl.text = "$0"
                    self.couponDiscountLbl.text = "$0"
                }
                self.upgradeMassageTableView.reloadData()
            }
        }
    }
    
    @IBAction func payButtonAction(_ sender: UIButton) {
        
        view.endEditing(true)
        
        if self.payButton.currentTitle == "Confirm"{
            
            let slotCut = self.slot.components(separatedBy: " -")
            
            let coupon : String = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String
            
            if coupon == "" {
                self.couponType = "2"
            }
            
            else {
                 self.couponType = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String

            }
            
            let couponId : String = self.couponDict.validatedValue("coupon_id", expected: "" as AnyObject) as! String
            var paramDict = Dictionary<String,AnyObject>()
            let userId : String = NSUSERDEFAULT.value(forKey: kUserId) as! String
            
            paramDict["user_id"] = userId as AnyObject
            paramDict["service"] = self.serviceId as AnyObject
            paramDict["therapist"] = obj.therapistDetailID as AnyObject
            paramDict["select_date"] = self.date as AnyObject
            paramDict["select_time"] = slotCut[0] as AnyObject
            
            if self.remarkTextView.text == "Remark or Instructions" || self.remarkTextView.text == ""{
              
                paramDict["instruction"] = "" as AnyObject
            }else{
            
                paramDict["instruction"] = self.remarkTextView.text as AnyObject
            }
            
            paramDict["price"] = self.needToPay as AnyObject
            paramDict["coupon_type"] = couponType as AnyObject
            paramDict["txn_id"] = "" as AnyObject
            paramDict["coupon_id"] = couponId as AnyObject
            paramDict["service_price"] = self.price as AnyObject
            
            APIManager.apiForConfirmBooking(dict:paramDict) { (status, dict) in
                if status{
                    let data : Dictionary<String,AnyObject> = dict.validatedValue("data", expected: [:] as AnyObject) as!
                        
                        Dictionary<String,AnyObject>
                    let bookingId : String = data.validatedValue("booking_id", expected: "" as AnyObject) as! String
                    AlertController.alert(title: "Booking Successful", message: dict.validatedValue("message", expected: "" as AnyObject) as! String, buttons: ["View Details"], tapBlock: { (alert, index) in
                        let bookingVC = storyBoardForName(name: "Main").instantiateViewController(withIdentifier: "ConfirmBookingViewController")as! ConfirmBookingViewController
                        bookingVC.bookingID = bookingId
                        self.navigationController!.pushViewController(bookingVC, animated: true)
                        
                    })
                }
            }
        }
        else{

            let massageDetailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "PaymentViewController")as! PaymentViewController
        massageDetailVC.amount = self.needToPay
        massageDetailVC.delegate = self
        self.navigationController!.pushViewController(massageDetailVC, animated: true)

        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension MAUpgradeMassageViewController : UITableViewDelegate,UITableViewDataSource,UpgradeeViewCellDelegate{
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Book your massage"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35.0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return  obj.serviceArray.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 110
           }
        else {
            return 85
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAUpgradeMassageTableViewCell") as! MAUpgradeMassageTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.post = obj.serviceArray[indexPath.row]
        
        if indexPath.row == 0 {
            cell.addOnServicesView.isHidden = false
        }
        else {
            cell.addOnServicesView.isHidden = true
        }
        
        if obj.serviceArray.count == 1 {
             cell.addOnServicesView.isHidden = true
        }
        else {
            cell.addOnServicesView.isHidden = false
        }
        
        return cell
    }

    func selectTapAction(_sender: MAUpgradeMassageTableViewCell) {
         guard let tappedIndexPath = upgradeMassageTableView.indexPath(for: _sender) else { return }

        if self.obj.serviceArray[tappedIndexPath.row].serviceId == "1"{
            return
        }
        
        for i in 0..<self.obj.serviceArray.count{
             let obj =  self.obj.serviceArray[i]
            if obj.serviceId != "1"{
           
            obj.isSelect = ""
            self.obj.serviceArray[i] = obj
            
            }
        }

        let obj = self.obj.serviceArray[tappedIndexPath.row]
        obj.isSelect = "1"
        self.price = obj.price
      //  self.dummyAmount = obj.dummyTotal
        self.serviceId = obj.serviceId
        
        //Coupon Type
        let couponPrice : String = self.couponDict.validatedValue("price", expected: "" as AnyObject) as! String
        let coupon : String = self.couponDict.validatedValue("coupon", expected: "" as AnyObject) as! String
        
        var checkOutprice = obj.subDiscountPrice
        
        if checkOutprice == "" {
            checkOutprice = obj.price
        }
        else {
            checkOutprice = obj.subDiscountPrice
        }

        APIManager.apiForBookingCheckout(price: checkOutprice, coupon: coupon, couponPrice: couponPrice, therapistId: self.obj.therapistDetailID, serviceId: obj.serviceId) { (status,dict) in
            if status {
                
                self.swedishMassageAmountLbl.text =  "$" + (dict.validatedValue("swedish_price", expected: "" as AnyObject)as! String)
                
                  let upgradePriceFloat = ((dict.validatedValue("upgrade_price", expected: "" as AnyObject)as! String) as NSString).floatValue
                self.discount = (dict.validatedValue("sub_discount", expected: "" as AnyObject)as! String)
                let discountPriceFloat = ((dict.validatedValue("sub_discount", expected: "" as AnyObject)as! String) as NSString).floatValue
                let subValue : Float = (100 - discountPriceFloat)
                let upgradeValueFloat : Float = upgradePriceFloat * 100/subValue
                let upgradeValue = String(format: "%.2f", upgradeValueFloat)
                let discountValueFloat : Float = upgradeValueFloat - upgradePriceFloat
                let discountValue = String(format: "%.2f", discountValueFloat)
                self.upgradePriceAmountLbl.text =  "$" + upgradeValue
                self.subTotalAmountLbl.text =   "$" + (dict.validatedValue("sub_total", expected: "" as AnyObject)as! String)
                self.totalAmountLbl.text =  "$" + (dict.validatedValue("need_to_pay", expected: "" as AnyObject)as! String)
                self.couponDisc =  (dict.validatedValue("cup_discount", expected: "" as AnyObject)as! String)
                
                if self.discount == "" {
                    self.discountAmountLbl.isHidden = true
                    self.discHeadingLbl.isHidden = true
                }
                else {
                    self.discountAmountLbl.isHidden = false
                    self.discHeadingLbl.isHidden = false
                    self.discountAmountLbl.text =  "- $" + discountValue
                }
                
                if self.couponDisc == "" {
                    self.couponDiscountLbl.isHidden = true
                    self.couponDiscHeadingLbl.isHidden = true
                }
                else {
                    self.couponDiscountLbl.text =  "$" + self.couponDisc 
                    self.couponDiscountLbl.isHidden = false
                    self.couponDiscHeadingLbl.isHidden = false
                }
                let couponType : String = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String

                if couponType == "1"{
                    self.swedishMassageAmountLbl.text = "$0"
                    self.couponDiscountLbl.text = "$0"
                }
                
                
                self.needToPay = dict.validatedValue("need_to_pay", expected: "" as AnyObject)as! String
                if self.needToPay == "" || self.needToPay == "0" || self.needToPay == "0.0"{
                    self.payButton.setTitle("Confirm", for: .normal)
                }
                else{
                    self.payButton.setTitle("Pay", for: .normal)
                }
            }
        }
        if obj.totalAmount == "0.00" || obj.totalAmount == "0" || obj.totalAmount ==  "0.0"{
            self.payButton.setTitle("Confirm", for: .normal)
        }else{
            self.payButton.setTitle("Pay", for: .normal)
        }
        self.obj.serviceArray[tappedIndexPath.row] = obj
        self.upgradeMassageTableView.reloadData()
    }
    
    
    func selectActionClearBtn(_sender: MAUpgradeMassageTableViewCell) {
        
        for i in 0..<self.obj.serviceArray.count{
            
        let index = self.obj.serviceArray[i]
            if index.serviceId != "1"{
                
                index.isSelect = ""
                self.obj.serviceArray[i] = index
            }
        }
       // self.upgradeMassageTableView.reloadData ()
        resetDataInitial()
    }
}

extension MAUpgradeMassageViewController : UITextViewDelegate,SuccessPayment{
    
    func textViewDidBeginEditing(_ textView: UITextView) {

        if remarkTextView.text == "Remark or Instructions"{
            remarkTextView.textColor = UIColor.darkGray
            remarkTextView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            remarkTextView.textColor = UIColor.lightGray
            remarkTextView.text = "Remark or Instructions"
        }
    }

    func notifySuccessPayment(transactionId:String)
    {
        print(transactionId)
        if transactionId != ""
        {
            let slotCut = self.slot.components(separatedBy: " -")
            let coupon : String = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String
            if coupon == "" {
                self.couponType = "2"
            }
                
            else {
                self.couponType = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String
                
            }
            
            let couponId : String = self.couponDict.validatedValue("coupon_id", expected: "" as AnyObject) as! String
            var paramDict = Dictionary<String,AnyObject>()
            let userId : String = NSUSERDEFAULT.value(forKey: kUserId) as! String
            paramDict["user_id"] = userId as AnyObject
            paramDict["service"] = self.serviceId as AnyObject
            paramDict["therapist"] = obj.therapistDetailID as AnyObject
            paramDict["select_date"] = self.date as AnyObject
            paramDict["select_time"] = slotCut[0] as AnyObject
            if self.remarkTextView.text == "Remark or Instructions" || self.remarkTextView.text == ""{
                
                paramDict["instruction"] = "" as AnyObject
            }else{
                paramDict["instruction"] = self.remarkTextView.text as AnyObject
            }
            paramDict["price"] = self.needToPay as AnyObject
            paramDict["coupon_type"] = couponType as AnyObject
            paramDict["txn_id"] = transactionId as AnyObject
            paramDict["coupon_id"] = couponId as AnyObject
            paramDict["service_price"] = self.price as AnyObject
            
            APIManager.apiForConfirmBooking(dict:paramDict) { (status, dict) in
                if status{
                    
                    let data : Dictionary<String,AnyObject> = dict.validatedValue("data", expected: [:] as AnyObject) as! Dictionary<String,AnyObject>
                    
                    let bookingId : String = data.validatedValue("booking_id", expected: "" as AnyObject) as! String
                    
                    AlertController.alert(title: "Booking Successful", message: dict.validatedValue("message", expected: "" as AnyObject) as! String, buttons: ["View Details"], tapBlock: { (alert, index) in
                        let bookingVC = storyBoardForName(name: "Main").instantiateViewController(withIdentifier: "ConfirmBookingViewController")as! ConfirmBookingViewController
                        bookingVC.bookingID = bookingId
                        self.navigationController!.pushViewController(bookingVC, animated: true)
                        
                    })
                }
            }
        }
    }
}
