//
//  MAMassageTypesViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 10/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAMassageTypesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var massageNameLbl: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var massageTypesCollectionView: UICollectionView!
    
    var searchPageNo = 1
    var therapistNameListArray = [UserInfo]()
    var massageName = ""
    var serviceId = ""
    var therapistName = ""
    var latitude = ""
    var longitude = ""
    var distance = ""
    var couponType = ""
    
    var couponDict = Dictionary<String,AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initialSetUp()
        
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp() {
        massageNameLbl.text = massageName
        APIManager.apiForSearchTherapist(selectService: serviceId, therapistName: therapistName, lat: latitude, long : longitude,distance: distance, couponType: couponType,page: self.searchPageNo,loader: .default) { (status,therapistListArry) in
            if status{
                self.therapistNameListArray += UserInfo.therapistListArray(responseArray: therapistListArry as! Array<Dictionary<String, Any>>)
                self.massageTypesCollectionView.reloadData()
            }
            else{
                let _ = (self.therapistNameListArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
            }
        }
    }
    
    func callReloadApi()
    {
        
        if ((massageTypesCollectionView.contentOffset.y + massageTypesCollectionView.frame.size.height) >= massageTypesCollectionView.contentSize.height)
        {
            searchPageNo = searchPageNo + 1
            APIManager.apiForSearchTherapist(selectService: serviceId, therapistName: therapistName, lat: latitude, long : longitude,distance: distance, couponType: couponType,page: self.searchPageNo,loader: .smoothProgress) { (status,therapistListArry) in
                if status{
                    self.therapistNameListArray += UserInfo.therapistListArray(responseArray: therapistListArry as! Array<Dictionary<String, Any>>)
                    self.massageTypesCollectionView.reloadData()
                }
                else{
                    let _ = (self.therapistNameListArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
                }
            }
        }
    }
    
    
    
    //MARK :- Btn Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK:- Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return therapistNameListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MAMassageTypesCollectionViewCell", for: indexPath) as! MAMassageTypesCollectionViewCell
        cell.post = therapistNameListArray[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 2
        var widthRemainingForCellContent = collectionView.bounds.width
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
        }
        
        let cellWidth = widthRemainingForCellContent / cellsAcross
        return CGSize(width: cellWidth, height: 220)
    }
}

extension MAMassageTypesViewController : MassageTypeCellDelegate,BookDelegate {
    
    func cellTapAction(_sender: MAMassageTypesCollectionViewCell) {
        
        guard let tappedIndexPath = massageTypesCollectionView.indexPath(for: _sender) else { return }
        
        let requestURL = self.therapistNameListArray[tappedIndexPath.row].requestURL
        
        if let _ = NSUSERDEFAULT.value(forKey: kCouponCode)
        {
            
            if requestURL != ""{
                APIManager.apiForRequestUrl(slug: self.therapistNameListArray[tappedIndexPath.row].slug) { (status) in
                    if status{
                        UIApplication.shared.openURL(NSURL(string: requestURL)! as URL)
                    }
                }
                
            }else{
                let couponCode : String = NSUSERDEFAULT.value(forKey: kCouponCode) as! String
                APIManager.apiForVerifyCouponCode(couponCode:couponCode ) { (status,msg,dict) in
                    if status {
                        self.couponDict = dict
                        self.showDetailPage(slug: self.therapistNameListArray[tappedIndexPath.row].slug, dict:self.couponDict)
                    } else
                    {
                        NSUSERDEFAULT.removeObject(forKey: kCouponCode)
                    }
                }
            }
        }else{
            if couponType == "1" {
                
                let obj = self.therapistNameListArray[tappedIndexPath.row]
                let slug = obj.slug
                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MACouponViewController") as! MACouponViewController
                popOverVC.delegate = self
                popOverVC.slug = slug
                popOverVC.isCoupon = couponType
                popOverVC.requestUrl = requestURL
                popOverVC.providesPresentationContextTransitionStyle = true
                popOverVC.definesPresentationContext = true
                popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                popOverVC.view.backgroundColor = UIColor.init(white : 0.50, alpha: 0.75)
                self.present(popOverVC, animated: true, completion: nil)
            }
            else {
                
                if let _ = NSUSERDEFAULT.value(forKey: kUserId) {
                    
                    if let _ = NSUSERDEFAULT.value(forKey: kIsSubs) {
                        
                        let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
                        if isSubscriptionValue == "1"
                        {
                            if requestURL == ""
                            {
                                let obj = self.therapistNameListArray[tappedIndexPath.row]
                                let slug = obj.slug
                                let massageDetailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "MAMassgaeTypeDetailViewController") as! MAMassgaeTypeDetailViewController
                                massageDetailVC.slug = slug
                                massageDetailVC.couponDict = [:]
                                self.navigationController!.pushViewController(massageDetailVC, animated: true)
                            }else{
                                APIManager.apiForRequestUrl(slug: self.therapistNameListArray[tappedIndexPath.row].slug) { (status) in
                                    if status{
                                        UIApplication.shared.openURL(NSURL(string: requestURL)! as URL)
                                    }
                                }
                            }
                        }
                        else{

                            AlertController.alert(title: "It seems, you are not a Massage Advantage Member click the below link to buy membership Plan", message: "", buttons: ["Cancel","Click Here"]) { (alert, index) in
                                
                                if index == 1{
                                    UIApplication.shared.openURL(NSURL(string: kSubscriptionPlanUrl)! as URL)
                                }
                            }
                        }
                    }
                }
                else  {
                    let obj = self.therapistNameListArray[tappedIndexPath.row]
                    let slug = obj.slug
                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MACouponViewController") as! MACouponViewController
                    popOverVC.delegate = self
                    popOverVC.slug = slug
                    popOverVC.isCoupon = couponType
                    popOverVC.requestUrl = requestURL
                    popOverVC.providesPresentationContextTransitionStyle = true
                    popOverVC.definesPresentationContext = true
                    popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                    popOverVC.view.backgroundColor = UIColor.init(white : 0.50, alpha: 0.75)
                    self.present(popOverVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if self.therapistNameListArray.count != 0{
            callReloadApi()
        }
    }
    
    func showDetailPage(slug:String,dict:Dictionary<String,AnyObject>){
        
        self.couponDict = dict
        
        let massageDetailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "MAMassgaeTypeDetailViewController") as! MAMassgaeTypeDetailViewController
        massageDetailVC.slug = slug
        massageDetailVC.couponDict = self.couponDict
        self.navigationController!.pushViewController(massageDetailVC, animated: true)
        
    }
    
    func showTherapistLoginPage()
    {
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Therapist", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "TherapistTabViewController")as! TherapistTabViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
        
    }
    
    func showRequestUrlPage(url:String,slug: String){
        
        APIManager.apiForRequestUrl(slug: slug) { (status) in
            if status{
                UIApplication.shared.openURL(NSURL(string: url)! as URL)
            }
        }
    }
}
