//
//  MAHomeViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 08/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAHomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var homeTableView: UITableView!
    var getServiceNameListArray = [UserInfo]()
    let couponType = "2"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadApi(notification:)), name: Notification.Name("reloadHomeApi"), object: nil)
        
        APIManager.apiForGetServiceName{ (status,seviceNameListArr) in
            if status{
                self.getServiceNameListArray.removeAll()
                self.getServiceNameListArray += UserInfo.getServiceNameListArray(responseArray: seviceNameListArr as! Array<Dictionary<String, Any>>)
                let _ = (self.getServiceNameListArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
                self.homeTableView.reloadData()
                
            }
            
        }
        
        // Do any additional setup after loading the view.
    }

    deinit {
        //  Disable the keyboard manager.
                //Removing notification observers on dealloc.
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func reloadApi(notification: Notification) {
        
        APIManager.apiForGetServiceName{ (status,seviceNameListArr) in
            if status{
                self.getServiceNameListArray.removeAll()
                self.getServiceNameListArray += UserInfo.getServiceNameListArray(responseArray: seviceNameListArr as! Array<Dictionary<String, Any>>)
                let _ = (self.getServiceNameListArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
                self.homeTableView.reloadData()
                
            }
        }
    }
    
    //MARK:- UI Table View Delegate and Datasource
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 381
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       

        return getServiceNameListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAHomeTableViewCell", for: indexPath) as! MAHomeTableViewCell
        cell.post = getServiceNameListArray[indexPath.row]
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
}

extension MAHomeViewController : HomeTableViewCellDelegate{
    
    func cellTapAction(_sender: MAHomeTableViewCell) {

        guard let tappedIndexPath = homeTableView.indexPath(for: _sender) else { return }
        let obj = self.getServiceNameListArray[tappedIndexPath.row]
         let serId = obj.serviceId
        let massageName = obj.serviceName
        let massageTypeVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "MAMassageTypesViewController")as! MAMassageTypesViewController
        massageTypeVC.serviceId = serId
        massageTypeVC.massageName = massageName
        massageTypeVC.couponType = couponType
        APPDELEGATE.navController?.pushViewController(massageTypeVC, animated: true)
        
    }
}
