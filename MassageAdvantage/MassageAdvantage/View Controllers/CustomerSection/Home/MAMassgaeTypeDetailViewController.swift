//
//  MAMassgaeTypeDetailViewController.swift
//  MassageAdvantage
//
//  Created by nile on 27/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAMassgaeTypeDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    @IBOutlet weak var continueBtn: UIButton!
    
    @IBOutlet weak var ownerDetalCollectionView: UICollectionView!
    
    @IBOutlet weak var massageTypeDetailTableViewOutlet: UITableView!
    @IBOutlet weak var topHeadingLbl: UILabel!
 
    var slug = ""
    var bookingDetailDate = ""
    var date = Date()
    var formatter = DateFormatter()
    var bookingDetailObj = UserInfo()
    var coverImageArray = [UserInfo]()
    var checkoutArray = [UserInfo]()
    var reviewArray = [UserInfo]()

    var serviceArray = [UserInfo]()
    var availableDaysArray = [UserInfo]()
    var notAvailabelDatesArray = Array<String>()
    var couponDict = Dictionary<String,AnyObject>()
    //   var notAvailableArray = [String]()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let coupon : String = couponDict.validatedValue("coupon", expected: "" as AnyObject) as! String
        
        APIManager.apiForBookingDetails(slugName: slug, coupon: coupon) { (status, dict,checkOutArray,reviewArray) in
            if status {
                self.bookingDetailObj.therapistDetailID = dict.validatedValue("therapist_detail_id", expected: "" as AnyObject) as! String
                self.bookingDetailObj.therapistID = dict.validatedValue("therapist_id", expected: "" as AnyObject)as! String
                self.bookingDetailObj.serviceName = dict.validatedValue("business_name", expected: "" as AnyObject)as! String
                self.bookingDetailObj.slug = dict.validatedValue("slug", expected: "" as AnyObject)as! String
                self.bookingDetailObj.desc = dict.validatedValue("description", expected: "" as AnyObject)as! String
                self.bookingDetailObj.address = dict.validatedValue("location", expected: "" as AnyObject)as! String
                self.bookingDetailObj.numberOfTherapist = dict.validatedValue("no_of_therapist", expected: "" as AnyObject)as! String
                self.bookingDetailObj.notAvlFromDate = dict.validatedValue("not_avl_from_date", expected: "" as AnyObject)as! String
                self.bookingDetailObj.notAvlToDate = dict.validatedValue("not_avl_to_date", expected: "" as AnyObject)as! String
                self.bookingDetailObj.gender = dict.validatedValue("gender", expected: "" as AnyObject)as! String
                self.bookingDetailObj.acceptCoupon = dict.validatedValue("accept_coupon", expected: "" as AnyObject)as! String
                self.bookingDetailObj.acceptNoOfCoupon = dict.validatedValue("accept_no_of_coupon", expected: "" as AnyObject)as! String
                self.bookingDetailObj.phoneNumber = dict.validatedValue("contact", expected: "" as AnyObject)as! String
                self.bookingDetailObj.email = dict.validatedValue("email", expected: "" as AnyObject)as! String
                self.bookingDetailObj.profileImg = dict.validatedValue("profile_thumbnail", expected: "" as AnyObject)as! String
                self.bookingDetailObj.rating = dict.validatedValue("rating", expected: "" as AnyObject)as! String
                self.bookingDetailObj.name = dict.validatedValue("user_name", expected: "" as AnyObject)as! String
                self.topHeadingLbl.text =  self.bookingDetailObj.serviceName

                let therapistImgArr : Array = dict.validatedValue("gallery_map", expected: [] as AnyObject) as! Array<AnyObject>
                
                    self.coverImageArray += UserInfo.therapistImageArray(responseArray: therapistImgArr as! Array<Dictionary<String, Any>>,theripistId:self.bookingDetailObj.therapistDetailID)
                
                    self.bookingDetailObj.notAvailabelDatesArray = dict.validatedValue("not_available_dates", expected: [] as AnyObject) as! Array<String>
               
                    self.bookingDetailObj.availableDaysArray += UserInfo.getAvailabelDaysArray(responseArray: dict.validatedValue("available_days", expected: [] as AnyObject) as! Array<Dictionary<String, Any>>)
                
                    self.bookingDetailObj.serviceArray += UserInfo.getServiceArray(responseArray: dict.validatedValue("service_map", expected: [] as AnyObject) as! Array<Dictionary<String, Any>>)
                
                    self.checkoutArray += UserInfo.checkoutArray(responseArray: checkOutArray as! Array<Dictionary<String, Any>>)
                    self.reviewArray +=  UserInfo.reviewArray(responseArray: reviewArray as!Array<Dictionary<String, Any>>)
                    self.massageTypeDetailTableViewOutlet.reloadData()
                    self.ownerDetalCollectionView.reloadData()

            }
        }        
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {

        APPDELEGATE.backgroundApiForUpdateStatus()
        
        if let _ = NSUSERDEFAULT.value(forKey: kIsSubs) {

            let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
            let couponType : String = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String
            
            if isSubscriptionValue == "0" && couponType == "1" {
                
                AlertController.alert(title: "It seems, you are not a Massage Advantage Member click the below link to buy membership Plan", message: "", buttons: ["Not Now","Click Here"]) { (alert, index) in
                    if index == 1{
                        UIApplication.shared.openURL(NSURL(string: kSubscriptionPlanUrl)! as URL)
                       // self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {

    }

    //MARK:- UIBUTTON ACTION

    @IBAction func bckBtnAction(_ sender: UIButton) {
        
//        if let _ = NSUSERDEFAULT.value(forKey: kIsSubs){
//            let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
//
//            if isSubscriptionValue == "0"{
//
//                for controller in self.navigationController!.viewControllers as Array {
//                    if controller.isKind(of: MATabViewController.self) {
//                        self.navigationController!.popToViewController(controller, animated: true)
//                        break
//                    }
//                }
//
//            }else{
//                 self.navigationController?.popViewController(animated: true)
//            }
//
//        }
//
//        else{
        
            self.navigationController?.popViewController(animated: true)
            
//        }
    }
    
    
    @IBAction func continueBtnAction(_ sender: UIButton) {
        
        let massageDetailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "MADateAndTimeViewController")as! MADateAndTimeViewController
        
        massageDetailVC.obj = self.bookingDetailObj
        massageDetailVC.checkout = self.checkoutArray
        massageDetailVC.couponDict = self.couponDict
        massageDetailVC.headingName = self.bookingDetailObj.serviceName
        self.navigationController!.pushViewController(massageDetailVC, animated: true)
    }
    
    //MARK:- UI Table View Delegate And Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MAOwnerDetailTableViewCell", for: indexPath) as! MAOwnerDetailTableViewCell
            cell.serviceName.text = bookingDetailObj.serviceName
            cell.phoneNumberLbl.text = bookingDetailObj.phoneNumber
            cell.emailLbl.text = ""
            cell.therapistNameLbl.text = bookingDetailObj.name
            
            cell.readReviewsBtn.tag = indexPath.row
            let readReviews = reviewArray.count
            if readReviews == 0 {
                cell.readReviewsBtn.setTitle("Read Reviews(0)", for: .normal)
            }
            else {
                cell.readReviewsBtn.setTitle("Read Reviews(" + "\(readReviews)" + ")", for: .normal)

            }
            cell.readReviewsBtn.addTarget(self, action: #selector(readReviewButton), for: .touchUpInside)
            
            let abc = (bookingDetailObj.rating as NSString).doubleValue
            if abc == 0 {
                  cell.ratingView.rating = 0
            }
            else {
                cell.ratingView.rating = abc
            }
            
           cell.ratingView.rating = abc
            let gender = bookingDetailObj.gender
            if gender == "1" {
                cell.genderLbl.text = "Male"
            }
            
            else if gender == "2" {
                cell.genderLbl.text = "Female"

            }
            
            let userImg : String = "https://www.massageadvantage.com/uploads/profile/" + bookingDetailObj.therapistID  + "/thumbnail/" +  bookingDetailObj.profileImg
            
            cell.therapistProfileImageView.sd_setImage(with: URL(string: userImg), placeholderImage: #imageLiteral(resourceName: "usero"), options: SDWebImageOptions(rawValue: 0))
            
            cell.selectionStyle = .none
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerDescriptionTableViewCell", for: indexPath) as! OwnerDescriptionTableViewCell
            cell.selectionStyle = .none
            cell.knowMoreLBL.text = "Know more about " + self.bookingDetailObj.serviceName
            cell.descriptionLbl.text = self.bookingDetailObj.desc
            return cell
        }
        
    }
    
    //MARK :- Review Buttonaction
    @objc func readReviewButton(sender: UIButton){
          let massageDetailVC = storyBoardForName(name: "Main").instantiateViewController(withIdentifier: "ReadReviewsViewController")as! ReadReviewsViewController
        massageDetailVC.reviewsArray = self.reviewArray

        self.navigationController!.pushViewController(massageDetailVC, animated: true)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    //MARK:- UI Collection View Delegate And Datasource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        if self.coverImageArray.count == 0{
            return 1
        }else{
            return self.coverImageArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MAOwnerDetailsImagesCollectionViewCell", for: indexPath) as! MAOwnerDetailsImagesCollectionViewCell
        
        if self.coverImageArray.count == 0{
            cell.pageController.numberOfPages = 1
            
            cell.ownerMassageImageView.image = #imageLiteral(resourceName: "default")
        }else{
            cell.pageController.numberOfPages = self.coverImageArray.count
            cell.ownerMassageImageView.sd_setImage(with: URL(string: self.coverImageArray[indexPath.row].serviceImg), placeholderImage: #imageLiteral(resourceName: "default"), options: SDWebImageOptions(rawValue: 0))
        }
       
        cell.pageController.currentPage = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let padding: CGFloat = 0
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize.init(width:collectionViewSize , height: 220)
    }
}
