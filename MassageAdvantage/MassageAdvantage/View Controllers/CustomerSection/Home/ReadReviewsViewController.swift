//
//  ReadReviewsViewController.swift
//  MassageAdvantage
//
//  Created by nile on 20/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class ReadReviewsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var reviewsArray =  [UserInfo]()

    @IBOutlet weak var reviewsImageView: UIImageView!
    
    @IBOutlet weak var readReviewTableView: UITableView!
    @IBOutlet weak var noDataFountLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if reviewsArray.count == 0 {
            reviewsImageView.isHidden = false
            noDataFountLbl.isHidden = false
        }
        else {
            reviewsImageView.isHidden = true
            noDataFountLbl.isHidden = true
        }

        // Do any additional setup after loading the view.
    }
    

    @IBAction func bckBtnAction(_ sender: UIButton) {
     navigationController?.popViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReadReviewsTableViewCell", for: indexPath) as! ReadReviewsTableViewCell
        
        cell.post = reviewsArray[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
}
