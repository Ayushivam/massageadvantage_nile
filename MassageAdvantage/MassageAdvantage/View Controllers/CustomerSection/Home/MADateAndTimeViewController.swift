//
//  MADateAndTimeViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 10/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol goToviewAppoitment : class{
    func goToAppointment()
}

class MADateAndTimeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var countinueBtn: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var avalilabilityCollectionView: UICollectionView!
    @IBOutlet weak var calanderColectionView: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    
    var obj = UserInfo()
    var rescheduleObj = UserInfo()
    var delegate : goToviewAppoitment?
    
    var checkout =  [UserInfo]()
    
    var totalDates = [UserInfo]()
    var slotArray = [UserInfo]()
    var couponDict = Dictionary<String,AnyObject>()
    var bookingDate = ""
    var headingName = ""
    var object = UserInfo()
    var isReschedule = false    
    var isBlock = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        let sixMonthLaterDate = Calendar.current.date(byAdding: .month, value: 6, to: Date())!

        var fromDate = Date()
        
        while fromDate <= sixMonthLaterDate {
            let obj = UserInfo()
            
            let formattedDate = APPDELEGATE.formattedDateFromString(dateString: "\(fromDate)", withFormat: "MM/dd/yyyy",inputFormat: "yyyy-MM-dd HH:mm:ss z")
            obj.dateString = formattedDate!
            obj.day = APPDELEGATE.formattedDateFromString(dateString: formattedDate!, withFormat: "MMM",inputFormat: "MM/dd/yyyy")!
            obj.digit  = APPDELEGATE.formattedDateFromString(dateString: formattedDate!, withFormat: "dd",inputFormat: "MM/dd/yyyy")!
            obj.weekName  = APPDELEGATE.formattedDateFromString(dateString: formattedDate!, withFormat: "E",inputFormat: "MM/dd/yyyy")!
            obj.isActive = ""
            totalDates.append(obj)
            fromDate = Calendar.current.date(byAdding: .day, value: 1, to: fromDate)!
        }

        for i in 0..<self.totalDates.count{
            
            let obj = self.totalDates[i]
            
            for j in self.obj.availableDaysArray
            {
                if obj.weekName == j.availableDay
                {
                    obj.isActive = "1"
                    self.totalDates[i] = obj
                }
            }
        }
        
        for i in 0..<self.totalDates.count{
            
            let obj = self.totalDates[i]
            for j in self.obj.notAvailabelDatesArray
            {
                if obj.dateString == j
                {
                    obj.isActive = ""
                    self.totalDates[i] = obj
                }
            }
        }
        
        let couponType : String = self.couponDict.validatedValue("coupon_type", expected: "" as AnyObject) as! String
        if isReschedule{

            self.countinueBtn.setTitle("Confirm", for: .normal)
            let remainingDays : Int =  (self.rescheduleObj.dateCount as NSString).integerValue
            for i in 0..<remainingDays{
                let obj = self.totalDates[i]
                obj.isActive = ""
                obj.isBlocked  = "1"
                self.totalDates[i] = obj
            }
            self.calanderColectionView.reloadData()

        }else{

            if couponType != "" && couponType != "4"{

                for i in 0..<7{
                
                let obj = self.totalDates[i]
                        obj.isActive = ""
                        obj.isBlocked  = "1"
                        self.totalDates[i] = obj
            }
            self.calanderColectionView.reloadData()
            }
        }
        
        for i in 0..<self.totalDates.count//i in self.totalDates
        {
            let obj = self.totalDates[i]

            if obj.isActive == "1"
                {
                    obj.isSelect = "1"
                    
                    self.totalDates[i] = obj
                    let slotDate : String = APPDELEGATE.formattedDateFromString(dateString: obj.dateString, withFormat: "yyyy-MM-dd", inputFormat: "MM/dd/yyyy")!
                    APIManager.apiForGetSlotDetail(date: slotDate, theripistDetailsId: self.obj.therapistDetailID) { (status,dict) in
                        if status{

                            let slotArr = dict.validatedValue("time_slot", expected: [] as AnyObject) as! Array<AnyObject>
                            self.dateLabel.text = APPDELEGATE.formattedDateFromString(dateString: obj.dateString, withFormat: "EEEE,dd MMMM", inputFormat: "MM/dd/yyyy")
                            
                            self.bookingDate = APPDELEGATE.formattedDateFromString(dateString: obj.dateString, withFormat: "dd-MM-yyyy", inputFormat: "MM/dd/yyyy")!
                            self.slotArray += UserInfo.getSlotArray(responseArray: slotArr as! Array<Dictionary<String, Any>>)
                            self.avalilabilityCollectionView.reloadData()
                            self.calanderColectionView.reloadData()
                            
                            if self.isReschedule{
                                
                                let remainingDays : Int =  (self.rescheduleObj.dateCount as NSString).integerValue
                                let i = IndexPath(item: remainingDays, section: 0)
                                self.calanderColectionView.scrollToItem(at: i, at: .left, animated: true)
                                
                            }else{
                                if couponType != "" && couponType != "4"{
                                    let i = IndexPath(item: 7, section: 0)
                                    self.calanderColectionView.scrollToItem(at: i, at: .left, animated: true)
                                }
                            }

                        }
            }
            break
            }
        }
        
        if isReschedule{
        let bookingOn = rescheduleObj.bookingOn
        let BookingOnformatter = DateFormatter()
        BookingOnformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let bookingOnDate = BookingOnformatter.date(from: bookingOn)
        let diffInDays : Int  = Calendar.current.dateComponents([.hour], from: Date(), to:  bookingOnDate!).hour!
        
      
        let formatted = APPDELEGATE.formattedDateFromString(dateString: "\(Date())", withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd HH:mm:ss z")
        
        let bookingDate = rescheduleObj.bookDate
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let bookDate = formatter.date(from: bookingDate)
        let todayDate = formatter.date(from: formatted!)
        
        if rescheduleObj.cancellationCharge == "0" && rescheduleObj.bookingStatus == "1" {
            
            if bookDate?.compare(todayDate!) == .orderedAscending{
                     self.countinueBtn.setTitle("Pay", for: .normal)
            }
                
            else if bookDate?.compare(todayDate!) == .orderedSame {
                    if diffInDays >= 24 {
                       self.countinueBtn.setTitle("Confirm", for: .normal)
                        
                    }
                    else {
                        
                         self.countinueBtn.setTitle("Pay", for: .normal)
                    }
                
            }
            else {
                if diffInDays >= 24{
                    self.countinueBtn.setTitle("Confirm", for: .normal)
                    
                }
                    
                else {
                    self.countinueBtn.setTitle("Pay", for: .normal)
                    
                }
            }
           
        }
        
        else{
            if rescheduleObj.cancellationCharge == "0" && rescheduleObj.bookingStatus == "2" {
                 self.countinueBtn.setTitle("Confirm", for: .normal)
            }
            else {
                 self.countinueBtn.setTitle("Pay", for: .normal)
            }
            
        }
        }
    }

    //MARK:- UI Collection View Delegate And Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 10{
            
            return totalDates.count
            
        }else{
            return slotArray.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 10{

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MACalendarCollectionViewCell", for: indexPath) as! MACalendarCollectionViewCell
            cell.calendarDelegate = self
            cell.post = self.totalDates[indexPath.row]
            return cell

        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MAAvalabilityCollectionViewCell", for: indexPath) as! MAAvalabilityCollectionViewCell
            cell.post = self.slotArray[indexPath.row]
            cell.avalabilityDelegate = self
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag != 10{

        let cellsAcross: CGFloat = 3
        var widthRemainingForCellContent = collectionView.bounds.width
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
        }
        
        let cellWidth = widthRemainingForCellContent / cellsAcross
        return CGSize(width: cellWidth, height: cellWidth-45)
        
        } else{
            return CGSize(width: 63, height: 93)
        }
    }

    //MARK:- UI Button Action

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinueBtnAction(_ sender: UIButton) {
        
        if self.countinueBtn.currentTitle == "Confirm"{
            
                    AlertController.alert(title: "", message: "Are you sure you want to reschedule  this appointment?", buttons: ["No","Yes"]) { (alert, index) in
            
                        if index == 1{
                            
                            APIManager.apiForRescheduleBooking(bookingId: self.rescheduleObj.paymentID, selectDate: self.bookingDate, selectTime: self.object.time,txnId: "") { (status, dict) in
                                
                                if status{
                                    
                                    AlertController.alert(title: "Success", message: dict.validatedValue("message", expected: "" as AnyObject) as! String, buttons: ["Ok"], tapBlock: { (alert, index) in
                                        self.delegate?.goToAppointment()
                                        self.navigationController?.popViewController(animated: true)
                                        
                                    })
                                    
                                }
                            }
                        }
                    }

        }
        else if self.countinueBtn.currentTitle == "Pay" {
            
            AlertController.alert(title: "", message: "This appointment is being scheduled within 24 hours of the appointment time. So you need to pay $25 extra to reschedule.Are you sure you want to reschedule  this appointment?", buttons: ["No","Yes"]) { (alert, index) in
                if index == 1{
                    
                    let massageDetailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "PaymentViewController")as! PaymentViewController
                    massageDetailVC.amount = self.rescheduleObj.payCancellationCharge
                    massageDetailVC.rescheduleDelegate = self
                    massageDetailVC.isFromReschedule = true
                    self.navigationController!.pushViewController(massageDetailVC, animated: true)
                }
            }
        }
        else{
            let upgradeMassageVc = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "MAUpgradeMassageViewController")as! MAUpgradeMassageViewController
            upgradeMassageVc.obj = obj
            upgradeMassageVc.couponDict = self.couponDict
            upgradeMassageVc.slot = object.time
            upgradeMassageVc.date = self.bookingDate
            upgradeMassageVc.headingName = self.headingName
            upgradeMassageVc.checkout = self.checkout
            self.navigationController!.pushViewController(upgradeMassageVc, animated: true)
        }
    }
}

extension MADateAndTimeViewController : RescheduleMassagePayment{

    func notifySuccessReschedulePayment(transactionId:String)
        
{
    APIManager.apiForRescheduleBooking(bookingId: self.rescheduleObj.paymentID, selectDate: self.bookingDate, selectTime: self.object.time,txnId: transactionId) { (status, dict) in
        if status{
            
            AlertController.alert(title: "Success", message: dict.validatedValue("message", expected: "" as AnyObject) as! String, buttons: ["Ok"], tapBlock: { (alert, index) in
                self.delegate?.goToAppointment()
                self.navigationController?.popViewController(animated: true)
                
            })
//            self.delegate?.goToAppointment()
//            self.navigationController?.popViewController(animated: true)
        }
    }

    print(transactionId)
    }

}

extension MADateAndTimeViewController : AvalabilityTypeCellDelegate,CalendarTypeCellDelegate{

    func avalibilityCellTapAction(_sender: MAAvalabilityCollectionViewCell) {
      
        guard let tappedIndexPath = avalilabilityCollectionView.indexPath(for: _sender) else { return }
         object = self.slotArray[tappedIndexPath.row]
        
       if self.isBlock
       {
        AlertController.alert(title: "Want to move to the front of the line? Priority booking is available for members only.")
       }else{
        for i in 0..<slotArray.count {
            let obj = self.slotArray[i]
            obj.isSelect = ""
            self.slotArray[i] = obj
        }
        
        let indexObj = self.slotArray[tappedIndexPath.row]
        indexObj.isSelect = "1"
        self.slotArray[tappedIndexPath.row] = indexObj
        self.countinueBtn.isHidden = false
        self.avalilabilityCollectionView.reloadData()
        }
    }

    func calendarCellTapAction(_sender: MACalendarCollectionViewCell) {

        guard let tappedIndexPath = calanderColectionView.indexPath(for: _sender) else { return }

        for i in 0..<self.totalDates.count{
            let obj = self.totalDates[i]
            obj.isSelect = ""
            self.totalDates[i] = obj
        }

        let obj = self.totalDates[tappedIndexPath.row]

        if obj.isBlocked == "1"{
            self.isBlock = true
        }else{
            self.isBlock = false
        }

        obj.isSelect = "1"
            self.totalDates[tappedIndexPath.row] = obj
            
            let slotDate : String = APPDELEGATE.formattedDateFromString(dateString: obj.dateString, withFormat: "yyyy-MM-dd", inputFormat: "MM/dd/yyyy")!
            APIManager.apiForGetSlotDetail(date: slotDate, theripistDetailsId: self.obj.therapistDetailID) { (status,dict) in
                self.slotArray.removeAll()
                self.countinueBtn.isHidden = true

                if status{

                    let slotArr = dict.validatedValue("time_slot", expected: [] as AnyObject) as! Array<AnyObject>
                    
                    self.slotArray += UserInfo.getSlotArray(responseArray: slotArr as! Array<Dictionary<String, Any>>)
                    self.dateLabel.text = APPDELEGATE.formattedDateFromString(dateString: obj.dateString, withFormat: "EEEE,dd MMMM", inputFormat: "MM/dd/yyyy")
                    self.bookingDate = APPDELEGATE.formattedDateFromString(dateString: obj.dateString, withFormat: "dd-MM-yyyy", inputFormat: "MM/dd/yyyy")!
                    self.avalilabilityCollectionView.reloadData()
                    self.calanderColectionView.reloadData()
                }
            }
       // }
    }
}
