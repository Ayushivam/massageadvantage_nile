//
//  MATabViewController.swift
//  MassageAdvantage
//
//  Created by nile on 08/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MATabViewController: UIViewController {

    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var heightConstraints: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topTitleLbl: UILabel!
    
    @IBOutlet weak var exploreImageView: UIImageView!
    @IBOutlet weak var homeImageView: UIImageView!
    @IBOutlet weak var bookingImageView: UIImageView!
    @IBOutlet weak var moreImageView: UIImageView!
    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var bookingsBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var exploreBtn: UIButton!
    
    var currentVC: UIViewController?
    
    var homeVc: MAHomeViewController?
    var exploreVc: MAPlanMassageViewController?
    var bookingVc: MABookingListViewController?
    var moreVc: MAMoreMenuViewController?

    var  baseNavController: UINavigationController?

    override func viewDidLoad() {

        super.viewDidLoad()
        configureHeight()
        initialSetUp()

        self.topTitleLbl.text = "Request a Massage"
        self.exploreImageView.image = UIImage(named: "exploreB")
        self.exploreBtn.setTitleColor(UIColor.init(red: 71/255, green: 191/255, blue: 195/255, alpha: 1.0), for: .normal)
        display(exploreVc!)
        
    }
    
    func initialSetUp() {
        self.navigationController?.navigationBar.isHidden = true
        topView.center = self.view.center
        topView.backgroundColor = UIColor.white
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 8.0
        topView.layer.shadowOffset = CGSize.zero
        topView.layer.shadowRadius = 8.0
        
        baseNavController = UINavigationController()
        baseNavController?.isNavigationBarHidden = true
        let homeStoryboard = UIStoryboard.init(name: "Home", bundle: nil)
        homeVc = homeStoryboard.instantiateViewController(withIdentifier: "MAHomeViewController") as? MAHomeViewController
        exploreVc = homeStoryboard.instantiateViewController(withIdentifier: "MAPlanMassageViewController") as? MAPlanMassageViewController
        bookingVc = homeStoryboard.instantiateViewController(withIdentifier: "MABookingListViewController") as? MABookingListViewController
        moreVc = homeStoryboard.instantiateViewController(withIdentifier: "MAMoreMenuViewController") as? MAMoreMenuViewController
//        if let _ = NSUSERDEFAULT.value(forKey: kUserId){
//            logoutBtn.isHidden = false
//        }else{
//            logoutBtn.isHidden = true
//        }
        
        NotificationCenter.default.post(name: Notification.Name("reloadHomeApi"), object: nil)
        
       
    }
    
//  public  func updateLogoutBtn()
//    {
//        if let _ = NSUSERDEFAULT.value(forKey: kUserId){
//            logoutBtn.isHidden = false
//        }else{
//            logoutBtn.isHidden = true
//        }
//    }
    
    
    func replaceCurrentVC(withVC newVC: UIViewController) {
        
        currentVC?.willMove(toParent: nil)
        currentVC?.view.removeFromSuperview()
        currentVC?.removeFromParent()
        currentVC = newVC
        addChild(currentVC!)
        containerView?.addSubview((currentVC?.view)!)
        currentVC?.view.frame = (containerView?.bounds)!
        currentVC?.didMove(toParent: self)
    }
    
    func display(_ viewController: UIViewController) {
        
        baseNavController?.viewControllers = [viewController]
        baseNavController?.isNavigationBarHidden = true
        replaceCurrentVC(withVC: baseNavController!)
    }
    
    @IBAction func commonActnButton(_ sender: UIButton) {
        
        APPDELEGATE.backgroundApiForUpdateStatus()
        switch sender.tag {
        case 10:
            initialImage()
            self.topTitleLbl.text = "Home"
            self.homeImageView.image = UIImage(named: "homeB")
            self.homeBtn.setTitleColor(UIColor.init(red: 71/255, green: 191/255, blue: 195/255, alpha: 1.0), for: .normal)
            NotificationCenter.default.post(name: Notification.Name("reloadHomeApi"), object: nil)
            display(homeVc!)
            break
            
        case 11:
            initialImage()
            self.topTitleLbl.text = "Request a Massage"
            self.exploreImageView.image = UIImage(named: "exploreB")
            self.exploreBtn.setTitleColor(UIColor.init(red: 71/255, green: 191/255, blue: 195/255, alpha: 1.0), for: .normal)
            NotificationCenter.default.post(name: Notification.Name("reloadServiceApi"), object: nil)

            display(exploreVc!)
            break
            
        case 12:
            if let _ = NSUSERDEFAULT.value(forKey: kUserId)
            {
                initialImage()
                self.topTitleLbl.text = "Bookings"
                self.bookingImageView.image = UIImage(named: "bookingB")
                self.bookingsBtn.setTitleColor(UIColor.init(red: 71/255, green: 191/255, blue: 195/255, alpha: 1.0), for: .normal)
              //  NotificationCenter.default.post(name: Notification.Name("reloadBookingApi"), object: nil)
                display(bookingVc!)

            }else {
                initialImage()
                self.topTitleLbl.text = "Bookings"
                self.bookingImageView.image = UIImage(named: "bookingB")
                self.bookingsBtn.setTitleColor(UIColor.init(red: 71/255, green: 191/255, blue: 195/255, alpha: 1.0), for: .normal)
                display(bookingVc!)
            }

            break
        case 13:
            initialImage()
            self.topTitleLbl.text = "More"
            self.moreImageView.image = UIImage(named: "menuB")
            self.moreBtn.setTitleColor(UIColor.init(red: 71/255, green: 191/255, blue: 195/255, alpha: 1.0), for: .normal)
            display(moreVc!)
            break
        default:
            break
        }
    }

    func initialImage() {
        self.homeImageView.image = UIImage(named: "homeG")
        self.exploreImageView.image = UIImage(named: "exploreG")
        self.bookingImageView.image = UIImage(named: "bookingG")
        self.moreImageView.image = UIImage(named: "menuG")
        self.homeBtn.setTitleColor(UIColor.init(red: 67/255, green: 67/255, blue: 67/255, alpha: 1.0), for: .normal)
        self.exploreBtn.setTitleColor(UIColor.init(red: 67/255, green: 67/255, blue: 67/255, alpha: 1.0), for: .normal)
        self.bookingsBtn.setTitleColor(UIColor.init(red: 67/255, green: 67/255, blue: 67/255, alpha: 1.0), for: .normal)
        self.moreBtn.setTitleColor(UIColor.init(red: 67/255, green: 67/255, blue: 67/255, alpha: 1.0), for: .normal)
    }
    
    //MARK:- Bottom View Constrainsts
    func configureHeight()
    {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                heightConstraints.constant = 60
            //  print("iPhone 5 or 5S or 5C")
            case 1334:
                heightConstraints.constant = 60
            //  print("iPhone 6/6S/7/8")
            case 1920, 2208:
                heightConstraints.constant = 60
            //   print("iPhone 6+/6S+/7+/8+")
            case 2436:
                heightConstraints.constant = 80
            //   print("iPhone X, Xs")
            case 2688:
                heightConstraints.constant = 80
            //   print("iPhone Xs Max")
            case 1792:
                heightConstraints.constant = 80
            //  print("iPhone Xr")
            default:
                break
                //   print("unknown")
            }
        }
    }

    @IBAction func logoutAction(_ sender: UIButton) {

        if let _ = NSUSERDEFAULT.value(forKey: kUserId) {

            AlertController.alert(title: "Logout", message: "Are you sure, you want to logout?", buttons: ["Cancel","Confirm"]) { (alert, index) in
                
                if index == 1{
                    APIManager.apiForLogout(completion: { (status) in
                        if status{
                            let viewControllers = APPDELEGATE.navController!.viewControllers
                            
                            for i in 0..<viewControllers.count {
                                let obj = viewControllers[i]
                                if obj.isKind(of: MALoginViewController.self){
                                    APPDELEGATE.resetDefaults()
                                    APPDELEGATE.navController!.popToViewController(obj, animated: true)
                                }
                            }
                            
                        }
                    })
                    
                }
            }
        }else{
            
            let viewControllers = APPDELEGATE.navController!.viewControllers
            for i in 0..<viewControllers.count {
                let obj = viewControllers[i]
                if obj.isKind(of: MALoginViewController.self){
                    APPDELEGATE.resetDefaults()
                    APPDELEGATE.navController!.popToViewController(obj, animated: true)
                }
            }
        }
    }
}
