//
//  MAMembershipPlanViewController.swift
//  MassageAdvantage
//
//  Created by nile on 03/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAMembershipPlanViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var memberShipPlanTableView: UITableView!
    var memberShipPlanArray = [UserInfo]()

    @IBOutlet weak var noDataFoundLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()

        APIManager.apiForGetSubscriptionList {(status,subscriptionListArr) in
            if status{
                self.memberShipPlanArray.removeAll()
                self.memberShipPlanArray += UserInfo.getmemberShipPlanArray(responseArray: subscriptionListArr as! Array<Dictionary<String, Any>>)
                self.memberShipPlanTableView.reloadData()
            }
            else {
                let _ = (self.memberShipPlanArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
            }
        }
        // Do any additional setup after loading the view.
    }

    
    
    //MARK:- UIBtn Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- UI Table View Delegate And Datasource

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberShipPlanArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAMembershipPlanTableViewCell", for: indexPath)as! MAMembershipPlanTableViewCell
        cell.post = self.memberShipPlanArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
}
