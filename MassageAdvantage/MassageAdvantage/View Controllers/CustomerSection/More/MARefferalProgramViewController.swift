//
//  MARefferalProgramViewController.swift
//  MassageAdvantage
//
//  Created by nile on 24/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

enum refferalORGiftEnumType {
  case  referAfriend,giftAMassage,referAcustomer
}

class MARefferalProgramViewController: UIViewController,UITextFieldDelegate,SuccessGiftAMassagePayment {
 
    
    @IBOutlet weak var estricLabel: UILabel!
    @IBOutlet weak var noteViewConst: NSLayoutConstraint!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var topheadingLbl: UILabel!
    var refferalORGiftType : refferalORGiftEnumType?
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var emailAddressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.hideKeyboardWhenTappedAround()
        emailAddressTextField.setBottomBorder()
        nameTextField.setBottomBorder()
        shareBtn.setShadow()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    
    
    
    func initialSetUp() {
        
        if refferalORGiftType == refferalORGiftEnumType.referAfriend {
            
            self.shareBtn.setTitle("SHARE", for: .normal)
            self.topheadingLbl.text = "Refer a Friend"
            self.noteLabel.text = "Each referral will add 20 loyalty points in your account."
            self.estricLabel.text = "* Loyalty points will be added only when the referral register on massage advantage."
        }
        
        else if refferalORGiftType == refferalORGiftEnumType.giftAMassage {
            
            let amount : String = NSUSERDEFAULT.value(forKey: kGiftPrice) as! String
            self.shareBtn.setTitle("Gift", for: .normal)
            
             self.topheadingLbl.text = "Gift a Massage"
            self.noteLabel.numberOfLines = 1
            
            self.noteLabel.text = "Love It! Gift It! only @ $" + amount + "/massage."
            noteViewConst.constant = 0
        }
        
        else if refferalORGiftType == refferalORGiftEnumType.referAcustomer {
            
            self.topheadingLbl.text = "Refer a Customer"
      self.noteLabel.text = "Refer and Earn!!"
            self.estricLabel.text = "* 50% value of membership plan will be credited to your account."
            
        }
        
    }
    
    
    //MARK:- Success Payment API

    func notifySuccessGiftAMassagePayment(transactionId: String) {
        print(transactionId)
        
        APIManager.apiForGiftMassage(name: nameTextField.text!, email: emailAddressTextField.text!, txnId: transactionId) { (status, dict) in
            
            if status {
                
           let msg : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                AlertController.alert(title: msg, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
            else {
                let msg : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                AlertController.alert(title: msg)
            }
            
        }
    }
    
    //MARK:- UIBTN Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func shareBtnAction(_ sender: UIButton) {
        
        
         if refferalORGiftType == refferalORGiftEnumType.referAfriend {
        
        let validationObj = ValidationClass.verifyFieldForRefferalProg(name: self.nameTextField.text!, email: self.emailAddressTextField.text!)
        
        if validationObj.1{
            
            
            APIManager.apiForRefferalProg(name: self.nameTextField.text!, email: self.emailAddressTextField.text!) {(status,dict) in
                if status {
                    let data : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                    AlertController.alert(title: data, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
            
        }
            
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }
        
         else if refferalORGiftType == refferalORGiftEnumType.giftAMassage {
            
            let validationObj = ValidationClass.verifyFieldForRefferalProg(name: self.nameTextField.text!, email: self.emailAddressTextField.text!)
            
            if validationObj.1{
                let massageDetailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "PaymentViewController")as! PaymentViewController
                let amount : String = NSUSERDEFAULT.value(forKey: kGiftPrice) as! String
                massageDetailVC.amount = amount
                massageDetailVC.giftDelegate = self
                massageDetailVC.isFromGift = true
                self.navigationController!.pushViewController(massageDetailVC, animated: true)
            }
                
            else {
                _ = AlertController.alert(title: "", message: validationObj.0)
            }
            
        }
         else if refferalORGiftType == refferalORGiftEnumType.referAcustomer {
            
            let validationObj = ValidationClass.verifyFieldForRefferalProg(name: self.nameTextField.text!, email: self.emailAddressTextField.text!)
            
            if validationObj.1{
                APIManager.apiForRefferalProg(name: self.nameTextField.text!, email: self.emailAddressTextField.text!) {(status,dict) in
                    if status {
                        let data : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                        AlertController.alert(title: data, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
               
            }
                
            else {
                _ = AlertController.alert(title: "", message: validationObj.0)
            }
            
            
        }
        
    }
    
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 25
            {
                return false
            }
            break
        case 101:
            if str.length > 60
            {
                return false
            }
        default:
            break
        }
        
        //MARK:- Does'nt Support Emojies
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }

    
    
}

