//
//  MAStaticViewController.swift
//  MassageAdvantage
//
//  Created by nile on 15/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
import WebKit
enum methodType {
    case contactUs,aboutUs,termnCond,refundPolicy,cancellationPolicy
}

class MAStaticViewController: UIViewController,WKNavigationDelegate {
    var contentType:methodType?
    var webView = WKWebView()

    @IBOutlet weak var titleLabel: UILabel!
   
    var url = ["https://www.massageadvantage.com/contact-us","https://www.massageadvantage.com/mobile-refund-policy","https://www.massageadvantage.com/mobile-cancellation-policy"]
    
    var index = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let _  = (contentType == methodType.contactUs) ? (titleLabel.text = "CONTACT US", index = 0) : (contentType == methodType.refundPolicy) ? (titleLabel.text = "REFUND POLICY" , index = 1) :(titleLabel.text = "CANCELLATION POLICY" , index = 2 )

        // Do any additional setup after loading the view.

        webView.frame = CGRect(x: 0, y: 84, width: Window_Width, height: Window_Height-84)
        webView.navigationDelegate = self
        webView.backgroundColor = UIColor.clear
        webView.load(NSURLRequest(url: NSURL(string: url[index])! as URL) as URLRequest)
        view.addSubview(webView)
    }
    

    //    Mark:- UIButton
    @IBAction func bckBTnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Web Kit Delgate
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        if (APPDELEGATE.isReachable == false) {
            AlertController.alert(title: "Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
            return
        }
        else {
            ServiceHelper.hideAllHuds(false, type: .default)
        }
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        if (APPDELEGATE.isReachable == false) {
            AlertController.alert(title: "Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
            return
        }
        else {
            ServiceHelper.hideAllHuds(true, type: .default)
        }
        
    }
}
