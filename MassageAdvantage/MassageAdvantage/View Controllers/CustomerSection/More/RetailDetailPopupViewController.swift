//
//  RetailDetailPopupViewController.swift
//  MassageAdvantage
//
//  Created by nile on 23/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class RetailDetailPopupViewController: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    @IBOutlet weak var packLabel: UILabel!
    var name = ""
    var id = ""
    var totalAmount = ""
    var contentTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailAddressTextField.setBottomBorder()
        nameTextField.setBottomBorder()
        phoneTextField.setBottomBorder()
        nextBtn.setShadow()
        
        self.packLabel.text = name
        if id == "2"
        {
            let totalPriceFloat = (totalAmount as NSString).floatValue * 6
            totalAmount = String(format: "%.2f", totalPriceFloat)
            
            self.totalPriceLabel.text = "$" + totalAmount
            
        }
        else if id == "3"
        {
            
            let totalPriceFloat = (totalAmount as NSString).floatValue * 12
             totalAmount = String(format: "%.2f", totalPriceFloat)
            
             self.totalPriceLabel.text = "$" + totalAmount
        }
        
        else {
            self.totalPriceLabel.text = "$" + totalAmount

        }
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func crossBtnAction(_ sender: Any) {
        dismiss(animated: true, completion:nil)
    }
    
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        let validationObj = ValidationClass.verifyFieldForRetailInfo(name: self.nameTextField.text!, email: self.emailAddressTextField.text!, phone: self.phoneTextField.text!)
        
        if validationObj.1{
            let retailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "PaymentViewController")as! PaymentViewController
            retailVC.amount = totalAmount
            retailVC.retailPlanDelegate = self
            retailVC.isFromRetailPlan = true
           // self.navigationController!.pushViewController(retailVC, animated: true)
            self.present(retailVC, animated: true, completion: nil)
        }
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 25
            {
                return false
            }
            break
        case 101:
            if str.length > 60
            {
                return false
            }
            
        case 103:
            if str.length > 12
            {
                return false
            }
            
        default:
            break
        }
        
        //MARK:- Does'nt Support Emojies
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
    
    
    
    
}
extension  RetailDetailPopupViewController : RetailPlanPayment {
    func notifySuccessRetailPlanPayment(transactionId: String,responseString:String) {
        
        let userDict : Dictionary<String,String> = [
            "user_name" : self.nameTextField.text!,
            "user_email" : self.emailAddressTextField.text!,
            "user_contact" :self.phoneTextField.text!,
            "price" : self.totalAmount,
            "title" : self.contentTitle,
            "planid" : self.id,
            "txn_id" : transactionId,
            "txn_data" : responseString,
        ]
        
        APIManager.apiForSuccessPaymentRetailPlan(dict: userDict as Dictionary<String, AnyObject>) { (status,msg) in
            if status {
                AlertController.alert(title: "", message: msg, buttons: ["OK"], tapBlock: { (alert, index) in
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }
        
        print(transactionId + responseString)
    }
}
