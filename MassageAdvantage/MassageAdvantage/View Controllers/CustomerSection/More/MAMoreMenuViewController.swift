//
//  MAMoreMenuViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 07/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAMoreMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var moreTableView: UITableView!
    
    
    var menuListArray = [String]()
    var profileList = [String]()
    var isOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        view.endEditing(true)
        profileList = ["User Profile","Soap Note"]
        if let _ = NSUSERDEFAULT.value(forKey: kUserId){
            let isSubs: String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
            if isSubs == "1" {

                if let _ = NSUSERDEFAULT.value(forKey: kPlanId) {
                    let planId : String = NSUSERDEFAULT.value(forKey: kPlanId ) as! String
                    let planIDInt = (planId as NSString).integerValue
                    if planIDInt > 1 {
                        menuListArray = ["Profile","Membership Plan","Retail Plan","Payment Summary","Loyalty Points","Refer a Friend","Gift a Massage","Change Password","Terms & Conditions","Privacy Policy","Contact US","Refund Policy","Cancellation Policy","Log Out"]
                    }
                    else {
                        menuListArray = ["Profile","Membership Plan","Retail Plan","Payment Summary","Loyalty Points","Refer a Friend","Change Password","Terms & Conditions","Privacy Policy","Contact US","Refund Policy","Cancellation Policy","Log Out"]
                    }
                }
            }
            else if isSubs == "0" {
                menuListArray = ["Profile","Membership Plan","Retail Plan","Payment Summary","Loyalty Points","Refer a Friend","Change Password","Terms & Conditions","Privacy Policy","Contact US","Refund Policy","Cancellation Policy","Log Out"]
            }
        }
        else {
            menuListArray = ["Profile","Membership Plan","Retail Plan","Payment Summary","Loyalty Points","Refer a Friend","Change Password","Terms & Conditions","Privacy Policy","Contact US","Refund Policy","Cancellation Policy","LogIn"]
        }
        moreTableView.reloadData()
    }

    //MARK:- UI Table View Delegate And Datasource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 47
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 && isOpen{
            return 2
        }else{
            return 0
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return menuListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 65
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAMoreMenuTableViewCell") as! MAMoreMenuTableViewCell

        cell.selectionStyle = .none
        cell.nameLabel.text = self.menuListArray[section]
        cell.menuCellButton.tag = section+100
        cell.menuCellButton.addTarget(self, action: #selector(moreCellAction), for: .touchUpInside)
        if section == 0{
            
            let _ = (self.isOpen) ? (cell.cornerImg.image = #imageLiteral(resourceName: "downArrow")) : (cell.cornerImg.image = #imageLiteral(resourceName: "rightArrow"))
        }else{
            cell.cornerImg.image = #imageLiteral(resourceName: "rightArrow")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "MAMoreMenuSubTableViewCell", for: indexPath) as! MAMoreMenuSubTableViewCell
        cell.selectionStyle = .none
        cell.nameLbl.text = self.profileList[indexPath.row]
        cell.menuButton.tag = indexPath.row+200
        cell.menuButton.addTarget(self, action: #selector(moreSubCellAction), for: .touchUpInside)
        return cell
    }
    
    @objc func moreSubCellAction(sender: UIButton){
        
        let index = sender.tag-200
        
        
        if index == 0{
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newVC = storyBoard.instantiateViewController(withIdentifier: "MAProfileViewController") as! MAProfileViewController
            APPDELEGATE.navController?.pushViewController(newVC, animated: true)
            
        }else{
            
            let soapId : String = NSUSERDEFAULT.value(forKey: kSoapNoteID) as! String
            if let url = URL(string: "https://www.massageadvantage.com/user/mobile_profile" + "/" + soapId) {
                UIApplication.shared.open(url)
            }
        }
    }

    @objc func moreCellAction(sender: UIButton){

        let index = sender.tag-100
        if self.menuListArray.count == 14
        {
            if index == 0{
                if let _ = NSUSERDEFAULT.value(forKey: kUserId)
                {
                    self.isOpen = !self.isOpen
                    UIView.animate(withDuration: 1) {
                        self.moreTableView.reloadData()
                    }
                }else {
                    skipAlert()
                }
            }
            else if index == 1{
               
                let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAMembershipPlanViewController") as! MAMembershipPlanViewController
                indexSelection(controllerNme: newVC)
            }
            else if index == 2{
                
                let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RetailPlansViewController") as! RetailPlansViewController
                indexSelection(controllerNme: newVC)
            }
            else if index == 3{
                let newVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MAPaymentSummaryViewController") as! MAPaymentSummaryViewController
                indexSelection(controllerNme: newVC)
            }else if index == 4 {
                
                let newVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MALoyaltyPointsViewController") as! MALoyaltyPointsViewController
                indexSelection(controllerNme: newVC)
            }else if index == 5{
                
                let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MARefferalProgramViewController") as! MARefferalProgramViewController
                newVC.refferalORGiftType = refferalORGiftEnumType.referAfriend
                indexSelection(controllerNme: newVC)
            }else if index == 6{
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let staticVC = storyBoard.instantiateViewController(withIdentifier: "MARefferalProgramViewController") as! MARefferalProgramViewController
                staticVC.refferalORGiftType = refferalORGiftEnumType.giftAMassage
                APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
            }
            else if index == 7{
                let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAChangePasswordViewController") as! MAChangePasswordViewController
                indexSelection(controllerNme: newVC)
            }
                
            else if index == 8 {
                UIApplication.shared.openURL(NSURL(string: "https://www.massageadvantage.com/terms-and-service")! as URL)

            }else if index == 9 {

                UIApplication.shared.openURL(NSURL(string: "https://www.massageadvantage.com/privacy-policy")! as URL)
            }
                
            else if index == 10 || index == 11 || index == 12{
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let staticVC = storyBoard.instantiateViewController(withIdentifier: "MAStaticViewController") as! MAStaticViewController
                staticVC.contentType = methodType.contactUs
                
                let _ = (index == 10) ? (staticVC.contentType = methodType.contactUs) : (index == 11) ? (staticVC.contentType = methodType.refundPolicy) : (staticVC.contentType = methodType.cancellationPolicy)
                APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
            }
            else if index == 13{
                if let _ = NSUSERDEFAULT.value(forKey: kUserId) {
                    AlertController.alert(title: "Logout", message: "Are you sure, you want to logout?", buttons: ["Cancel","Confirm"]) { (alert, index) in
                        if index == 1{
                            APIManager.apiForLogout(completion: { (status) in
                                if status{
                                    let viewControllers = APPDELEGATE.navController!.viewControllers
                                    
                                    for i in 0..<viewControllers.count {
                                        let obj = viewControllers[i]
                                        if obj.isKind(of: MALoginViewController.self){
                                            APPDELEGATE.resetDefaults()
                                            APPDELEGATE.navController!.popToViewController(obj, animated: true)
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
                else {
                    self.navigationController!.viewControllers.removeAll()
                    APPDELEGATE.goToLogin()
                }
            }
        }else{
            if index == 0{
                if let _ = NSUSERDEFAULT.value(forKey: kUserId)
                {
                    self.isOpen = !self.isOpen
                    
                    UIView.animate(withDuration: 1) {
                        self.moreTableView.reloadData()
                    }
                }else {
                    skipAlert()
                }
            } else if index == 1{

                let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAMembershipPlanViewController") as! MAMembershipPlanViewController
                indexSelection(controllerNme: newVC)
            }
            else if index == 2{
                    let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RetailPlansViewController") as! RetailPlansViewController
                    APPDELEGATE.navController?.pushViewController(newVC, animated: true)
            }
            else if index == 3{
                
                let newVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MAPaymentSummaryViewController") as! MAPaymentSummaryViewController
                indexSelection(controllerNme: newVC)
            }else if index == 4 {
                let newVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MALoyaltyPointsViewController") as! MALoyaltyPointsViewController
                indexSelection(controllerNme: newVC)
            }else if index == 5{
                let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MARefferalProgramViewController") as! MARefferalProgramViewController
                newVC.refferalORGiftType = refferalORGiftEnumType.referAfriend
                indexSelection(controllerNme: newVC)
            }
            else if index == 6{
                let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAChangePasswordViewController") as! MAChangePasswordViewController
                indexSelection(controllerNme: newVC)
            }

            else if index == 7 {
               
                UIApplication.shared.openURL(NSURL(string: "https://www.massageadvantage.com/terms-and-service")! as URL)

            }else if index == 8 {
                
                UIApplication.shared.openURL(NSURL(string: "https://www.massageadvantage.com/privacy-policy")! as URL)

            }
                
            else if index == 9 || index == 10 || index == 11{
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let staticVC = storyBoard.instantiateViewController(withIdentifier: "MAStaticViewController") as! MAStaticViewController
                let _ = (index == 9) ? (staticVC.contentType = methodType.contactUs) : (index == 10) ? (staticVC.contentType = methodType.refundPolicy) : (staticVC.contentType = methodType.cancellationPolicy)
                APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
            }
            else if index == 12{
                if let _ = NSUSERDEFAULT.value(forKey: kUserId) {
                    AlertController.alert(title: "Logout", message: "Are you sure, you want to logout?", buttons: ["Cancel","Confirm"]) { (alert, index) in
                        
                        if index == 1{
                            
                            APIManager.apiForLogout(completion: { (status) in
                                if status{
                                    let viewControllers = APPDELEGATE.navController!.viewControllers
                                    
                                    for i in 0..<viewControllers.count {
                                        let obj = viewControllers[i]
                                        if obj.isKind(of: MALoginViewController.self){
                                            APPDELEGATE.resetDefaults()
                                            APPDELEGATE.navController!.popToViewController(obj, animated: true)
                                        }
                                    }
                                    
                                }
                            })
                        }
                    }
                }
                else {
                    self.navigationController!.viewControllers.removeAll()
                    APPDELEGATE.goToLogin()
                }
            }
        }
    }

    func skipAlert() {
        AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
            if index == 1{
                self.navigationController!.viewControllers.removeAll()
                APPDELEGATE.goToLogin()
            }
        }
    }

    func indexSelection(controllerNme:UIViewController){

        if let _ = NSUSERDEFAULT.value(forKey: kUserId)
        {
            APPDELEGATE.navController?.pushViewController(controllerNme, animated: true)
        }else {
            skipAlert()
        }
    }
}

class MAMoreMenuSubTableViewCell: UITableViewCell {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}




