//
//  MAPlanMassageViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 08/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class MAPlanMassageViewController: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var currentLocationBtn: UIButton!
    @IBOutlet weak var selectDistanceBtn: UIButton!
    @IBOutlet weak var selectServiceTextField: SearchTextField!
    @IBOutlet weak var selectTheripistNameField: UITextField!
    @IBOutlet weak var selectLocationField: UITextField!
    @IBOutlet weak var selectDistanceField: UITextField!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var grouponButton: UIButton!
    
    var getServiceListArray = [UserInfo]()
    var selectDistanceArray = ["Up to 10 miles","Up to 20 miles","Up to 30 miles","Up to 40 miles","Up to 50 miles"]
    var selectDistanceCountArray = ["0-10","10-20","20-30","30-40","40-50"]
    var distanceString = ""
    var couponType = "2"
    var serviceId = ""
    var locationManager:CLLocationManager!

    var lat = Double()
    var long = Double()
    var latString = ""
    var longString = ""
    
    var address = ""
    var isFromTap = false

    override func viewDidLoad() {
        super.viewDidLoad()
       initialSetUp()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadServiceApi(notification :)), name: NSNotification.Name("reloadServiceApi"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp() {
        
        selectServiceTextField.setBottomBorder()
        selectTheripistNameField.setBottomBorder()
        selectLocationField.setBottomBorder()
        selectDistanceField.setBottomBorder()
        findButton.setShadow()
        selectDistanceBtn.setShadow()
        selectLocationField.isUserInteractionEnabled = true
        let adressSelector : Selector = Selector("addressTapped")
        let tapGesture = UITapGestureRecognizer(target: self, action: adressSelector)
        tapGesture.numberOfTapsRequired = 1
        selectLocationField.addGestureRecognizer(tapGesture)
        
//        APIManager.apiForGetServiceList{ (status,seviceListArr) in
//            if status{
//                self.getServiceListArray += UserInfo.getServiceListArray(responseArray: seviceListArr as! Array<Dictionary<String, Any>>)
//                let arr =   self.getServiceListArray.map { $0.serviceName  }
//                self.configureSimpleSearchTextField(arr: arr)
//            }
//        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        view.endEditing(true)

    }
    
    override func viewWillAppear(_ animated: Bool) {

        NSUSERDEFAULT.removeObject(forKey: kCouponCode)
        
    }
    
    //MARK:- UI Button Action
    @IBAction func findButtonAction(_ sender: UIButton) {

            let massageTypeVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "MAMassageTypesViewController")as! MAMassageTypesViewController
             massageTypeVC.serviceId = self.serviceId
             massageTypeVC.therapistName = self.selectTheripistNameField.text!
             massageTypeVC.latitude = self.latString
             massageTypeVC.longitude = self.longString
        if self.selectServiceTextField.text == "" {
             massageTypeVC.massageName = "Massage List"
        }
        else {
             massageTypeVC.massageName = self.selectServiceTextField.text!
        }
        if selectDistanceBtn.currentTitle == "Select Distance" {
             massageTypeVC.distance = ""
        }
        else {
             massageTypeVC.distance = distanceString
        }
             massageTypeVC.couponType = self.couponType
            APPDELEGATE.navController?.pushViewController(massageTypeVC, animated: true)

         }

    @IBAction func grouponServiceButtonAction(_ sender: UIButton) {

        sender.isSelected = !sender.isSelected
        
        if grouponButton.isSelected {
            self.couponType = "1"
        }
        else {
            self.couponType = "2"
        }
    }

    @IBAction func selectDistanceBtnActn(_ sender: UIButton) {
        
           commonOptionPicker(selectArray: selectDistanceArray, button: selectDistanceBtn)
    }

    @objc func reloadServiceApi(notification: Notification) {

        self.resetAllField()
        
    }
    
    //MARK: - Auto Complete PLace API Functions
    @objc func addressTapped(){

        if (APPDELEGATE.isReachable == false) {
            AlertController.alert(title: "Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
            return
        }

        isFromTap = true
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self as! GMSAutocompleteViewControllerDelegate
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //Reset All Field
    func resetAllField()
    {
        
        self.selectDistanceField.text = ""
        self.selectServiceTextField.text = ""
        self.selectLocationField.text = ""
        self.grouponButton.isSelected = false
        self.couponType = "2"
        self.lat  = 0
        self.long = 0
        self.latString = ""
        self.longString = ""
        self.selectTheripistNameField.text = ""
        selectDistanceBtn.setTitle("Select Distance", for: .normal)
        self.distanceString = ""
        
    }
    
    //MARK:- UI Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
            if str.length > 100
            {
                return false
            }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 100:
            selectServiceTextField.startVisibleWithoutInteraction = true
            break
        default:
            break
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 100:

            let arr = self.getServiceListArray.map { $0.serviceName  }
            if arr.contains(obj: textField.text)
            {
                let index = arr.firstIndex(of: textField.text!)
                self.serviceId = self.getServiceListArray[index!].serviceId
            }else{
                textField.text = ""
                self.serviceId = ""
            }
       break
        default:
            break
        }
    }

    //  MARK:- SEARCHTEXT FILED
    fileprivate func configureSimpleSearchTextField(arr:Array<Any>) {
        // Set data source
        
        selectServiceTextField.filterStrings(arr as! [String])
        selectServiceTextField.theme.bgColor = UIColor.white
        selectServiceTextField.theme.font = UIFont.systemFont(ofSize: 14)
        selectServiceTextField.theme.fontColor = UIColor.black
        selectServiceTextField.maxResultsListHeight = 280
        selectServiceTextField.theme.borderWidth = 1.0
        selectServiceTextField.theme.borderColor = UIColor.lightGray.withAlphaComponent(0.5)
    }
    
    //  MARK:- UI Button Action
    
    @IBAction func currentLocationAction(_ sender: UIButton) {
      
        if (APPDELEGATE.isReachable == false) {
            AlertController.alert(title: "Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
            return
        }
        
        determineMyCurrentLocation()
        
    }

//    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
//
//        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
//
//            button.setTitle(string, for: .normal)
//        }
//    }
//
    
    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
        if selectArray.count == 0
        {
            
        }else
        {
            RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
                
                if button == self.selectDistanceBtn{
                    button.setTitle(string, for: .normal)
                    let id = self.selectDistanceCountArray[i]
                    self.distanceString = id
                }
                else {
                    button.setTitle(string, for: .normal)
                }
            }
        }
    }
    
    //MARK:- Get Current Location
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
       // locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
        self.locationManager.stopUpdatingLocation()
        let userLocation:CLLocation = locations[0] as CLLocation
        
        ServiceHelper.getAddressString(lat:userLocation.coordinate.latitude , long:userLocation.coordinate.longitude ) { (addressStr) in
        
            self.selectLocationField.text = addressStr
            self.lat  = userLocation.coordinate.latitude
            self.long = userLocation.coordinate.longitude
            self.latString = "\(userLocation.coordinate.latitude)"
            self.longString = "\(userLocation.coordinate.longitude)"
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        
        self.lat  = 0
        self.long = 0
        self.latString = ""
        self.longString = ""
        
        
        AlertController.alert(title: "Required Location", message: "Please go to Settings and turn on the permissions", buttons: ["Cancel","Ok"]) { (alert, index) in
            
            if index == 1{
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    self.locationManager.stopUpdatingLocation()
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in }
                        
                    )}
            }
        }
    }
}

extension MAPlanMassageViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
            view.endEditing(true)
            dismiss(animated: true, completion: nil)
        
            // It Is double value of lat long
        
            self.lat = place.coordinate.latitude
            self.latString = String(self.lat)
        
            // It Is String value of lat long which can be pass in next controller(Keep in mind and dont call me)
        
            self.long = place.coordinate.longitude
            self.longString = String(self.long)

        self.selectLocationField.text = place.formattedAddress
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // self.viewContainer.isHidden = true

        //print("Error: ", error.localizedDescription)
    }

    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
