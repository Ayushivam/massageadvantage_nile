//
//  MAMembersViewController.swift
//  MassageAdvantage
//
//  Created by nile on 16/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAMembersViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,CreateDelegate {
    
    
  

    @IBOutlet weak var clickAddBtnLbl: UILabel!
    @IBOutlet weak var membersListTableViewOutlet: UITableView!
    @IBOutlet weak var plusBtn: UIButton!
    var getMembersListArray = [UserInfo]()
    override func viewDidLoad() {
        super.viewDidLoad()
        APIManager.apiForGetMembersList{(status,memberListArr)in
            
            if status{
                self.getMembersListArray.removeAll()
                self.getMembersListArray += UserInfo.getMembersListArray(responseArray: memberListArr as! Array<Dictionary<String, Any>>)
                let _ = (self.getMembersListArray.count == 0) ? (self.clickAddBtnLbl.isHidden = false) : (self.clickAddBtnLbl.isHidden = true)
                self.membersListTableViewOutlet.reloadData()
                
            }
        }
        // Do any additional setup after loading the view.
    }
    
    //MARK:- UIBUTTON
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

    @IBAction func plusBtnAction(_ sender: UIButton) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAAddMemberViewController") as! MAAddMemberViewController
        popOverVC.delegate = self
        popOverVC.providesPresentationContextTransitionStyle = true
        popOverVC.definesPresentationContext = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
        self.present(popOverVC, animated: true, completion: nil)
    }
    
    //MARK :- Delegate Method
    
    func passData() {
        APIManager.apiForGetMembersList{(status,memberListArr)in
            
            if status{
                self.getMembersListArray.removeAll()
                self.getMembersListArray += UserInfo.getMembersListArray(responseArray: memberListArr as! Array<Dictionary<String, Any>>)
                let _ = (self.getMembersListArray.count == 0) ? (self.clickAddBtnLbl.isHidden = false) : (self.clickAddBtnLbl.isHidden = true)
                self.membersListTableViewOutlet.reloadData()
                
            }
        }
    }
    
    //MARK:- UITableView delegate/Datatsource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getMembersListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAMembersListTableViewCell", for: indexPath)as! MAMembersListTableViewCell
         cell.post = getMembersListArray[indexPath.row]
        return cell
    }
    
    
}
