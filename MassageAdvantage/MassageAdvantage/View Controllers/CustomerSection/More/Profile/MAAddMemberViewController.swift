//
//  MAAddMemberViewController.swift
//  MassageAdvantage
//
//  Created by nile on 28/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

protocol CreateDelegate : class {
    
    func passData()
    
}

class MAAddMemberViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    var delegate : CreateDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        emailAddressTextField.setBottomBorder()
        nameTextField.setBottomBorder()
        addBtn.setShadow()
        // Do any additional setup after loading the view.
    }

    @IBAction func crossBtnAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func addBtnAction(_ sender: Any) {
        
        let validationObj = ValidationClass.verifyFieldForRefferalProg(name: self.nameTextField.text!, email: self.emailAddressTextField.text!)
        
        if validationObj.1{
            APIManager.apiForAddMember(name: self.nameTextField.text!, email: self.emailAddressTextField.text!) {(status,dict) in
                if status {
                    let data : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                    AlertController.alert(title: data, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                          self.delegate?.passData()
                         self.dismiss(animated: true, completion: nil)
                    })
                }
                else {
                    let data : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                    AlertController.alert(title: data, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                        self.delegate?.passData()
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            }
        }
        else {
            _ = AlertController.alert(title: "", message: validationObj.0)
        }
    }

    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 25
            {
                return false
            }
            break
        case 101:
            if str.length > 60
            {
                return false
            }
        default:
            break
        }
        
        //MARK:- Does'nt Support Emojies
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }

    
    
    
    
    
}
