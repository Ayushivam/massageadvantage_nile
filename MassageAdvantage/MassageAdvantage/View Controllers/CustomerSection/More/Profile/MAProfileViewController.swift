//
//  MAProfileViewController.swift
//  MassageAdvantage
//
//  Created by nile on 16/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MAProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var picker = UIImagePickerController()

    @IBOutlet weak var addMembersBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameView: UIView!

    @IBOutlet weak var userImageBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    var textFieldPlaceHoldersArray = ["First Name","Last Name","Email Address","Phone","Location","Special Requirement"]
    let accountInfoObj = UserInfo()
    var isEdit = false
    var imageTakenProcess = false
    var userImg = ""
    var profileThumbnail = ""

    @IBOutlet weak var profileTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         initialSetUp()
        
        if let _ = NSUSERDEFAULT.value(forKey: kIsSubs) {
            let isSubscriptionValue : String = NSUSERDEFAULT.value(forKey: kIsSubs) as! String
            if isSubscriptionValue == "1" {
                addMembersBtn.isHidden = false
            }
            else if isSubscriptionValue == "0" {
                addMembersBtn.isHidden = true
            }
        }
        
        if let _ = NSUSERDEFAULT.value(forKey: kAddMember) {
            let addmember : String = NSUSERDEFAULT.value(forKey: kAddMember) as! String
            if addmember > "0" {
                addMembersBtn.isHidden = false
            }
            else {
                addMembersBtn.isHidden = true
            }
        }
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp() {
        
        self.navigationController?.navigationBar.isHidden = true
        topView.center = self.view.center
        topView.backgroundColor = UIColor.white
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 5.0
        topView.layer.shadowOffset = CGSize.zero
        topView.layer.shadowRadius = 5.0
        saveBtn.isHidden = true
        userImageBtn.isUserInteractionEnabled = false
        APIManager.apiForGetProfile(userId: NSUSERDEFAULT.value(forKey: kUserId) as! String){ (status,dict) in
            if status {
                self.isEdit = false
                self.accountInfoObj.firstName = dict["first_name"] as! String
                self.accountInfoObj.lastName = dict["last_name"] as! String
                self.accountInfoObj.email = dict["email"] as! String
                self.accountInfoObj.phoneNumber = dict["phone"] as! String
                  self.accountInfoObj.address = dict["address"] as! String
                self.accountInfoObj.desc = dict["description"] as! String
                self.accountInfoObj.userLat = dict["latitude"] as! String
                self.accountInfoObj.userLong = dict["longitude"] as! String
                self.accountInfoObj.profileImg = dict["profile_image"] as! String
                self.accountInfoObj.profileThumb = dict["profile_thumbnail"] as! String
                self.profileThumbnail = self.accountInfoObj.profileThumb
                self.userNameLbl.text = self.accountInfoObj.firstName + " " + self.accountInfoObj.lastName
                self.userImg = self.accountInfoObj.profileImg
                self.userImageView.sd_setImage(with: URL(string: self.userImg), placeholderImage: UIImage(named:"usero"), options: SDWebImageOptions(rawValue: 0))
                 self.profileTableView.reloadData()
            }
    }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.profileTableView.reloadData()
    }
    
    //MARK:- UIBUTTON
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editProfileBtnActn(_ sender: UIButton) {
   
        self.isEdit = true
        self.profileTableView.reloadData{
            let field : UITextField = self.view.viewWithTag(100) as! UITextField
            field.becomeFirstResponder()
        }
        editBtn.isHidden = true
        saveBtn.isHidden = false
        userImageBtn.isUserInteractionEnabled = true
    }

    @IBAction func saveBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        if accountInfoObj.firstName ==  "" {
            AlertController.alert(title: "Please enter your first name.")
        }
        else if accountInfoObj.lastName ==  "" {
            AlertController.alert(title: "Please enter your last name.")
        }
        else if accountInfoObj.phoneNumber == "" {
            AlertController.alert(title: "Please enter your Phone Number.")
        }
        else if accountInfoObj.address == "" {
            AlertController.alert(title: "Please enter your Address.")
        }
        else {
             var userDict : Dictionary = Dictionary<String,String>()
            
            userDict["user_id"] = (NSUSERDEFAULT.value(forKey: kUserId) as AnyObject as! String)
            userDict["first_name"] = self.accountInfoObj.firstName
            userDict["last_name"] = self.accountInfoObj.lastName
            userDict["phone"] = self.accountInfoObj.phoneNumber
            userDict["description"] = self.accountInfoObj.desc
            userDict["address"] = self.accountInfoObj.address
            userDict["latitude"] = "46"
            userDict["longitude"] = "236"
            userDict["old_image"] = self.profileThumbnail
            print(userDict)
            var imageData = Data()
            var isImage = false
            if self.userImageView.image != nil{
                imageData = self.userImageView.image!.pngData()!
                isImage = true
            }
            else{
                isImage = false
            }

            APIManager.apiForUpdateProfile(dict:userDict as Dictionary<String,AnyObject>,data:imageData ,haveImage:isImage) { (status) in
                if status{
                    AlertController.alert(title: "Profile updated successfully.", message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                       self.navigationController?.popViewController(animated: true)
                    })
                }
            }
    }
    }

    @IBAction func userImageBtnAction(_ sender: UIButton) {

        imageTapped()

    }

    @IBAction func membersBtnActn(_ sender: UIButton) {
        
        self.isEdit = false
        editBtn.isHidden = false
        saveBtn.isHidden = true
        userImageBtn.isUserInteractionEnabled = false

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyBoard.instantiateViewController(withIdentifier: "MAMembersViewController") as! MAMembersViewController
        APPDELEGATE.navController?.pushViewController(newVC, animated: true)
        
    }
    
    //MARK:- Image Picker Functions
    func imageTapped(){
        
        self.view.endEditing(true)
        let alert = UIAlertController()
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ (UIAlertAction)in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera;
                self.picker.allowsEditing = false
                self.imageTakenProcess = true
                self.present(self.picker, animated: true, completion: nil)
                
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Choose from gallery", style: .default , handler:{ (UIAlertAction)in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.imageTakenProcess = true
                self.present(imagePicker, animated: true, completion: {
                })
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
        })
    }
    
    //:-MARK Camera delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            picker.dismiss(animated: true, completion: {() -> Void in
                self.userImageView.image = image.fixOrientationAndResize(width: 500,height:500)
               self.imageTakenProcess = false
            })
        }
    }
    
    //MARK:- UIUITableViewDataSource,UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textFieldPlaceHoldersArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAProfileTableViewCell")as! MAProfileTableViewCell
        self.hideKeyboardWhenTappedAround()
        cell.commonTextField.placeholder = textFieldPlaceHoldersArray[indexPath.row]
        cell.commonTextField.tag = indexPath.row+100
        let _ = (indexPath.row == 0) ? (cell.commonTextField.text = accountInfoObj.firstName) : (indexPath.row == 1) ? (cell.commonTextField.text = accountInfoObj.lastName) : (indexPath.row == 2) ? (cell.commonTextField.text = accountInfoObj.email) :  (indexPath.row == 3) ? (cell.commonTextField.text = accountInfoObj.phoneNumber) :  (indexPath.row == 4) ? (cell.commonTextField.text = accountInfoObj.address) : (cell.commonTextField.text = accountInfoObj.desc)
        
        let _ = (indexPath.row == 0) ? (cell.dataLbl.text = accountInfoObj.firstName) : (indexPath.row == 1) ? (cell.dataLbl.text = accountInfoObj.lastName) : (indexPath.row == 2) ? (cell.dataLbl.text = accountInfoObj.email) : (indexPath.row == 3) ? (cell.dataLbl.text = accountInfoObj.phoneNumber) : (indexPath.row == 4) ? (cell.dataLbl.text = accountInfoObj.address)  : (cell.dataLbl.text = accountInfoObj.desc)

        if !isEdit {

            cell.commonTextField.isHidden = true
            cell.dataLbl.isHidden = false

        }else{

            if indexPath.row == 2{
                
                cell.commonTextField.isHidden = true
                cell.dataLbl.isHidden = false
            }else {
                cell.commonTextField.isHidden = false
                cell.dataLbl.isHidden = true
            }
        }

        switch indexPath.row {
        case 0:
            cell.commonTextField.returnKeyType = .next
            cell.commonTextField.autocapitalizationType = .words
            cell.commonTextField.keyboardType = .alphabet

        case 1:
            cell.commonTextField.returnKeyType = .next
            cell.commonTextField.autocapitalizationType = .words
            cell.commonTextField.keyboardType = .alphabet

        case 2:
            cell.commonTextField.returnKeyType = .next
            cell.commonTextField.keyboardType = .emailAddress

        case 3:
            cell.commonTextField.returnKeyType = .next
            cell.commonTextField.keyboardType = .phonePad

        case 4:
            cell.commonTextField.returnKeyType = .next
            cell.commonTextField.autocapitalizationType = .words
            cell.commonTextField.keyboardType = .alphabet

        case 5:
            cell.commonTextField.returnKeyType = .done
            cell.commonTextField.autocapitalizationType = .words
            cell.commonTextField.keyboardType = .alphabet
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
        
    }
    
    //MARK:- UIText Field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100,101:
            if str.length > 25
            {
                return false
            }
            break
        case 102:
            if str.length > 60
            {
                return false
            }
            
        case 103:
            if str.length > 15
            {
                return false
            }
        case 104:
            if str.length > 100
            {
                return false
            }
            
        case 105:
            if str.length > 100
            {
                return false
            }
        default:
            break
        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField.tag {
        case 100:
            
            accountInfoObj.firstName = textField.text!
            break
            
        case 101:
            
            accountInfoObj.lastName = textField.text!

            break
            
        case 102:
            accountInfoObj.email = textField.text!
            break
        case 103:
             accountInfoObj.phoneNumber = textField.text!
            
           break
        case 104:
         accountInfoObj.address = textField.text!
            break
            
        case 105:
            
             accountInfoObj.desc = textField.text!
            break
        default:
            break
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
}
