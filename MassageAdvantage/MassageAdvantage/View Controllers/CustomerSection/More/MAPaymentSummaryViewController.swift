//
//  MAPaymentSummaryViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 13/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MAPaymentSummaryViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var paymentSummaryTableView: UITableView!

    @IBOutlet weak var noDataFoundLbl: UILabel!
    var paymentSummaryArray = [UserInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()

                APIManager.apiForGetPaymentSummaryList {(status,paymentSummArr) in
                    if status{
                        self.paymentSummaryArray.removeAll()
                        self.paymentSummaryArray += UserInfo.getPaymentSummArray(responseArray: paymentSummArr as! Array<Dictionary<String, Any>>)
                        self.paymentSummaryTableView.reloadData()
                    }
                    
                    
                    else {
                        let _ = (self.paymentSummaryArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)

                    }
                }
        
        

        // Do any additional setup after loading the view.
    }
    //MARK:- UIBtn Action

    @IBAction func backButtonAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }


}

extension  MAPaymentSummaryViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 196
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return paymentSummaryArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAPaymentSummaryTableViewCell") as! MAPaymentSummaryTableViewCell
        cell.selectionStyle = .none
        cell.post = self.paymentSummaryArray[indexPath.row]
        return cell
    }
}
