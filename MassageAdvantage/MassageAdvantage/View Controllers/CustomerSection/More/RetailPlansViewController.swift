//
//  RetailPlansViewController.swift
//  MassageAdvantage
//
//  Created by nile on 23/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class RetailPlansViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
   
    @IBOutlet weak var retailPlansTableView: UITableView!
    var retailPlanArray = [UserInfo]()

    var isFromCoupon = false
    override func viewDidLoad() {
        super.viewDidLoad()
        APIManager.apiForRetailPlan {(status,retailPlanArr) in
            if status{
                   self.retailPlanArray.removeAll()
                self.retailPlanArray += UserInfo.retailPlanArray(responseArray: retailPlanArr as! Array<Dictionary<String, Any>>)
                self.retailPlansTableView.reloadData()
            }
            
        }

        // Do any additional setup after loading the view.
    }
    

    //MARK:- UiButton Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
       if  isFromCoupon {
          dismiss(animated: true, completion: nil)
    }
        
       else {
        navigationController?.popViewController(animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 180
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return retailPlanArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "retailPlanTableViewCell", for: indexPath)as! retailPlanTableViewCell
        let obj = retailPlanArray[indexPath.row]
        cell.packsLabel.text = obj.name
        cell.borderColorView.backgroundColor = UIColor.hexStringToUIColor(hex: obj.colorCode,alpha :1.0)
        cell.mainView.backgroundColor = UIColor.hexStringToUIColor(hex: obj.colorCode,alpha: 0.1)
        cell.massagePriceLabel.text =  obj.price
        cell.contentLbl.text = obj.content
        cell.commonBtn.tag = indexPath.row+1000
        cell.commonBtn.addTarget(self, action: #selector(buyBtnAction), for: .touchUpInside)
        return cell
    }
    
    @objc func buyBtnAction(sender: UIButton){
        let obj = retailPlanArray[sender.tag-1000]
        let price = obj.payAmount
        let packName = obj.name
        let content = obj.content
        let id = obj.id
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RetailDetailPopupViewController") as! RetailDetailPopupViewController
        
        popOverVC.name = packName
        popOverVC.totalAmount = price
        popOverVC.id = id
        popOverVC.contentTitle = content
        popOverVC.providesPresentationContextTransitionStyle = true
        popOverVC.definesPresentationContext = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        popOverVC.view.backgroundColor = UIColor.init(white : 0.50, alpha: 0.75)
        self.present(popOverVC, animated: true, completion: nil)
        
    }
}
