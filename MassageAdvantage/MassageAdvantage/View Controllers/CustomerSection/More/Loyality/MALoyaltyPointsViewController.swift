//
//  MALoyaltyPointsViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 15/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class MALoyaltyPointsViewController: UIViewController {

    @IBOutlet weak var bagroundView: UIView!
    @IBOutlet weak var noEarnDataLbl: UILabel!
    @IBOutlet weak var loyaltiImageView: UIImageView!
    @IBOutlet weak var earnRewardLabel: UILabel!
    @IBOutlet weak var redeemButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    var loyaltyPointsObj =  UserInfo()
    var productArray = [UserInfo]()
    var shippingCharge = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        
        APIManager.apiForLoyaltyPoints { (status,dict,productsArray) in
            if status {
                let loyaltyPoint : String = dict.validatedValue("loyalty_point", expected: "" as AnyObject) as! String
                
                self.productArray += UserInfo.productsArray(responseArray: productsArray as! Array<Dictionary<String, Any>>)
                self.shippingCharge  = dict.validatedValue("shiping_charge", expected: "" as AnyObject) as! String


                if loyaltyPoint == "" {
                                    self.earnRewardLabel.text = "You have 0 loyalty points."
                                    self.redeemButton.isHidden = false
                                    self.loyaltiImageView.isHidden = false
                                    self.noEarnDataLbl.isHidden = true
                                    self.bagroundView.isHidden = false
                                    self.earnRewardLabel.isHidden = false
                                    }
                                    else {
                                    self.earnRewardLabel.text = "You have " + loyaltyPoint + " loyalty points."
                                        self.redeemButton.isHidden = false
                                        self.loyaltiImageView.isHidden = false
                                        self.noEarnDataLbl.isHidden = true
                                        self.bagroundView.isHidden = false
                                        self.earnRewardLabel.isHidden = false
                                    }
            }
        }
    }
    
    //MARK:- UIBtn Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func redeemButtonAction(_ sender: UIButton) {
        let redeemVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "ReddemLoyaltyProductsViewController") as! ReddemLoyaltyProductsViewController
        redeemVC.products = productArray
        redeemVC.shippingCharge = shippingCharge
        APPDELEGATE.navController?.pushViewController(redeemVC, animated: true)
    }
}
