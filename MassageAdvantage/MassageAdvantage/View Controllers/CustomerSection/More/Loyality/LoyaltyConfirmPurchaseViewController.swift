//
//  LoyaltyConfirmPurchaseViewController.swift
//  MassageAdvantage
//
//  Created by nile on 11/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit


class LoyaltyConfirmPurchaseViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    @IBOutlet weak var orderSummaryTableView: UITableView!
    var subTotal = ""
    var itemID = ""
    var dollarPrice = ""
    var shippingPrice = ""
    var productDetailArr = [UserInfo]()

    @IBOutlet weak var leftOverCreditLbl: UILabel!
    @IBOutlet weak var totalCreditLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var shippingChrgeLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let totalFloat = (subTotal as NSString).floatValue * 20
        let total = String(format: "%.0f", totalFloat)
        
        let shippingChargeFloat = (shippingPrice as NSString).floatValue * 20
        let shippingCharge = String(format: "%.0f", shippingChargeFloat)
        
        let subTotalFloat = totalFloat -  shippingChargeFloat
        let subTotalPrice = String(format: "%.0f", subTotalFloat)
        
        let dollarPriceFloat = (dollarPrice as NSString).floatValue * 20
        let dollarPrice = String(format: "%.0f", dollarPriceFloat)
        
        let leftOverFloat = dollarPriceFloat - totalFloat
        let leftOverPrice = String(format: "%.0f", leftOverFloat)

        
        self.subTotalLbl.text =  "\(subTotalPrice)" + " Points"
        self.shippingChrgeLbl.text = "\(shippingCharge)" + " Points"
        self.totalLbl.text = "\(total)" + " Points"
        self.totalCreditLbl.text = "\(dollarPrice)" + " Points"
        self.leftOverCreditLbl.text =  "\(leftOverPrice)" + " Points"
        
        
      orderSummaryTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- UIButton action
    @IBAction func bckBTnAction(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func confirmOrderAction(_ sender: UIButton) {
        APIManager.apiForConfirmPurchase(item: self.itemID, price: self.subTotal) { (status,msg) in
            if status {
                AlertController.alert(title: "", message: msg, buttons: ["OK"], tapBlock: { (alert, index) in
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true);
                })
                
               
            }
            else {
                AlertController.alert(message: msg)
            }
        }
        
    }
    
    
    //MARK:- TableView
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDetailArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "loyaltyConfirmPurchaseTableViewCell", for: indexPath)as! loyaltyConfirmPurchaseTableViewCell
        
          cell.post = productDetailArr[indexPath.row]
        return cell
    }
    
}
