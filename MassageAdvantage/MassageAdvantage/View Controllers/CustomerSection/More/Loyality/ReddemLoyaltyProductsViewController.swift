//
//  ReddemLoyaltyProductsViewController.swift
//  MassageAdvantage
//
//  Created by nile on 10/07/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class ReddemLoyaltyProductsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    var itemId = ""
    var shippingCharge = ""
    @IBOutlet weak var shippingChargeLabel: UILabel!
    @IBOutlet weak var redeemProductCollectionView: UICollectionView!
    var products =  [UserInfo]()
    var productsDetailsArr =  [UserInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let shippingChargeFloat = (shippingCharge as NSString).floatValue  * 20
        
        let shippingChargePoints = String(format: "%.0f", shippingChargeFloat)
        
       
        
        shippingChargeLabel.text =  "Note: Shipping Charges extra (" + "\(shippingChargePoints)" + " Loyalty Points" + ")"
        

        let sectionNib = UINib(nibName: "RedeemProduct", bundle: nil)
        self.redeemProductCollectionView.register(sectionNib,
                                                  forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                     withReuseIdentifier: "RedeemProduct")
        
        
        if self.products.count == 0 {
            self.noDataFoundLbl.isHidden = false
        }
        else {
            self.noDataFoundLbl.isHidden = true
        }

        // Do any additional setup after loading the view.
    }
    
    //MARK :- Btn Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
        
        var sortedArr = [UserInfo]()
        
        for i in 0..<self.products.count{
            
            let obj  = self.products[i]
            
            for i in 0..<obj.productsDetails.count{
                
                let subObject = obj.productsDetails[i]
                
                if subObject.selectProduct == "1"
                {
                    sortedArr.append(subObject)
                }
            }
        }

            if sortedArr.count != 0{
                
                for  i in 0..<sortedArr.count {
                    let obj = sortedArr[i]
                    if sortedArr.count == 1{
                        self.itemId =  obj.id
                    }
                    else if i == 0{
                        self.itemId =  obj.id
                    }
                    else {
                        self.itemId = self.itemId + "," +  obj.id
                    }
                }
                callApiForProductPrice()
            }
            else {
                AlertController.alert(title: "Please select product.")
            }
        
            print(self.itemId)
    }

    //MARK:- Collection View

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        guard kind == UICollectionView.elementKindSectionHeader else {
            return UICollectionReusableView()
        }
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                   withReuseIdentifier: "RedeemProduct",
                                                                   for: indexPath) as! RedeemProduct
        
        let obj = products[indexPath.section]
        view.productTypeLbl.text = obj.name
    
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: self.redeemProductCollectionView.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products[section].productsDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RedeemProductsCollectionViewCell", for: indexPath) as! RedeemProductsCollectionViewCell
        
        cell.post = products[indexPath.section].productsDetails[indexPath.row]
         cell.delegate = self
     
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellsAcross: CGFloat = 2
        var widthRemainingForCellContent = collectionView.bounds.width
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
        }

        let cellWidth = widthRemainingForCellContent / cellsAcross
        return CGSize(width: cellWidth, height: 220)
    }


    func callApiForProductPrice() {
        APIManager.apiForProductPrice(item: self.itemId) { (status, dict,productsDetailsArray,msg) in
            if status {
                self.productsDetailsArr.removeAll()
                let subTotal : String = dict.validatedValue("sub_total", expected: "" as AnyObject) as! String
                let dollarPrice : String = dict.validatedValue("dollar_price", expected: "" as AnyObject) as! String
                
                
                let shippingPrice : String = dict.validatedValue("shipping_charge", expected: "" as AnyObject) as! String

                self.productsDetailsArr += UserInfo.productsDetailsArrayLoyalityPoints(responseArray: productsDetailsArray as! Array<Dictionary<String, Any>>)

                
                let redeemVC = storyBoardForName(name: "Main").instantiateViewController(withIdentifier: "LoyaltyConfirmPurchaseViewController") as!LoyaltyConfirmPurchaseViewController
                redeemVC.productDetailArr = self.productsDetailsArr
                redeemVC.subTotal = subTotal
                redeemVC.dollarPrice = dollarPrice
                redeemVC.shippingPrice = shippingPrice
                redeemVC.itemID = self.itemId
                APPDELEGATE.navController?.pushViewController(redeemVC, animated: true)
                
            }
            else {
                AlertController.alert(message: msg)
            }
            
        }
    }
}
    

//MARK: -  UIButton Action delegate
extension ReddemLoyaltyProductsViewController : RedeemProductsCollectionDelegate{
    
    func viewImage(_sender:RedeemProductsCollectionViewCell){
        guard let tappedIndexPath = redeemProductCollectionView.indexPath(for: _sender) else { return }
       
        let obj = products[tappedIndexPath.section].productsDetails[tappedIndexPath.row]
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MAReedemImagePopUpViewController") as! MAReedemImagePopUpViewController
        popOverVC.productImage = obj.productImg
        popOverVC.providesPresentationContextTransitionStyle = true
        popOverVC.definesPresentationContext = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        popOverVC.view.backgroundColor = UIColor.init(white : 0.50, alpha: 0.75)
        self.present(popOverVC, animated: true, completion: nil)

    }
    
    func seclectProduct(_sender:RedeemProductsCollectionViewCell){
        guard let tappedIndexPath = redeemProductCollectionView.indexPath(for: _sender) else { return }
        
        let obj = products[tappedIndexPath.section].productsDetails[tappedIndexPath.row]
        if obj.selectProduct == "0" {
            obj.selectProduct = "1"
            self.products[tappedIndexPath.section].productsDetails[tappedIndexPath.row] = obj
        }
        else {
            obj.selectProduct = "0"
            self.products[tappedIndexPath.section].productsDetails[tappedIndexPath.row] = obj
        }
        self.redeemProductCollectionView.reloadData()
    }
}
