//
//  MAViewAppointmentViewController.swift
//  MassageAdvantage
//
//  Created by nile on 05/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit
//
//protocol reloadBooking : class{
//    func reoadBookingData()
//}

//protocol goToviewAppoitment : class{
//    func goToAppointment()
//}

class MAViewAppointmentViewController: UIViewController,goToviewAppoitment {
    
    @IBOutlet weak var resheduledBtn: UIButton!
    @IBOutlet weak var cancelAppointmentBtn: UIButton!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var therapistNameLbl: UILabel!
    @IBOutlet weak var apoinmentDateLbl: UILabel!
    @IBOutlet weak var apointmentTime: UILabel!
    @IBOutlet weak var massageTypeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
 //   var delegate : reloadBooking?
    @IBOutlet weak var completionIDView: UIView!
    @IBOutlet weak var massageIdLbl: UILabel!

    var viewAppointmentObj = UserInfo()
    var errMessage = ""
    
    var bookingDetailObj = UserInfo()

    var availableDaysArray = [UserInfo]()
    var notAvailabelDatesArray = Array<String>()
    var couponDict = Dictionary<String,AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.view.layoutIfNeeded()
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.orange.cgColor
        yourViewBorder.lineDashPattern = [4, 4]
        yourViewBorder.frame = CGRect(x:0, y: 0, width: completionIDView.width, height: completionIDView.height)//savedOfferView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: CGRect(x:0, y: 0, width: completionIDView.width, height:completionIDView.height )).cgPath
        completionIDView.layer.addSublayer(yourViewBorder)

        massageIdLbl.text = "Massage Completion Id : " + viewAppointmentObj.completionID
        therapistNameLbl.text! = viewAppointmentObj.therapistName
        emailLbl.text = viewAppointmentObj.email
        phoneNumberLbl.text = viewAppointmentObj.phoneNumber
        apoinmentDateLbl.text = viewAppointmentObj.bookDate
        apointmentTime.text = viewAppointmentObj.time
        massageTypeLbl.text = viewAppointmentObj.serviceName
        addressLbl.text = viewAppointmentObj.address
        
        let userID : String = (NSUSERDEFAULT.value(forKey: kUserId) as AnyObject) as! String
        let bookingDate = viewAppointmentObj.bookDate
        
        let formatted = APPDELEGATE.formattedDateFromString(dateString: "\(Date())", withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd HH:mm:ss z")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let bookDate = formatter.date(from: bookingDate)
        let todayDate = formatter.date(from: formatted!)
                
        let cancelby = viewAppointmentObj.cancelledBy
        
        
                 if viewAppointmentObj.bookingStatus == "2"  {
        
                    if cancelby == userID
                    {
                    self.cancelAppointmentBtn.setTitle("Cancelled", for: .normal)
                    cancelAppointmentBtn.isUserInteractionEnabled = false
                  }
                    else
                    {
                        self.cancelAppointmentBtn.setTitle("Cancelled", for: .normal)
                        cancelAppointmentBtn.isUserInteractionEnabled = false
                    }
                }

                 else if  viewAppointmentObj.bookingStatus == "1"  {
                    if viewAppointmentObj.isPaid == "1"{
                        self.cancelAppointmentBtn.setTitle("Completed", for: .normal)
                        self.cancelAppointmentBtn.backgroundColor = #colorLiteral(red: 0, green: 0.7770329118, blue: 0.7831611633, alpha: 1)
                        cancelAppointmentBtn.isUserInteractionEnabled = false
                        self.resheduledBtn.isHidden = true
                        self.cancelAppointmentBtn.isHidden = false
                    }
                }
                 else {
                    if viewAppointmentObj.bookingStatus == "1" {
                        
                        if bookDate?.compare(todayDate!) == .orderedAscending {
                            
                            self.resheduledBtn.isHidden = false
                            self.cancelAppointmentBtn.isHidden = true
                        }
                        else if bookDate?.compare(todayDate!) == .orderedSame {
                            self.cancelAppointmentBtn.setTitle("Cancel Appointment", for: .normal)
                            cancelAppointmentBtn.isUserInteractionEnabled = true
                            self.resheduledBtn.isHidden = false
                            self.cancelAppointmentBtn.isHidden = false
                            self.cancelAppointmentBtn.backgroundColor = #colorLiteral(red: 0.9398615956, green: 0.07864715904, blue: 0.2439668477, alpha: 1)
                        }
                            
                        else{
                            self.cancelAppointmentBtn.setTitle("Cancel Appointment", for: .normal)
                            cancelAppointmentBtn.isUserInteractionEnabled = true
                            self.resheduledBtn.isHidden = false
                            self.cancelAppointmentBtn.isHidden = false
                            self.cancelAppointmentBtn.backgroundColor = #colorLiteral(red: 0.9398615956, green: 0.07864715904, blue: 0.2439668477, alpha: 1)
                            
                        }
                    }
        }
        if viewAppointmentObj.bookingType == "offline" {
            resheduledBtn.isHidden = true
            if viewAppointmentObj.bookingStatus == "2" {
                resheduledBtn.isHidden = true
                cancelAppointmentBtn.isHidden = false
                cancelAppointmentBtn.setTitle("Cancelled", for: .normal)
            }
                
            else  if viewAppointmentObj.bookingStatus == "1" {
                if viewAppointmentObj.isPaid == "1" {
                    cancelAppointmentBtn.isHidden = true
                    resheduledBtn.isHidden = false
                    resheduledBtn.setTitle("Completed", for: .normal)
                }
            }
            else {
                resheduledBtn.isHidden = true
                cancelAppointmentBtn.isHidden = false
                cancelAppointmentBtn.setTitle("Cancel Appointment", for: .normal)
            }
        }
    }

    //MARK:- UIBtn Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func cncelAppointmentBtn(_ sender: UIButton) {
        let bookingOn = viewAppointmentObj.bookingOn
                let BookingOnformatter = DateFormatter()
                BookingOnformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let bookingOnDate = BookingOnformatter.date(from: bookingOn)
                let diffInDays : Int  = Calendar.current.dateComponents([.hour], from: Date(), to:  bookingOnDate!).hour!
        
        let bookingDate = viewAppointmentObj.bookDate

        let formatted = APPDELEGATE.formattedDateFromString(dateString: "\(Date())", withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd HH:mm:ss z")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let bookDate = formatter.date(from: bookingDate)
        let todayDate = formatter.date(from: formatted!)
                if viewAppointmentObj.cancellationCharge == "0" && viewAppointmentObj.bookingStatus == "1" {
                    if bookDate?.compare(todayDate!) == .orderedAscending{
                        CancelAlertMessage(msg: "This appointment is being cancelled within 24 hours of the appointment time. So you need to pay $25 extra at the time of rescheduling this appointment. Are you sure you want to cancel this appointment.?")
                    }
        
                    else if bookDate?.compare(todayDate!) == .orderedSame {
                        if diffInDays >= 24 {
                            CancelAlertMessage(msg: "Are you sure you want to cancel this appointment.?")

        
                        }
                        else {
                          CancelAlertMessage(msg: "This appointment is being cancelled within 24 hours of the appointment time. So you need to pay $25 extra at the time of rescheduling this appointment. Are you sure you want to cancel this appointment.?")
                        }
        
                    }
                    else {
                        if diffInDays >= 24{
                        CancelAlertMessage(msg: "Are you sure you want to cancel this appointment.?")
        
                        }
        
                        else {
                           CancelAlertMessage(msg: "This appointment is being cancelled within 24 hours of the appointment time. So you need to pay $25 extra at the time of rescheduling this appointment. Are you sure you want to cancel this appointment.?")
        
                        }
                    }
                }
        
                else{
                    if viewAppointmentObj.cancellationCharge == "0" && viewAppointmentObj.bookingStatus == "2" {
                        CancelAlertMessage(msg: "Are you sure you want to cancel this appointment.?")
                    }
                    else {
             CancelAlertMessage(msg: "This appointment is being cancelled within 24 hours of the appointment time. So you need to pay $25 extra at the time of rescheduling this appointment. Are you sure you want to cancel this appointment.?")                    }
        
                }
        
        
    }
    
    @IBAction func resheduledBtnAction(_ sender: UIButton) {
        
        APIManager.apiForBookingDetails(slugName: self.viewAppointmentObj.slug, coupon: "") { (status, dict,checkOutArray,reviewArray) in
            if status {
                self.bookingDetailObj.notAvlFromDate = dict.validatedValue("not_avl_from_date", expected: "" as AnyObject)as! String
                self.bookingDetailObj.notAvlToDate = dict.validatedValue("not_avl_to_date", expected: "" as AnyObject)as! String
                self.bookingDetailObj.notAvailabelDatesArray = dict.validatedValue("not_available_dates", expected: [] as AnyObject) as! Array<String>
                self.bookingDetailObj.therapistDetailID = dict.validatedValue("therapist_detail_id", expected: "" as AnyObject) as! String
                self.bookingDetailObj.availableDaysArray += UserInfo.getAvailabelDaysArray(responseArray: dict.validatedValue("available_days", expected: [] as AnyObject) as! Array<Dictionary<String, Any>>)
                self.bookingDetailObj.serviceName = dict.validatedValue("business_name", expected: "" as AnyObject)as! String
                let massageDetailVC = storyBoardForName(name: "Home").instantiateViewController(withIdentifier: "MADateAndTimeViewController")as! MADateAndTimeViewController
                massageDetailVC.obj = self.bookingDetailObj
                massageDetailVC.couponDict = self.couponDict
                massageDetailVC.rescheduleObj = self.viewAppointmentObj
                massageDetailVC.headingName = self.bookingDetailObj.serviceName
                massageDetailVC.isReschedule = true
                massageDetailVC.delegate = self
                self.navigationController!.pushViewController(massageDetailVC, animated: true)
            }
        }
      
    }

    //MARK:- Go to Booking
    func goToAppointment()
    {
        
        self.navigationController?.popViewController(animated: true)
       // self.delegate?.reoadBookingData()
    }
    
    func CancelAlertMessage(msg: String) {
        
        AlertController.alert(title: "", message: msg, buttons: ["No","Yes"]) { (alert, index) in
            
            if index == 1{
                APIManager.apiForCancelBooking(bookingId: self.viewAppointmentObj.paymentID) { (status,dict) in
                    if status{
                        
                        let msg : String = dict.validatedValue("message", expected: "" as AnyObject) as! String
                        AlertController.alert(title: msg, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                            //  self.delegate?.reoadBookingData()
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
        }
        
        
    }
    
    
    
}
