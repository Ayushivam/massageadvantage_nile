//
//  ConfirmBookingViewController.swift
//  MassageAdvantage
//
//  Created by nile on 06/06/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit

class ConfirmBookingViewController: UIViewController {

    @IBOutlet weak var whtsAppImageViewConstraints: NSLayoutConstraint!
    @IBOutlet weak var textImageView: UIImageView!
    @IBOutlet weak var whatsAppImageView: UIImageView!
    @IBOutlet weak var goToHomeBtn: UIButton!
    @IBOutlet weak var transactionIDLabel: UILabel!
    @IBOutlet weak var bookedOnLbl: UILabel!
    @IBOutlet weak var amountPaidLbl: UILabel!
    @IBOutlet weak var timeSlotLbl: UILabel!
    @IBOutlet weak var serviceDateLbl: UILabel!
    @IBOutlet weak var massageNameLbl: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var contactNumberLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var UserImageView: UIImageView!
    @IBOutlet weak var transactionIDView: UIView!
    var bookingID = ""
    var bookinkConfirmAppointObj = UserInfo()
    var userImg = ""
    var serviceID = ""
    var couponType = ""
    var lat = ""
    var long = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.orange.cgColor
        yourViewBorder.lineDashPattern = [4, 4]
        yourViewBorder.frame = CGRect(x:0, y: 0, width: transactionIDView.width, height: transactionIDView.height)//savedOfferView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: CGRect(x:0, y: 0, width: transactionIDView.width, height:transactionIDView.height )).cgPath
        transactionIDView.layer.addSublayer(yourViewBorder)
        
  
        APIManager.apiForBookingConfirmAppointment(bookingID :bookingID) { (status, dict) in
            if status {
                
                self.bookinkConfirmAppointObj.name = dict.validatedValue("username", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.phoneNumber = dict.validatedValue("phone", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.email = dict.validatedValue("email", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.serviceName = dict.validatedValue("massage_name", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.completionID = dict.validatedValue("pay_code", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.bookDate = dict.validatedValue("booking_date", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.createdDate  = dict.validatedValue("created_date", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.amount  = dict.validatedValue("total", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.createdDate  = dict.validatedValue("created_date", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.timeSlotFrom = dict.validatedValue("booking_from", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.timeSlotTo = dict.validatedValue("booking_to", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.therapistID = dict.validatedValue("therapist_id", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.profileImg = dict.validatedValue("profile_thumbnail", expected: "" as AnyObject) as! String
                self.lat = dict.validatedValue("latitude", expected: "" as AnyObject) as! String
                self.long = dict.validatedValue("longitude", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.serviceId = dict.validatedValue("service_id", expected: "" as AnyObject) as! String
                self.bookinkConfirmAppointObj.couponValue = dict.validatedValue("booking_type", expected: "" as AnyObject) as! String
                self.serviceID = self.bookinkConfirmAppointObj.serviceId
                self.couponType = self.bookinkConfirmAppointObj.couponValue
                if self.serviceID == "1" && self.couponType == "coupon" {
                      self.amountPaidLbl.text = "$0" 
                }
                else {
                    self.amountPaidLbl.text = "$ " + self.bookinkConfirmAppointObj.amount
                }
                let TimeSlotFrom : String =  self.bookinkConfirmAppointObj.timeSlotFrom
                let TimeSlotTo : String =  self.bookinkConfirmAppointObj.timeSlotTo
                self.timeSlotLbl.text = TimeSlotFrom + " - " + TimeSlotTo
                self.userName.text =  self.bookinkConfirmAppointObj.name
                self.contactNumberLbl.text = self.bookinkConfirmAppointObj.phoneNumber
                self.emailLabel.text = self.bookinkConfirmAppointObj.email
                self.massageNameLbl.text =  self.bookinkConfirmAppointObj.serviceName
                self.transactionIDLabel.text = "Massage Completion Id :" + self.bookinkConfirmAppointObj.completionID
                let bookDate : String = APPDELEGATE.formattedDateFromString(dateString: self.bookinkConfirmAppointObj.bookDate, withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd")!
                self.serviceDateLbl.text =  bookDate
                let createdDate : String = APPDELEGATE.formattedDateFromString(dateString: self.bookinkConfirmAppointObj.createdDate, withFormat: "MM/dd/yyyy", inputFormat: "yyyy-MM-dd HH:mm:ss")!
                self.bookedOnLbl.text = createdDate
                self.userImg =  "https://www.massageadvantage.com/uploads/profile/" + self.bookinkConfirmAppointObj.therapistID + "/thumbnail/" + self.bookinkConfirmAppointObj.profileImg
                self.UserImageView.sd_setImage(with: URL(string: self.userImg), placeholderImage: UIImage(named:"usero"), options: SDWebImageOptions(rawValue: 0))
                let accept =  dict.validatedValue("accept_message", expected: "" as AnyObject) as! String
                if accept == ""  {
                    self.textImageView.isHidden = true
                    self.whatsAppImageView.isHidden = true
                }
                else  if accept == "2"  {
                    self.textImageView.isHidden = true
                    self.whatsAppImageView.isHidden = false
                }
                else  if accept == "1" {
                    self.whatsAppImageView.isHidden = true
                    self.textImageView.isHidden = false
                }
                else  if accept == "3" {
                    self.whatsAppImageView.isHidden = false
                    self.textImageView.isHidden = false
                }
                else  if accept == "4" {
                    self.textImageView.isHidden = true
                    self.whatsAppImageView.isHidden = true
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    //MARK:- UIButton Action
    @IBAction func goToHomeBtnAction(_ sender: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MATabViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

    @IBAction func getDirectionBtnAction(_ sender: UIButton) {
                if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(self.lat),\(self.long)&directionsmode=driving") {
                    UIApplication.shared.open(url, options: [:])
                }else {
                    AlertController.alert(title: "Please install google map application.")
                }
        }
}
