//
//  MABookingListViewController.swift
//  MassageAdvantage
//
//  Created by Vibhuti Sharma on 07/05/19.
//  Copyright © 2019 Nile Technologies. All rights reserved.
//

import UIKit


class MABookingListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var viewingListLbl: UILabel!
    @IBOutlet weak var bookingTableView: UITableView!
   
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var noDataFoundLbl: UILabel!
    var bookingArray = [UserInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadBookingForRating(notification :)), name: NSNotification.Name("reloadAfterSucessComlpete"), object: nil)

      //  initialSetUp()
//        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadBookingApi(notification :)), name: NSNotification.Name("reloadBookingApi"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp() {
        
        if let _ = NSUSERDEFAULT.value(forKey: kUserId) {

        APIManager.apiForGetBookingAppointmentList{(status,bookingAppointmentListArr,PayCharge) in
            if status{

                self.bookingArray.removeAll()
                self.bookingArray += UserInfo.getBookingListArray(responseArray: bookingAppointmentListArr as! Array<Dictionary<String, Any>>,payCancellationCharge: PayCharge)
                
                self.bookingTableView.reloadData()
            }
            else {
                let _ = (self.bookingArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
            }
            self.viewingListLbl.isHidden = true
            self.logInBtn.isHidden = true
            self.bgImage.isHidden = true
        }
        }
        else {
           viewingListLbl.isHidden = false
            logInBtn.isHidden = false
            self.bgImage.isHidden = false
        }
    }
    
    @objc func reloadBookingForRating(notification: Notification) {
        initialSetUp()
    }
    deinit {

        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        initialSetUp()
        
    }
    
//    @objc func reloadBookingApi(notification: Notification) {
//
//         if let _ = NSUSERDEFAULT.value(forKey: kUserId) {
//        APIManager.apiForGetBookingAppointmentList{(status,bookingAppointmentListArr) in
//            if status{
//
//                self.bookingArray.removeAll()
//                self.bookingArray += UserInfo.getBookingListArray(responseArray: bookingAppointmentListArr as! Array<Dictionary<String, Any>>)
//                self.bookingTableView.reloadData()
//            }
//            else {
//                 let _ = (self.bookingArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
//            }
//            self.viewingListLbl.isHidden = true
//            self.logInBtn.isHidden = true
//            self.bgImage.isHidden = true
//        }
//        }
//         else {
//            viewingListLbl.isHidden = false
//            logInBtn.isHidden = false
//            self.bgImage.isHidden = false
//        }
//    }

//    func reoadBookingData(){
//
//        if let _ = NSUSERDEFAULT.value(forKey: kUserId) {
//
//            APIManager.apiForGetBookingAppointmentList{(status,bookingAppointmentListArr) in
//                if status{
//
//                    self.bookingArray.removeAll()
//                    self.bookingArray += UserInfo.getBookingListArray(responseArray: bookingAppointmentListArr as! Array<Dictionary<String, Any>>)
//                    self.bookingTableView.reloadData()
//                }
//                else {
//                    let _ = (self.bookingArray.count == 0) ? (self.noDataFoundLbl.isHidden = false) : (self.noDataFoundLbl.isHidden = true)
//                }
//
//                self.viewingListLbl.isHidden = true
//                self.logInBtn.isHidden = true
//                self.bgImage.isHidden = true
//            }
//        }
//
//        else {
//            viewingListLbl.isHidden = false
//            logInBtn.isHidden = false
//            self.bgImage.isHidden = false
//
//        }
//    }

    //MARK:- Button Actions
    @IBAction func loginBtnAction(_ sender: UIButton) {
        let viewControllers = APPDELEGATE.navController!.viewControllers
        
        for i in 0..<viewControllers.count {
            let obj = viewControllers[i]
            if obj.isKind(of: MALoginViewController.self){
                APPDELEGATE.resetDefaults()
                APPDELEGATE.navController!.popToViewController(obj, animated: true)
            }
        }
    }

    //MARK:-  UITable View Delegate And Datasource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125.0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return bookingArray.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAMyBookingListTableViewCell", for: indexPath) as! MAMyBookingListTableViewCell
        cell.post = self.bookingArray[indexPath.row]
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
}

extension MABookingListViewController : MyBookingTableViewCellDelegate{
    
    func viewTapAction(_sender: MAMyBookingListTableViewCell) {
        
        guard let tappedIndexPath = bookingTableView.indexPath(for: _sender) else { return }
        
        let massageDetailVC = storyBoardForName(name: "Main").instantiateViewController(withIdentifier: "MAViewAppointmentViewController") as! MAViewAppointmentViewController
       // massageDetailVC.delegate = self
        massageDetailVC.viewAppointmentObj = self.bookingArray[tappedIndexPath.row]
           APPDELEGATE.navController?.pushViewController(massageDetailVC, animated: true)
        
    }
}
